<?php
/**
 * Logging.php File
 * 
 * Adds error handling on shutdown to ensure PHP errors get logged.
 * Simply including the file suffices.
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */

/**
 * Handles errors on unexpected shutdown of the PHP environment, 
 * passing information to the logger
 * 
 * @return void
 */
function shutdownHandler()
{
    $last_error = error_get_last();
    if ($last_error) {
        errorHandler($last_error['type'], $last_error['message'], $last_error['file'], $last_error['line']);
    }
}
register_shutdown_function('shutdownHandler');

/**
 * Logs errors to Apache and writes out a 500 Internal Server Error header
 * 
 * @param int    $code    the error code (i.e. E_COMPILE_ERROR, E_USER_ERROR)
 * @param string $message the error message
 * @param string $errFile the file where the error occured
 * @param int    $errLine the line of the file where the error occured
 * 
 * @return void
 */
function errorHandler($code, $message, $errFile, $errLine)
{

    switch($code) {
    case E_WARNING:
    case E_DEPRECATED:
    case E_NOTICE:
        //ignore
        return;

    // default fall through to error
    }

    $codeStr = errToString($code);
    $logLine = "Error $codeStr on line $errLine of [$errFile]:  \n"
              . "  " . $message;

    error_log($logLine);
  

    ob_clean();
    header("HTTP/1.1 500 Internal Server Error");
    include 'error_500.html'; 
}

/**
 * Converts an integer error code to its string representation
 * i.e. 256 => 'E_USER_ERROR'
 * http://php.net/manual/en/errorfunc.constants.php
 * 
 * @param int $code the error code
 * 
 * @return string the name of the error
 */
function errToString($code)
{
    switch($code) {
    case 1: 
        return 'E_ERROR';
    case 2: 
        return 'E_WARNING';
    case 4: 
        return 'E_PARSE';
    case 8: 
        return 'E_NOTICE';
    case 16: 
        return 'E_CORE_ERROR';
    case 32: 
        return 'E_CORE_WARNING';
    case 64: 
        return 'E_COMPILE_ERROR';
    case 128: 
        return 'E_COMPILE_WARNING';
    case 256: 
        return 'E_USER_ERROR';
    case 512: 
        return 'E_USER_WARNING';
    case 1024: 
        return 'E_USER_NOTICE';
    case 8192: 
        return 'E_DEPRECATED';
    case 16384: 
        return 'E_USER_DEPRECATED';
    default: 
        return strval($code);
    }
}