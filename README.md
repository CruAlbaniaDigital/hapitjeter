## Hapi Tjeter

This is the source code for [HapiTjeter.net](http://hapitjeter.net/).

[![Join the chat at https://gitter.im/campuscrusade/lobby](https://badges.gitter.im/campuscrusade/lobby.svg)](https://gitter.im/campuscrusade/lobby?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

Simple PHP behind Apache with mysql database.

To set up your development environment for hapitjeter [Click Here](https://gitlab.com/cru-albania-ds/hapitjeter/wikis/home)
