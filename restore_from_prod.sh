#! /bin/bash

set -e

FILE=$(gsutil ls gs://ijr-hapitjeter-backup/backups/dump.hapitjeter* | sort -r | head -n 1)
mkdir -p backups/
gsutil cp $FILE backups/
FILE=backups/$(basename $FILE)

vagrant ssh -c "gzip -dc /src/$FILE | mysql -h 127.0.0.1 -uroot -proot -Dhapitjeter --default-character-set=utf8"