
$(document).ready(
    function() {
        $('span#ref_link1').click(
            function() {
                prompt('Copiati acest link si promovati-l',$("span#ref_linktext1").text());
            }
        )
        $('span#ref_link2').click(
            function() {
                prompt('Copiati acest link si promovati-l',$("span#ref_linktext2").text());
            }
        )
    
        $(".biblie a").tooltip(
            { 
                bodyHandler: function() { 
                    return $($(this).attr("href")).html(); 
                }, 
                showURL: false,
                extraClass: "verset",
                track: true 
            }
        );
    
        $(".sugerat").tooltip(
            { 
                bodyHandler: function() { 
                    return $(this).attr("rel"); 
                }, 
                showURL: false,
                track: true
            }
        );
    

    
    }
);

checked=false;
function checkAll () {
    var aa= document.getElementById('form');
    if(checked == false) { 
        checked = true
    }
    else {
        checked = false
    }
    
    for (var i =0; i < aa.elements.length; i++) {
        aa.elements[i].checked = checked;
    }
}

function expand(cum) {
    switch(cum) {
    case'expand':
        $('#btn_hide').hide()
        $('#btn_show').show();
        break;
        
    case'collapse':
        $('#btn_show').hide();
        $('#btn_hide').show();
        break;
    }
    $('#lectie').slideToggle();
}

function info_mentor(idu) {
    if(idu == "") {
        $('.mentor').remove();
    }
    else {
        $.ajax(
            {
                type: "GET",
                url: "index.php?act=ajax&op=info_user",
                data: "id="+idu+"",
                success: function(msg){
                    if(msg == "error") {
                        $('.mentor').remove();
                    }
                    else {
                        $('<div class="mentor">'+msg+'</div>').insertAfter($('#mentor_info'));
                    }
                }
            }
        );
        
        $('input[name=parent_id]').val(idu);
    }
}

