<?php
/**
 * File useri.php
 * 
 * Admin page showing a table of the users in the system.
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */

ob_start();
acces(); 
if(nivel($_SESSION['log_id']) != 5) { header("location:/"); 
}

$sql = mysql_query("SELECT COUNT(id) AS tot FROM useri") or trigger_error(mysql_error(), E_USER_ERROR);
if(mysql_num_rows($sql) == 0) { $nr=0; 
}
else {
    $row = mysql_fetch_object($sql);
    $nr = $row->tot;
}
mysql_free_result($sql);
?>

<div id="content">
    <h1><?php echo L_USER_TITLU?></h1>
    <br/>
    <form method="get" action="index.php">
        <input type="text" name="c" value="<?php echo $_GET['c']?>"/> <input type="submit" name="ok" value="Search"/>
        <input type="hidden" name="act" value="useri"/>
    </form>
    <br/>
    
    
    <?php
    echo $_SESSION['mesaj'];
    unset($_SESSION['mesaj']);
    switch($_GET['op']) {
    case'':
        
        if($_GET['c'] != "") {
             $c = $_GET['c'];
             $extra_q=" AND nume LIKE '%$c%'";
        }
        
        if(!isset($_GET["page"]) ) { $page = 1; 
        } else { $page = mysql_real_escape_string($_GET["page"]); 
        }
            
        $id = $_SESSION['log_id'];
        $q = "SELECT * FROM useri WHERE id!=$id $extra_q ORDER BY id ASC";
        $limit = 20;
        $query = mysql_query($q) or trigger_error(mysql_error(), E_USER_ERROR);
        $results= mysql_num_rows($query);
        $nrpages = ceil($results/$limit);
        $limitvalue = $page * $limit - ($limit); 
        $sql = mysql_query("$q LIMIT $limitvalue, $limit") or trigger_error(mysql_error(), E_USER_ERROR);
        $dela = $limitvalue +1 ;
        $la   = $limitvalue + $limit;
        if ($la > $results ) { $la = $results ; 
        }
        if(mysql_num_rows($sql) == 0) {
        }
        else {
            echo'
				<table id="nice_table" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td width="10"></td>
						<td class="tbb" width="40"><b>ID</b></td>
						<td class="tbb"><b>'.L_NUME.'</b></td>
						<td class="tbl tbb" width="30"><b>'.L_STAT_NIVEL.'</b></td>
						<td class="tbl tbb" width="80"><b>Op.</b></td>
						<td width="10"></td>
					</tr>';
                
            while($row = mysql_fetch_object($sql)) {
                echo'
					<tr>
						<td width="10"></td>
						<td class="tbb">'.$row->id.'.</td>
						<td class="tbb">'.$row->nume.' <span style="font-size:11px;">[ <b>user:</b> '.$row->user.', <b>email:</b> '.$row->email.' ]</span></td>
						<td class="tbl tbb" align="center">'.$row->nivel.'</td>
						<td class="tbl tbb">
							<a href="?act=inbox&op=compose&destinatar='.$row->user.'"><img src="images/mesaj.png"/></a>&nbsp;
							<a href="?act=useri&op=edit&id='.$row->id.'"><img src="images/edit.gif"/></a>
							<a href="?act=useri&op=delete&id='.$row->id.'" onclick="return confirm(\''.L_USER_CONFIRM.'\');"><img src="images/delete.png"/></a></td>
						<td width="10"></td>
					</tr>';
            }
            $inapoi  = max($page-1, 1);
            $inainte = min($page+1, $nrpages);
                
            if($page == $inapoi) { $inapoi = '';
            } else { 
                $inapoi = '<span style="font-size:11px;">&laquo;</span> <a href="index.php?page='.$inapoi.'&act=useri&c='.$c.'">'.L_PREV.'</a>';
            }
            if($page == $inainte) { $inainte = '';
            } else { 
                $inainte = '&nbsp;&nbsp;<a href="index.php?page='.$inainte.'&act=useri&c='.$c.'">'.L_NEXT.'</a> <span style="font-size:11px;">&raquo;</span>';
            }
            echo'<tr><td width="10"></td><td colspan="4"><b>'.$page.'</b> '.L_DIN.' <b>'.$nrpages.'</b> '.$inapoi.' &nbsp;&nbsp;';
                
            for($i=1;$i<=$nrpages;$i++) {
                if($i <> $page) {
                    echo'<a href="index.php?page='.$i.'&act=useri&c='.$c.'">'.$i.'</a> ';
                }
                else {
                    echo'<a href="index.php?page='.$i.'&act=useri&c='.$c.'"><u>'.$i.'</u></a> ';
                }
                        
            }
                
            echo''.$inainte.'</td><td width="10"></td></tr>';
            echo'</table>';
        }
        mysql_free_result($sql);
        break;
        
    case'edit':
        echo'&raquo; <a href="?act=useri">'.L_INAPOI_USERI.'</a><br/><br/>';
        if(isset($_POST['ok'])) {
             $user = mysql_real_escape_string($_POST['user']);
             $parola = mysql_real_escape_string($_POST['parola']);
             $nume = diacritice(mysql_real_escape_string($_POST['nume']));
             $email = mysql_real_escape_string($_POST['email']);
             $ym = mysql_real_escape_string($_POST['ym']);
             $parent_id = mysql_real_escape_string($_POST['parent_id']);
             $nivel = mysql_real_escape_string($_POST['nivel']);
             $varsta = mysql_real_escape_string($_POST['varsta']);
             $oras = mysql_real_escape_string($_POST['oras']);
             $profesie = diacritice(mysql_real_escape_string($_POST['profesie']));
             $telefon = mysql_real_escape_string($_POST['telefon']);
             $de_unde = mysql_real_escape_string($_POST['de_unde']);
             $id = mysql_real_escape_string($_POST['id']);
             $afis_mentor = mysql_real_escape_string($_POST['afis_mentor']);
            
            if(($user == "") OR ($parola == "") OR ($nume == "") OR ($email == "") OR ($parent_id == "") OR ($nivel == "")) {
                echo'<span id="error">'.L_CAMPURI_INCOMPLETE.'!</span>';
            }
            else {
                mysql_query("UPDATE useri SET user='$user',parola='$parola',nume='$nume',email='$email',parent_id='$parent_id',nivel='$nivel',varsta='$varsta',oras='$oras',profesie='$profesie',telefon='$telefon',de_unde='$de_unde',ym='$ym',afis_mentor='$afis_mentor' WHERE id='$id'");
                echo'<span id="done">'.L_CU_SUCCES.'!</span>';
            }
            
            
        }
        elseif(isset($_POST['muta'])) {
             $id = mysql_real_escape_string($_POST['id']);
             $id_viitor = mysql_real_escape_string($_POST['id_viitor']);
            if($id_viitor == "") {
                echo'<span id="error">'.L_USER_ERROR_MUTA.'!</span>';
            }
            else {
                mysql_query("UPDATE useri SET parent_id='$id_viitor' WHERE parent_id='$id'");
                echo'<span id="done">'.L_USER_MUTA_OK.'!</span>';
            }
        }
        else {
        }
        
        $id = mysql_real_escape_string($_GET['id']);
        $sql = mysql_query("SELECT * FROM useri WHERE id='$id'");
        $row = mysql_fetch_object($sql);
        
        if($row->afis_mentor == "1") {$afis_mentor='checked'; 
        } else { $afis_mentor=''; 
        }
        
        echo'<form method="post" action="">
		<table cellspacing="2" cellpadding="4" style="margin-top:5px;">
			<tr>
				<td>'.L_REG_NICK.':</td>
				<td><input type="text" class="inputus" size="40" name="user" value="'.stripslashes($row->user).'"/></td>
				<td rowspan="8" valign="top" style="padding-left:15px;">';
        $sql2 = mysql_query("SELECT id FROM useri WHERE parent_id=$id");
        if(mysql_num_rows($sql2) == 0) {
            echo''.L_USER_NO.'.';
        }
        else {
            echo''.str_replace("[nr]", mysql_num_rows($sql2), L_USER_NR).'.<br/>';
            echo'<b>'.L_USER_MUTA.':</b><br/><br/>
					<form method="post" action="">
					<select name="id_viitor" class="selectus" style="margin-bottom:10px;">
						<option value="">--</option>';
            $sql0 = mysql_query("SELECT id,user,nume FROM useri ORDER BY user ASC") or trigger_error(mysql_error(), E_USER_ERROR);
            if(mysql_num_rows($sql0) == 0) {
            }
            else {
                while($row0 = mysql_fetch_object($sql0)) {
                    echo'<option value="'.$row0->id.'">'.$row0->user.' ('.$row0->nume.')</option>';
                }
            }
            mysql_free_result($sql0);
        
            echo'</select> <input type="submit" name="muta" value="'.L_USER_MUT.'" class="menu menu_active"/>
					<input type="hidden" name="id" value="'.$id.'"/>
					</form>';
        }
                
        echo'</td>
			</tr>
			<tr>
				<td>'.L_REG_PAROLA.':</td>
				<td><input type="password" class="inputus" size="40" name="parola" value="'.stripslashes($row->parola).'"/></td>
			</tr>
			<tr>
				<td>'.L_REG_NUME.':</td>
				<td><input type="text" class="inputus" size="40" name="nume" value="'.stripslashes($row->nume).'"/></td>
			</tr>
			<tr>
				<td>'.L_REG_EMAIL.':</td>
				<td><input type="text" class="inputus" size="40" name="email" value="'.stripslashes($row->email).'"/></td>
			</tr>
			<tr>
				<td>'.L_YAHOO.':</td>
				<td><input type="text" class="inputus" size="40" name="ym" value="'.stripslashes($row->ym).'"/></td>
			</tr>
			<tr>
				<td>'.L_REG_TELEFON.':</td>
				<td><input type="text" class="inputus" size="40" name="telefon" value="'.stripslashes($row->telefon).'"/></td>
			</tr>
			<tr>
				<td>'.L_REG_ORAS.':</td>
				<td><input type="text" class="inputus" size="40" name="oras" value="'.stripslashes($row->oras).'"/></td>
			</tr>
			<tr>
				<td>'.L_REG_PROFESIE.':</td>
				<td><input type="text" class="inputus" size="40" name="profesie" value="'.stripslashes($row->profesie).'"/></td>
			</tr>
			<tr>
				<td>'.L_REG_REF.':</td>
				<td><input type="text" class="inputus" size="40" name="de_unde" value="'.stripslashes($row->de_unde).'"/></td>
			</tr>
			<tr>
				<td>'.L_REG_VARSTA.':</td>
				<td><input type="text" class="inputus" style="width:30px" name="varsta" value="'.stripslashes($row->varsta).'"/></td>
			</tr>
			<tr>
				<td>'.L_USER_PARENT.':</td>
				<td><select name="parent_id" class="selectus">
					<option value=""> -- </option>';
        if ($row->nivel < 5) {
            $sql1 = mysql_query("SELECT id,user,nume FROM useri WHERE nivel>$row->nivel ORDER BY nume ASC") or trigger_error(mysql_error(), E_USER_ERROR);
        }
        else if ($row->nivel == 5) {
            $sql1 = mysql_query("SELECT id,user,nume FROM useri WHERE nivel='5' ORDER BY nume ASC") or trigger_error(mysql_error(), E_USER_ERROR);
        }
        if(mysql_num_rows($sql1) == 0) {
        }
        else {
            while($row1 = mysql_fetch_object($sql1)) {
                if($row->parent_id == $row1->id) { $sel=" selected"; 
                } else { $sel=""; 
                }
                echo'<option value="'.$row1->id.'"'.$sel.'>'.$row1->user.' ('.$row1->nume.')</option>';
            }
        }
        mysql_free_result($sql);
        echo'</select></td>
			</tr>
			<tr>
				<td>'.L_STAT_NIVEL.':</td>
				<td><input type="text" class="inputus" style="width:20px;" name="nivel" value="'.stripslashes($row->nivel).'"/> (maxim <b>5</b>)</td>
			</tr>
			<tr>
				<td>'.L_USER_LOGIN.':</td>
				<td>'.date('d.m.Y H:i', $row->last_login).'</td>
			</tr>
			<tr>
				<td></td>
				<td><input type="checkbox" name="afis_mentor" value="1" '.$afis_mentor.' id="afis_mentor"/> <label for="afis_mentor">'.L_MENTOR_RECOMANDAT.'</label></td>
			</tr>
			<tr>
				<td><input type="hidden" name="id" value="'.$id.'"/></td>
				<td><input type="submit" name="ok" value="'.L_MODIFICA.'" class="menu"/></td>
			</tr>
		</table>
		</form>';
        break;
        
    case'delete':
        $id = mysql_real_escape_string($_GET['id']);
        mysql_query("DELETE FROM useri WHERE id='$id'");
        mysql_query("DELETE FROM rezolvari WHERE id_user='$id'");
        mysql_query("DELETE FROM status_lectie WHERE id_user='$id'");
        mysql_query("DELETE FROM status_intrebare WHERE id_user='$id'");
        $_SESSION['mesaj'] = '<span id="done">'.L_USER_STERS.'!</span>';
        header("location:index.php?act=useri");
        break;
    }
    ?>
</div>

<?php
$content = ob_get_clean();
ob_end_clean();
?>