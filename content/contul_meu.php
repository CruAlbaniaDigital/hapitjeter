<?php
/**
 * File contul_menu.php
 * 
 * User's personal information page, allows user to
 * change info and reset password.
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */

ob_start();
acces(); 
?>

<div id="content">
    <h1><?php echo L_CONT_CONTUL_MEU?></h1>
    <br/>
    <?php

    if(isset($_POST['ok'])) {
                
        $email = mysql_real_escape_string($_POST['email']);
        $ym = mysql_real_escape_string($_POST['ym']);
        $nume = diacritice(mysql_real_escape_string($_POST['nume']));
        $descriere = diacritice(mysql_real_escape_string($_POST['descriere']));
        $poza = $_FILES['poza']['name'];
        $id = $_SESSION['log_id'];
        
        if(!is_valid($email) OR ($nume == "")) {
            echo'<span id="error">'.L_CAMPURI_INCOMPLETE.'!</span>';
        }
        else {
            
            $poza_ex = getOneValue("useri", "id", $id, "poza");
            
            if($poza == "") {
                if($poza_ex == "") { $poza=''; 
                } else { $poza = $poza_ex; 
                }
                if($_POST['sterge_poza'] == 1) {
                    unlink("upload/fckfiles/$poza");
                    $poza='';
                }
            }
            else {
                if($poza_ex == "") {$poza = rand(345454, 65565).'_'.$poza;
                }
                else { $poza = $poza_ex; 
                }
                move_uploaded_file($_FILES['poza']['tmp_name'], "upload/fckfiles/$poza");
            }
            
            mysql_query("UPDATE useri SET email='$email',nume='$nume',ym='$ym',descriere='$descriere',poza='$poza' WHERE id=$id");
            echo'<span id="done">'.L_CU_SUCCES.'!</span>';
        
        }
    }
    elseif(isset($_POST['parola'])) {
        
        $parola_veche = mysql_real_escape_string($_POST['parola_veche']);
        $parola_noua = mysql_real_escape_string($_POST['parola_noua']);
        $id = $_SESSION['log_id'];
        
        if(($parola_veche == "") OR ($parola_noua == "")) {
            echo'<span id="error">'.L_CAMPURI_INCOMPLETE.'!</span>';
        }
        else {
            $sql = mysql_query("SELECT parola FROM useri WHERE id='$id' LIMIT 0,1");
            $row = mysql_fetch_object($sql);
            if($row->parola != md5($parola_veche)) {
                echo'<span id="error">'.L_CONT_PAROLA_GRESITA.'</span>';
            }
            else {
                mysql_query("UPDATE useri SET parola='".md5($parola_noua)."' WHERE id='$id'");
                echo'<span id="done">'.L_CU_SUCCES.'!</span>';
            }
        }
    }
    elseif(isset($_POST['sterge'])) {
        $id_user = $_SESSION['log_id'];
        mysql_query("DELETE FROM mesagerie WHERE id_expeditor='$id_user' OR id_destinatar='$id_user'");
        mysql_query("DELETE FROM rezolvari WHERE id_user='$id_user'");
        mysql_query("DELETE FROM status_intrebare WHERE id_user='$id_user'");
        mysql_query("DELETE FROM status_lectie WHERE id_user='$id_user'");
        $parent_id = getOneValue("useri", "id", $id_user, "parent_id");
        mysql_query("UPDATE useri SET parent_id='$parent_id' WHERE parent_id='$id_user'");
        header("location:index.php?act=deconectare");
    }
    else {
    }
    
    $poza = getOneValue("useri", "id", $_SESSION['log_id'], "poza");
    if($poza == "") { $poza_ex=''; 
    }
    else {
        $poza_ex='<div style="margin-top:5px;"><img src="upload/fckfiles/'.$poza.'" width="150"/> <input type="checkbox" name="sterge_poza" id="sterge_poza" value="1"> <label for="sterge_poza">'.L_STERGE_POZA.'</label></div>';
    }
    
    echo'<form method="post" action="" enctype="multipart/form-data">
	<table>
			<tr>
				<td valign="top">
				
					<table width="100%" id="form">
						<tr>
							<td valign="top" width="360">
								<h2>'.L_DATE_PERSONALE.'</h2>
								<span class="star">*</span> '.L_EMAIL.':<br/>
								<input type="text" class="inputus" value="'.getOneValue("useri", "id", $_SESSION['log_id'], "email").'" name="email"/>
								<br/><br/>
								<span class="star">*</span> '.L_YAHOO.':<br/>
								<input type="text" class="inputus" value="'.getOneValue("useri", "id", $_SESSION['log_id'], "ym").'" name="ym"/>
								<br/><br/>
								<span class="star">*</span> '.L_NUME.':<br/>
								<input type="text" class="inputus" value="'.getOneValue("useri", "id", $_SESSION['log_id'], "nume").'" name="nume"/>
								<br/><br/>
								'.L_DESCRIERE.':<br/>
								<textarea class="inputus" style="width:360px; margin-top:7px; height:100px;" name="descriere">'.getOneValue("useri", "id", $_SESSION['log_id'], "descriere").'</textarea>
								<br/><br/>
								'.L_POZA.':<br/>
								<input type="file" name="poza"/>
								'.$poza_ex.'
								<br/><br/>
								<input type="submit" class="menu" value="'.L_MODIFICA.'" name="ok"/>
							</td>
							<td valign="top" style="padding-left:30px;">
								<h2>'.L_SCHIMBA_PAROLA.'</h2>
								<span class="star">*</span> '.L_CONT_PAROLA_VECHE.':<br/>
								<input type="text" class="inputus" value="" size="70" name="parola_veche"/>
								<br/><br/>
								<span class="star">*</span> '.L_CONT_PAROLA_NOUA.':<br/>
								<input type="text" class="inputus" value="" name="parola_noua"/><br>
								<br/>
								<input type="submit" class="menu" value="'.L_CONT_SCHIMBA_PAROLA.'" name="parola" style="float:right;"/>
								<br class="clear"/><br/>
								<h2>'.L_STERGE_CONT.'</h2>
								
								<div class="error">'.L_CONT_OP_IREVERSIBILA.'</div>
								<br/>
								<input type="submit" class="menu menu_red" value="'.L_CONT_STERGE_CONT.'" name="sterge" style="background:red;" onclick="return confirm(\''.L_SIGUR.'?\');"/>
							</td>
						</tr>
					</table>

				</td>
			</tr>
		</table>
	</form>';
    
?>
</div>

<?php
$content = ob_get_clean();
ob_end_clean();
?>