<?php
/**
 * File mesaj.php
 * 
 * Allows editing the welcome message that users see on logging in.
 * The mentor can set the message for their mentee.
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */

ob_start();
acces(); 
if ($userismentor != "1") { header("location:/"); 
}
require "includes/fckeditor/fckeditor_php5.php";
?>

<div id="content">
    <h1><?php echo L_MESAJ_TITLU?></h1>
    <br/>
    <?php
    if(isset($_POST['ok'])) {
        $mesaj = diacritice(mysql_real_escape_string($_POST['mesaj']));
        
        if($mesaj == "") {
            echo'<span id="error">'.L_CAMPURI_INCOMPLETE.'!</span>';
        }
        else {
             // see if current user already has their own message
             $sql = mysql_query("SELECT * FROM setari WHERE user_id='" . $_SESSION['log_id'] . "'") or trigger_error(mysql_error(), E_USER_ERROR);
            if (mysql_num_rows($sql) == 0) {
                // INSERT
                mysql_query("INSERT INTO setari (user_id, mesaj, lang) VALUES ('".$_SESSION['log_id']."','$mesaj','".$limba."')");
            } 
            else {
                 // UPDATE
                 mysql_query("UPDATE setari SET mesaj='$mesaj' WHERE user_id='".$_SESSION['log_id']."'");
            }
            echo'<span id="done">'.L_CU_SUCCES.'!</span>';
        }
    }
    else {
    }
    
    $mentor_message_text = getOneValue("setari", "user_id", $_SESSION["log_id"], "mesaj");
    echo'<form method="post" action="">
	<table cellspacing="2" cellpadding="4" style="margin-top:5px;">
		<tr>
			<td>';
    $oFCKeditor = new FCKeditor('mesaj');
    $oFCKeditor->BasePath = 'includes/fckeditor/' ;
    $oFCKeditor->Width     = 955;
    $oFCKeditor->Height     = 300;
    $oFCKeditor->Value     = $mentor_message_text;
    $oFCKeditor->Create();
    echo'</td>
		</tr>
		<tr>
			<td><input type="submit" name="ok" value="'.L_MODIFICA.'" class="menu menu_active"/></td>
		</tr>
	</table>
	</form>';
?>
</div>

<?php
$content = ob_get_clean();
ob_end_clean();
?>