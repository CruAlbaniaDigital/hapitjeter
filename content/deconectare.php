<?php
/**
 * File deconnectare.php
 * 
 * Logs a user out by way of the big red button top right of the screen.
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */

session_destroy();
unset($_SESSION['logged']);
unset($_SESSION['log_id']);
header("location:" . $url_absolut);
?>