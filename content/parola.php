<?php
/**
 * File parola.php
 * 
 * Forgot password form
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */

ob_start();
?>

<?php
function random_password( $length = 8 ) 
{
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
    $password = substr(str_shuffle($chars), 0, $length);
    return $password;
}
?>

<div id="content">
    <h1><?php echo L_PASS_TITLU?></h1>
    <br/>
    <form method="post" action="">
        <table width="400" cellpadding="5">
            <tr>
                <td colspan="2">
        <?php
        if(isset($_POST['ok'])) {
            $email = mysql_real_escape_string($_POST['email']);
                        
            if($email == "") {
                echo'<span id="error">'.L_CAMPURI_INCOMPLETE.'!</span>';
            }
            else {
                $sql= mysql_query("SELECT * FROM useri WHERE email='$email' LIMIT 0,1") or trigger_error(mysql_error(), E_USER_ERROR);
                if(mysql_num_rows($sql) == 0) {
                    echo'<span id="error">'.L_LOG_GRESIT.'!</span>';
                }
                else {
                    $row = mysql_fetch_object($sql);
                                
                    $text = str_replace("[nume]", $row->nume, L_PASS_EMAIL);
                    $text = str_replace("[url]", $url_absolut, $text);
                    $newpass = random_password(8);
                    mysql_query("UPDATE useri SET parola='".md5($newpass)."' WHERE email='$email'");
                    $text = str_replace("[parola]", $newpass, $text);
                                
                    sendHTMLemail($text, "no-reply@hapitjeter.net", $row->email, L_PASS_TITLU);
                                
                    echo'<span id="done">'.L_PASS_SUCCES.'!</span>';
                }
                mysql_free_result($sql);
            }
        }
        else {
        }
        ?>
                </td>
            </tr>
            <tr>
                <td width="100"><?php echo L_EMAIL?>:</td>
                <td><input type="text" class="inputus" size="40" name="email"/></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" class="menu menu_active" value="<?php echo L_PASS_TRIMITE?>" name="ok"/><br/><a href="index.php?act=login" style="text-decoration:none;">&raquo; <?php echo L_LOG_CONECTARE?></a></td>
            </tr>
        </table>
    </form>
</div>

<?php
$content = ob_get_clean();
ob_end_clean();
?>