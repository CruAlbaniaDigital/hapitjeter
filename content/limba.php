<?php
/**
 * Limba.php File
 * 
 * Language selection form, included from ../index.php
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */

ob_start();
acces(); 
if(nivel($_SESSION['log_id']) != 5) { header("location:/"); 
}
require "includes/fckeditor/fckeditor_php5.php";

?>

<div id="content">
    <h1><?php echo L_LIM_TITLU?></h1>
    <br/>
    <?php

    if(isset($_POST['ok'])) {

        $limba = mysql_real_escape_string($_POST['limba']);
        
        mysql_query("UPDATE setari SET lang='$limba' WHERE id='1'");
        echo'<span id="done">'.L_CU_SUCCES.'!</span>';
    
    }
    else {
    }
    
    echo'<form method="post" action="">
	<table cellspacing="2" cellpadding="4" style="margin-top:5px;">
		<tr>
			<td>'.L_LIM_ALEGE.':</td>
			<td><select name="limba" class="selectus">';
    $default = getOneValue("setari", "id", 1, "lang");
    if ($handle = opendir('includes/lang/')) {
        while (false !== ($file = readdir($handle))) {
            if ($file != "." && $file != "..") {
                $file = str_replace(".php", "", $file);
                if($file == $default) { $sel=" selected"; 
                } else { $sel=""; 
                }
                echo'<option value="'.$file.'"'.$sel.'>'.strtoupper($file).'</option>';
            }
        }
        closedir($handle);
    }
    echo'</select></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit" name="ok" value="'.L_MODIFICA.'" class="menu menu_active"/></td>
		</tr>
	</table>
	</form>';
    
?>
</div>

<?php
$content = ob_get_clean();
ob_end_clean();
?>