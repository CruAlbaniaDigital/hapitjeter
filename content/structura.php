<?php
/**
 * Structura.php File
 * 
 * Page displaying an org chart view of the user, his mentees, and their mentees.
 * This tree view gives an overview of the people that the user has mentored and 
 * their progress mentoring others.
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */

ob_start();
acces(); 
?>

<script type='text/javascript' src='http://www.google.com/jsapi'></script>
<script type='text/javascript'>
  google.load('visualization', '1', {packages:['orgchart']});
  google.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Name');
    data.addColumn('string', 'Manager');
    
    data.addRows([
    <?php
    $id = $_SESSION['log_id'];
    
    generate_tree($id);
    
    ?>
    [{v:'', f:''}, '']
    ]);
    var chart = new google.visualization.OrgChart(document.getElementById('structura'));
    
    chart.draw(data, {allowHtml:true,allowCollapse:true});
  }
</script>
<div id="content">
    <h1><?php echo L_STRUCTURA?></h1>
    <br/>
    <div align="center" id="structura"></div>
</div>

<?php
$content = ob_get_clean();
ob_end_clean();
?>