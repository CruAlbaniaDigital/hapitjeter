<?php
/**
 * Main.php File
 * 
 * Displays a welcome message after the user has registered, personalizing it with the user's name
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */

acces();
ob_start();
?>

<div id="content">
    <h1><?php echo L_MAIN_BUN?>, <?php echo getOneValue("useri", "id", $_SESSION['log_id'], "nume"); ?>!</h1>
    <br/>
    <?php echo L_MAIN_MESAJ?>
</div>

<?php
$content = ob_get_clean();
ob_end_clean();
?>