<?php
/**
 * File pagini.php
 * 
 * A view of the other "pages" of the application,
 * the .htaccess file in the root rewrites the other pages to this view.
 * 
 * Example:
 *   RewriteRule rreth_nesh.html$ index.php?act=pagini&pag=rreth_nesh [QSA,L]
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */

ob_start();
?>
<link rel="stylesheet" href="quizmc.css" type="text/css">
<script src="quizqa1.js" type="text/javascript"></script>
<script src="quizmc.js" type="text/javascript"></script>

<div id="content">
<?php
switch($_GET['pag']) {
case'rreth_nesh':
?>
<h1>Rreth Nesh</h1><br />
<span class="maintext">Ne jemi një grup ndjekësish të Jezusit, që kemi pasion të ndihmojmë të tjerët, të zbulojnë mrekullinë e një jete në besim! Pasioni ynë e ka origjinën te fakti që kemi zbuluar vetë bukurinë e mësimeve të Biblës; ndaj duam t'ju japim një mundësi edhe juve që të përjetoni të njëjtën gjë.
Faleminderit që keni ardhur te "Hapi Tjeter"! Na kontaktoni nëse keni nevojë për ndihmë!</span><br /><br />
        
    <?php break;
        
case'studimet':
?>
        
<h1>Studimet</h1>
<br/>
<p>Put Studimet Content Here</p>

    <?php break;
        
case'testet':
?>
        
<h1>Testet</h1>
<br><div id="quiz1" class="quiz">hello</div>

    <?php break;
        
case'kontakt':
?>
        
<h1>Kontakt</h1>
<br/>
<span class="maintext" style="float: left; display: inline;">Na dërgoni një mesazh më poshtë:</span>
<!-- Do not change the code! -->
<a id="foxyform_embed_link_729520" href="http://www.foxyform.com/">foxyform</a>
<script type="text/javascript">
(function(d, t){
var g = d.createElement(t),
s = d.getElementsByTagName(t)[0];
g.src = "http://www.foxyform.com/js.php?id=729520&sec_hash=12ecca13669&width=350px";
s.parentNode.insertBefore(g, s);
}(document, "script"));
</script>
<!-- Do not change the code! -->
        
    <?php break;
}
?>


        
</div>

<?php
$content = ob_get_clean();
ob_end_clean();
?>
