<?php
/**
 * Register.php File
 * 
 * Registration form - included from ../index.php
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */

if(is_logged()) { header("location:" . $url_relative . "index.php?act=main"); }
ob_start();

$mentor_id = getOneValue("useri", "user", $username, "id");
$message_text = getOneValue("setari", "user_id", $mentor_id, "mesaj");
$register_error = false;
?>

<div id="content">
  <h1><?php echo L_REG_INREGISTRARE?></h1>
  <?php if ($message_text) {
  echo '<div style="float: left; display: inline; height: 84px; margin-bottom: 74px; width: 100%;">';
  $sql = mysql_query("SELECT poza FROM useri WHERE id='".$mentor_id."'");
  if(mysql_num_rows($sql) == 1) { $row = mysql_fetch_object($sql); $mentor_pic_file = "/upload/fckfiles/".$row->poza; }
  if (file_exists($mentor_pic_file)) { echo '<h4 style="margin-bottom: 5px;">'.L_MENTOR.': '.$username.'</h4><img style="height: 80px; float: left; margin-right:10px; border: 2px solid black;" src="'.$mentor_pic_file.'" alt="" />'; }
  echo $message_text . '</div>';
  } ?>
  
  <form method="post">
    <div class="cc-selector">
    <span style="float: left; display: inline; margin-right:10px; font-size: 18px; font-weight: bold;">Zgjidh<br /><?php echo L_TESTE; ?>:</span>
<?php // Get all the series
        $sql0 = mysql_query("SELECT titlu, nivel FROM teste ORDER BY nivel");
        if(mysql_num_rows($sql0) > 0) {
          $i = 1;
          while($row0 = mysql_fetch_object($sql0)) {
            if ($i <= 2) {
              echo '<div class="series"><div class="series_pic"><input';
              if ((($i == 1) && ($_GET["seria"] == "")) || ($_GET["seria"] == $i)) { echo ' checked="checked"'; }
                echo ' id="series'.$row0->nivel.'" type="radio" name="selected_series" value="'.$row0->nivel.'" ';
                echo '/><label class="drinkcard-cc series'.$row0->nivel.'" for="series'.$row0->nivel.'">'.$row0->titlu.'</label></div><div class="series_level">NIVEL '.$row0->nivel.'</div>';
              echo '</div>';
            }
            $i++;
          }
        }
?>  </div>

      <table align="center" cellpadding="5" style="min-width:500px;">
      <tr>
        <td colspan="2">
          <?php
          $allow=1;
          $username = mysql_real_escape_string($_GET['username']);
          
          if($username == "register") {
            //allow
          }
          else {
            $nivel = getOneValue("useri", "user", $username, "nivel");
            if($username == "" OR $nivel == 1) {
              $allow=0;
            }
            else {
              $username = mysql_real_escape_string($_GET['username']);
              $parent_id = getOneValue("useri", "user", $username, "id");
              if(($parent_id == "0") OR ($parent_id == "")) {
                $allow=0;
              }
              else {
                //allow
              }
            }
          }
          
          if($allow == 1) {
            if(isset($_POST['ok'])) {
                
              $mentor = mysql_real_escape_string($_POST['mentor']);
              $mentor_username = getOneValue("useri", "id", $mentor, "user");
              $selected_series = mysql_real_escape_string($_POST['selected_series']);
              $user = mysql_real_escape_string($_POST['user']);
              $parola = mysql_real_escape_string($_POST['parola']);
              $parola2 = mysql_real_escape_string($_POST['parola2']);
              $email = mysql_real_escape_string($_POST['email']);
              $email2 = mysql_real_escape_string($_POST['email2']);
              $ym = mysql_real_escape_string($_POST['ym']);
              $nume = diacritice(mysql_real_escape_string($_POST['nume']));
              $parent_id = mysql_real_escape_string($_POST['parent_id']);
              $varsta = mysql_real_escape_string($_POST['varsta']);
              $oras = mysql_real_escape_string($_POST['oras']);
              $profesie = diacritice(mysql_real_escape_string($_POST['profesie']));
              $telefon = mysql_real_escape_string($_POST['telefon']);
              $de_unde = diacritice(mysql_real_escape_string($_POST['de_unde']));
              $mesaj = diacritice(mysql_real_escape_string($_POST['mesaj']));
              $antispam = mysql_real_escape_string($_POST['antispam']);
              
              $_SESSION['mentor'] = $mentor;
              $_SESSION['r_user'] = $user;
              $_SESSION['r_nume'] = $nume;
              $_SESSION['r_email'] = $email;
              $_SESSION['r_email2'] = $email2;
              $_SESSION['r_ym'] = $ym;
              $_SESSION['r_telefon'] = $telefon;
              $_SESSION['r_varsta'] = $varsta;
              $_SESSION['r_oras'] = $oras;
              $_SESSION['r_profesie'] = $profesie;
              $_SESSION['r_de_unde'] = $de_unde;
              $_SESSION['r_mesaj'] = $mesaj;
                      
              if(($user == "") OR ($parola == "") OR ($parola2 != $parola) OR !is_valid($email) OR ($email2 != $email) OR ($nume == "") OR ($parent_id == "") OR ($varsta == "") OR ($oras == "") OR ($profesie == "") OR ($telefon == "") OR ($de_unde == "") OR ($antispam != $_SESSION['antispam'])) {
                
                echo'<span id="error">'.L_REG_ERROR.':
                <ul>';
                if($user == "") { echo'<li>'.L_REG_ERROR_NICK.'!</li>'; }
                if($parola != $parola2) { echo'<li>'.L_REG_ERROR_PAROLA.'!</li>'; }
                if($email != $email2) { echo'<li>'.L_REG_ERROR_EMAIL.'!</li>'; }
                if($nume == "") { echo'<li>'.L_REG_ERROR_NUME.'!</li>'; }
                if($varsta == "") { echo'<li>'.L_REG_ERROR_VARSTA.'!</li>'; }
                if($oras == "") { echo'<li>'.L_REG_ERROR_ORAS.'!</li>'; }
                if($profesie == "") { echo'<li>'.L_REG_ERROR_PROFESIE.'!</li>'; }
                if($telefon == "") { echo'<li>'.L_REG_ERROR_TELEFON.'!</li>'; }
                if($de_unde == "") { echo'<li>'.L_REG_ERROR_REF.'!</li>'; }
                if($parent_id == "") { echo'<li>'.L_REG_ERROR_PARENT.'!</li>'; }
                if($antispam != $_SESSION['antispam']) { echo'<li>'.L_REG_ERROR_SPAM.'!</li>'; }
                echo'</ul>
                </span>
                ';
                
              }
              else {
                $sql= mysql_query("SELECT id FROM useri WHERE user='$user' LIMIT 0,1") or trigger_error(mysql_error(), E_USER_ERROR);
                if(mysql_num_rows($sql) == 0) {
                  $starting_nivel = 1;
                  if ($selected_series == "2") { $starting_nivel = 2; }
                  mysql_query("INSERT INTO useri SET user='$user',parola='".md5($parola)."',email='$email',nume='$nume',parent_id='$parent_id',nivel='$starting_nivel',varsta='$varsta',oras='$oras',profesie='$profesie',telefon='$telefon',de_unde='$de_unde',ym='$ym'") or trigger_error(mysql_error(), E_USER_ERROR);
                  
                  // trimitere mail parinte
                  $text=''.L_REG_MESAJ_EMAIL.'<hr/>Mentor: '.$mentor_username.'<br/>'.L_REG_NUME.': '.$nume.'<br/>'.L_REG_EMAIL.': '.$email.'<br/>'.L_YAHOO.': '.$ym.'<br/>'.L_REG_TELEFON.': '.$telefon.'<br/>'.L_REG_VARSTA.': '.$varsta.'<br/>'.L_REG_ORAS.': '.$oras.'<br/>'.L_REG_PROFESIE.': '.$profesie.'<br/>'.L_REG_MESAJ.': '.$mesaj.'<hr/>'.L_REG_REF.': '.$de_unde.'<hr/>';
                  $from = 'no-reply@hapitjeter.net';
                  $to = getOneValue("useri", "id", $parent_id, "email");
                  $subject = ''.L_REG_MESAJ_EMAIL.' ['.DENUMIRE.']';
                  sendHTMLemail($text, $from, $to, $subject);
                  // send admin email as well (JDV)
                  $adminEmail = getOneValue("useri", "id", 1, "email");
                  sendHTMLemail($text, $from, $adminEmail, $subject);
                  // end mail
                  
                  $_SESSION['logged'] = 'Yes';
                  $_SESSION['log_id'] = mysql_insert_id();
                  $azi = mktime();
                  $last_id = $_SESSION['log_id'];
                  mysql_query("UPDATE useri SET last_login='$azi' WHERE id='$last_id'");
                  
                  unset($_SESSION['mentor']);
                  unset($_SESSION['r_user']);
                  unset($_SESSION['r_nume']);
                  unset($_SESSION['r_email']);
                  unset($_SESSION['r_email2']);
                  unset($_SESSION['r_ym']);
                  unset($_SESSION['r_telefon']);
                  unset($_SESSION['r_varsta']);
                  unset($_SESSION['r_oras']);
                  unset($_SESSION['r_profesie']);
                  unset($_SESSION['r_de_unde']);
                  unset($_SESSION['r_mesaj']);
                  header("location:" . $url_absolut . "index.php");
                }
                else {
                  echo'<span id="error">'.L_REG_ERROR_NICK.'!</span>';
                }
                mysql_free_result($sql);
              }
            }
            else {}
          }
          else {
            $register_error = true;
            echo'<span id="error">'.L_REG_NU.'!</span>';
          }
          
          $nr1 = rand(0, 20);
          $nr2 = rand(0, 20);
          $_SESSION['antispam'] = $nr1+$nr2;
          ?>
        </td>
      </tr>
      <?php
      if($username == "register") {
        echo'
        <tr>
          <td>'.L_REG_MENTOR.': <span style="color:red;">*</span></td>
          <td><select name="mentor" class="selectus" style="width:200px;" onchange="info_mentor(this.value)">
              <option value="">- -</option>';
        
        $sql1 = mysql_query("SELECT nume, id FROM useri WHERE afis_mentor=1 and id <> '1' AND user <> 'test_mentor' ORDER BY nume ASC");
        if(mysql_num_rows($sql1) > 0) {
          while($row1 = mysql_fetch_object($sql1)) {
            if($_SESSION['mentor'] == $row1->id) { $sel=" selected"; } else { $sel=""; }
            echo'<option value="'.$row1->id.'"'.$sel.'>'.$row1->nume.'</option>';
          }
        }
        mysql_free_result($sql1);
        
          echo'</select></td>
        </tr>
        <tr>
          <td colspan="2"><span style="color:#666; font-size:16px;" id="mentor_info">'.L_REG_MENTOR_WHY.'</span></td></td>
        </tr>';
          
        if(isset($_SESSION['mentor'])) { 
          $parent_id = $_SESSION['mentor']; 
          echo'<script>info_mentor('.$parent_id.')</script>';
        }
      }
      
      if (!$register_error) {
      ?>
      <tr>
        <td><?php echo L_REG_NUME?>: <span style="color:red;">*</span></td>
        <td><input type="text" class="inputus" size="40" name="nume" value="<?php echo $_SESSION['r_nume']; ?>"/></td>
      </tr>
      <tr>
        <td><?php echo L_REG_NICK?>: <span style="color:red;">*</span></td>
        <td><input type="text" class="inputus" size="40" name="user" value="<?php echo $_SESSION['r_user']; ?>"/></td>
      </tr>
      <tr>
        <td><?php echo L_REG_PAROLA?>: <span style="color:red;">*</span></td>
        <td><input type="password" class="inputus" size="40" name="parola"/></td>
      </tr>
      <tr>
        <td><?php echo L_REG_PAROLA2?>: <span style="color:red;">*</span></td>
        <td><input type="password" class="inputus" size="40" name="parola2"/></td>
      </tr>
      <tr>
        <td><?php echo L_REG_EMAIL?>: <span style="color:red;">*</span></td>
        <td><input type="text" class="inputus" size="40" name="email" value="<?php echo $_SESSION['r_email2']; ?>"/></td>
      </tr>
      <tr>
        <td><?php echo L_REG_EMAIL2?>: <span style="color:red;">*</span></td>
        <td><input type="text" class="inputus" size="40" name="email2" value="<?php echo $_SESSION['r_email2']; ?>"/></td>
      </tr>
      <tr>
        <td><?php echo L_YAHOO?>:</td>
        <td><input type="text" class="inputus" size="40" name="ym" value="<?php echo $_SESSION['ym']; ?>"/></td>
      </tr>
      <tr>
        <td><?php echo L_REG_TELEFON?>: <span style="color:red;">*</span></td>
        <td><input type="text" class="inputus" size="40" name="telefon" value="<?php echo $_SESSION['r_telefon']; ?>"/></td>
      </tr>
      <tr>
        <td><?php echo L_REG_VARSTA?>: <span style="color:red;">*</span></td>
        <td><input type="text" class="inputus" size="2" name="varsta" value="<?php echo $_SESSION['r_varsta']; ?>"/></td>
      </tr>
      <tr>
        <td><?php echo L_REG_ORAS?>: <span style="color:red;">*</span></td>
        <td><input type="text" class="inputus" size="40" name="oras" value="<?php echo $_SESSION['r_oras']; ?>"/></td>
      </tr>
      <tr>
        <td><?php echo L_REG_PROFESIE?>: <span style="color:red;">*</span></td>
        <td><input type="text" class="inputus" size="40" name="profesie" value="<?php echo $_SESSION['r_profesie']; ?>"/></td>
      </tr>
      <tr>
        <td><?php echo L_REG_REF?>? <span style="color:red;">*</span></td>
        <td><input type="text" class="inputus" size="40" name="de_unde" value="<?php echo $_SESSION['r_de_unde']; ?>"/></td>
      </tr>
      <tr>
        <td valign="top"><?php echo L_REG_MESAJ?>:<br/><i>(<?php echo L_REG_OPTIONAL?>)</i></td>
        <td><textarea name="mesaj" style="width:305px; height:70px;" class="inputus"><?php echo $_SESSION['r_mesaj']; ?></textarea></td>
      </tr>
      <tr>
        <td>Antispam: <span style="color:red;">*</span></td>
        <td><?php echo $nr1; ?> + <?php echo $nr2; ?> = <input type="text" class="inputus" size="2" name="antispam" style="width:30px;"/></td>
      </tr>
      <tr>
        <td><input type="hidden" name="parent_id" value="<?php echo $parent_id; ?>"/></td>
        <td><input type="submit" class="menu menu_active" value="<?php echo L_REG_INREGISTRARE?>" name="ok"/></td>
      </tr>
      <?php } // end if register error ?>
    </table>
  </form>
</div>

<?php
$content = ob_get_clean();
ob_end_clean();
?>