<?php
/**
 * File login.php
 * 
 * The default view - responds to POST requests including the username and password.
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */
ob_start();

if(is_logged()) { header("location:/"); 
}

if(isset($_POST['ok'])) {
        $user = mysql_real_escape_string($_POST['user']);
        $parola = mysql_real_escape_string($_POST['parola']);
        
    if(($user == "") || ($parola == "")) {
        echo'<span id="error">'.L_CAMPURI_INCOMPLETE.'!</span>';
                                
    }
    else {
        $sql= mysql_query("SELECT id, parent_id FROM useri WHERE user='$user' AND parola='".md5($parola)."' LIMIT 0,1") or trigger_error(mysql_error(), E_USER_ERROR);
        if(mysql_num_rows($sql) == 0) {
            echo'<span id="error">'.L_LOG_GRESIT.'!</span>';
        }
        else {
            $row = mysql_fetch_object($sql);
            $_SESSION['logged'] = 'Yes';
            $_SESSION['log_id'] = $row->id;
            $azi = mktime();
            mysql_query("UPDATE useri SET last_login='$azi' WHERE id='$row->id'");
            if ($_POST["new_nivel"] == "") {
                header("location:index.php");
            }
            else {
                header("location:index.php?new_nivel=".$_POST["new_nivel"]);
            }
        }
        mysql_free_result($sql);
    }
}
else {
}

$register_path = $url_relative . "register"

?>
<div id="main">
        <form method="post" action="">
        
        <div id="login">
                <h1><?php echo L_LOG_CONECTARE?></h1>
                <div class="link_main"><a href="<?php echo $register_path ?>"><?php echo L_REGISTER?></a>&nbsp;&nbsp;&nbsp;<a href="?act=parola"><?php echo ucfirst(L_LOG_RECUPERARE)?></a></div>
                <input type="text" name="user" class="login_name" placeholder="<?php echo L_LOG_NICKNAME?>"/>
                <input type="password" name="parola" class="login_pass" placeholder="<?php echo L_LOG_PAROLA?>"/>
                <input type="submit" class="enter-btn" placeholder="<?php echo L_LOG_CONECTARE?>" name="ok"/>
                                <?php if ($_GET["new_nivel"] != "") { ?>
                                <input type="hidden" name="new_nivel" id="new_nivel" value="<?php echo (int)($_GET["new_nivel"]); ?>" />
                                <?php } ?>
        </div>
        
        </form>
</div>

<div id="content" style="padding: 0;">
<div class="maintext" style="width: 920px; padding: 20px; padding-top:30px; margin: 0 auto;">
<a id="register-button" href="<?php echo $register_path ?>">Regjistrohu</a><h1 style="font-size:38px;">Mirë se erdhe te "Hapi Tjetër"!</h1>
<h3>Cili është hapi tjetër?</h3>
<img style="float:left; margin-top:5px; margin-right:20px; width: 200px;" src="images/cell-hapitjeter200.jpg" alt="">
Nëse ke interes të dish bindjet bazë të besimit të mësimeve të Jezusit, atëherë ke ardhur në vendin e duhur!
Nëse je i/e bindur që të ndjekësh mësimet e Jezusit, por nuk e di se cili është hapi tjetër, je në vendin e duhur!
Ne po ofrojmë një kurs të shkurtër studimi për ty; të lutem rregjistrohu, lexo materialet, përgjigju një testi të shkurtër rreth dijeve që ke marrë, dhe kalo në mësimin tjetër. I gjithë procesi është në dorën tënde - ne sigurojmë një mentor/mbikqyrës që të jetë ndihmë për ty në këtë udhëtim.
Shpresojmë që do të kesh një udhëtim të mirë, që jo vetëm shton njohuritë rreth besimit të krishterë, por më e rëndësishmja, pasuron jetën tënde të përditshme...
Udhëtim të këndshëm!<span style="float: right;"><em>- Stafi i hapitjeter.net</em></span></span>
</div>
<br class="clear"/>

<div class="homepage-series" style="background-image:url('images/stoneback1600.jpg');">
<?php // Get all the series and put them into links
              $sql0 = mysql_query("SELECT titlu, nivel, descriere FROM teste ORDER BY nivel");
if(mysql_num_rows($sql0) > 0) {
    $row1 = mysql_fetch_object($sql0);
    $seriestitle1 = $row1->titlu;
    $seriesdesc1 = $row1->descriere;
    $row2 = mysql_fetch_object($sql0);
    $seriestitle2 = $row2->titlu;
    $seriesdesc2 = $row2->descriere;                    
}
?>
<div style="width: 960px; padding-top: 105px; margin: 0 auto;">
<div class="homepage-seriesleft">
<h1>SERIA 1</h1>
<div class="homepage-seriespic"><a class="popup-youtube" href="http://www.youtube.com/watch?v=05l6KLgYXok"><img src="images/playbutton.png" alt=""></a></div>
</div>
<script>
$(document).ready(function() {
    $('.popup-youtube').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,

        fixedContentPos: false
    });
});
</script>
<div class="homepage-seriesright">
<div class="seriestitle"><a href="<?php echo $register_path ?>?seria=1" title="Registrohu"><?php echo $seriestitle1; ?></a></div>
<?php echo $seriesdesc1; ?>
<br /><br />
<span class="serieslink"><a href="<?php echo $register_path ?>?seria=1" title="Registrohu" style="">Filloni Serinë</a></span>
</div>
</div>
</div>

<div class="homepage-series" style="background-image:url('images/waterback2.jpg'); margin-top:0;">
<div style="width: 960px; padding-top: 105px; margin: 0 auto;">
<div class="homepage-seriesleft">
<h1>SERIA 2</h1>
<a href="<?php echo $register_path ?>?seria=2" title="Registrohu"><img style="border: 2px solid #D0352C;" src="images/life.jpg" alt=""></a>
</div>
<div class="homepage-seriesright">
<div class="seriestitle"><a href="<?php echo $register_path ?>?seria=2" title="Registrohu"><?php echo $seriestitle2; ?></a></div>
<?php echo $seriesdesc2; ?>
<br /><br />
<span class="serieslink"><a href="<?php echo $register_path ?>?seria=2" title="Registrohu" style="">Filloni Serinë</a></span>
</div>
</div>
</div>
  <br class="clear"/>
<?php
$content = ob_get_clean();
ob_end_clean();
?>
