<?php
/**
 * File intrebari.php
 * 
 * Editor for editing lesson questions
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */
ob_start();
acces(); 
if(nivel($_SESSION['log_id']) != 5) { header("location:/"); 
}
require "includes/fckeditor/fckeditor_php5.php";
$id_lectie = mysql_real_escape_string($_GET['id_lectie']);
$lectie = getOneValue("lectii", "id", $id_lectie, "titlu");
?>

<div id="content">
    <h1><?php echo L_ASK_PENTRU?> <i style="font-size:22px;"><?php echo $lectie; ?></i> &raquo; <a href="?act=intrebari&op=add&id_lectie=<?php echo $id_lectie;?>"><?php echo L_ASK_NOUA?></a></h1>
    <br/>
    
    <?php
    echo $_SESSION['mesaj'];
    unset($_SESSION['mesaj']);
    switch($_GET['op']) {
    case'':
        
        if(!isset($_GET["page"]) ) { $page = 1; 
        } else { $page = mysql_real_escape_string($_GET["page"]); 
        }    
        $q = "SELECT * FROM intrebari WHERE id_lectie='$id_lectie' ORDER BY ord ASC";
        $limit = 20;
        $query = mysql_query($q) or trigger_error(mysql_error(), E_USER_ERROR);
        $results= mysql_num_rows($query);
        $nrpages = ceil($results/$limit);
        $limitvalue = $page * $limit - ($limit); 
        $sql = mysql_query("$q LIMIT $limitvalue, $limit") or trigger_error(mysql_error(), E_USER_ERROR);
        $dela = $limitvalue +1 ;
        $la   = $limitvalue + $limit;
        if ($la > $results ) { $la = $results ; 
        }
        if(mysql_num_rows($sql) == 0) {
        }
        else {
             echo'
			<table id="nice_table" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td width="10"></td>
					<td class="tbb"><b>'.L_ASK_INTREBARE.'</b></td>
					<td class="tbb" width="40"><b>Ord</b></td>
					<td class="tbl tbb" width="80"><b>Op.</b></td>
					<td width="10"></td>
				</tr>';
            while($row = mysql_fetch_object($sql)) {
                echo'<tr>
						<td width="10"></td>
						<td>'.stripslashes($row->titlu).' &raquo; <a href="?act=raspunsuri&id_intrebare='.$row->id.'">'.L_ASK_RASPUNSURI.'</a></td>
						<td align="center">'.$row->ord.'</td>
						<td align="center">
							<a href="?act=intrebari&op=edit&id='.$row->id.'&id_lectie='.$id_lectie.'"><img src="images/edit.gif"/></a>
							<a href="?act=intrebari&op=delete&id='.$row->id.'&id_lectie='.$id_lectie.'" onclick="return confirm(\''.L_SIGUR.'?\');"><img src="images/delete.png"/></a>
						</td>
						<td width="10"></td>
					</tr>';
            }
             $inapoi  = max($page-1, 1);
             $inainte = min($page+1, $nrpages);
            
            if($page == $inapoi) { $inapoi = '';
            } else { 
                 $inapoi = '<span style="font-size:11px;">&laquo;</span> <a href="index.php?page='.$inapoi.'&act=intrebari&id_lectie='.$id_lectie.'">'.L_PREV.'</a>&nbsp;&nbsp;';
            }
            if($page == $inainte) { $inainte = '';
            } else { 
                 $inainte = '&nbsp;&nbsp;<a href="index.php?page='.$inainte.'&act=intrebari&id_lectie='.$id_lectie.'">'.L_NEXT.'</a> <span style="font-size:11px;">&raquo;</span>';
            }
             echo'<tr><td width="10"></td><td colspan="3">'.$inapoi.'<b>'.$page.'</b> '.L_DIN.' <b>'.$nrpages.'</b>'.$inainte.'</td><td width="10"></td></tr>';
             echo'</table>';
        }
        mysql_free_result($sql);
        break;
        
    case'add':
        echo'&raquo; <a href="?act=intrebari&id_lectie='.$id_lectie.'">'.L_INAPOI_INTREBARI.'</a><br/><br/>';
        if(isset($_POST['ok'])) {
             $id_lectie = mysql_real_escape_string($_POST['id_lectie']);
             $ord = mysql_real_escape_string($_POST['ord']);
             $titlu = diacritice(mysql_real_escape_string($_POST['titlu']));
             $descriere = diacritice(mysql_real_escape_string($_POST['descriere']));
             $tip = mysql_real_escape_string($_POST['tip']);
             $raspuns_sugerat = diacritice(mysql_real_escape_string($_POST['raspuns_sugerat']));
            
            if(($id_lectie == "") OR ($titlu == "")) {
                echo'<span id="error">'.L_CAMPURI_INCOMPLETE.'!</span>';
            }
            else {
                mysql_query("INSERT INTO intrebari SET id_lectie='$id_lectie',titlu='$titlu',descriere='$descriere',tip='$tip',ord='$ord',raspuns_sugerat='$raspuns_sugerat'");
                echo'<span id="done">'.L_ASK_SUCCES.'!</span>';
            }
            
            
        }
        else {
        }
        
        echo'<form method="post" action="">
		<table cellspacing="2" cellpadding="4" style="margin-top:5px;">
			<tr>
				<td width="100">'.L_ASK_LECTIE.':</td>
				<td><select name="id_lectie" class="selectus">
						<option value=""> -- </option>';
        $sql = mysql_query("SELECT id,titlu FROM lectii ORDER BY id DESC") or trigger_error(mysql_error(), E_USER_ERROR);
        if(mysql_num_rows($sql) == 0) {
        }
        else {
            while($row = mysql_fetch_object($sql)) {
                if($row->id == $id_lectie) { $sel=" selected"; 
                } else {$sel="";
                }
                echo'<option value="'.$row->id.'"'.$sel.'>'.$row->titlu.'</option>';
            }
        }
        mysql_free_result($sql);
        echo'</select></td>
			</tr>
			<tr>
				<td>'.L_ASK_INTREBARE.':</td>
				<td><input type="text" class="inputus" size="80" name="titlu" value=""/></td>
			</tr>
			<tr>
				<td valign="top">'.L_ASK_DESCRIERE.'</td>
				<td>';
        $oFCKeditor = new FCKeditor('descriere');
        $oFCKeditor->BasePath = 'includes/fckeditor/' ;
        $oFCKeditor->Width     = 800;
        $oFCKeditor->Height     = 400;
        $oFCKeditor->Create();
        echo'</textarea></td>
			</tr>
			<tr>
				<td>'.L_ASK_TIP.':</td>
				<td>'.tip_intrebare('', 1).'</td>
			</tr>
			<tr>
				<td>Ord:</td>
				<td><input type="text" class="inputus" style="width:20px;" name="ord" value=""/> <i>('.L_ASK_HINT.')</i></td>
			</tr>
			<tr>
				<td>'.L_ASK_SUGERAT.':</td>
				<td><input type="text" class="inputus" size="80" name="raspuns_sugerat" value=""/></td>
			</tr>
			<tr>
				<td><input type="hidden" name="id_lectie" value="'.$id_lectie.'"/></td>
				<td><input type="submit" name="ok" value="'.L_ADAUGA.'" class="menu"/></td>
			</tr>
		</table>
		</form>';
        break;
        
    case'edit':
        echo'&raquo; <a href="?act=intrebari&id_lectie='.$id_lectie.'">'.L_INAPOI_INTREBARI.'</a><br/><br/>';
        if(isset($_POST['ok'])) {
            
             $titlu = diacritice(mysql_real_escape_string($_POST['titlu']));
             $descriere = diacritice(mysql_real_escape_string($_POST['descriere']));
             $id = mysql_real_escape_string($_POST['id']);
             $tip = mysql_real_escape_string($_POST['tip']);
             $ord = mysql_real_escape_string($_POST['ord']);
             $raspuns_sugerat = diacritice(mysql_real_escape_string($_POST['raspuns_sugerat']));
            
            if($titlu == "") {
                echo'<span id="error">'.L_CAMPURI_INCOMPLETE.'!</span>';
            }
            else {
                mysql_query("UPDATE intrebari SET titlu='$titlu',descriere='$descriere',tip='$tip',ord='$ord',raspuns_sugerat='$raspuns_sugerat' WHERE id='$id'");
                echo'<span id="done">'.L_CU_SUCCES.'!</span>';
            }
            
            
        }
        else {
        }
        
        $id = mysql_real_escape_string($_GET['id']);
        $sql = mysql_query("SELECT * FROM intrebari WHERE id='$id'");
        $row = mysql_fetch_object($sql);
        echo'<form method="post" action="">
		<table cellspacing="2" cellpadding="4" style="margin-top:5px;">
			<tr>
				<td width="100">'.L_ASK_TIP.':</td>
				<td>'.tip_intrebare($row->tip, 1).'</td>
			</tr>
			<tr>
				<td>'.L_ASK_INTREBARE.':</td>
				<td><input type="text" class="inputus" size="80" name="titlu" value="'.stripslashes($row->titlu).'"/></td>
			</tr>
			<tr>
				<td valign="top">'.L_ASK_DESCRIERE.'</td>
				<td>';
        $oFCKeditor = new FCKeditor('descriere');
        $oFCKeditor->BasePath = 'includes/fckeditor/' ;
        $oFCKeditor->Width     = 800;
        $oFCKeditor->Height     = 400;
        $oFCKeditor->Value     = stripslashes($row->descriere);
        $oFCKeditor->Create();
        echo'</td>
			</tr>
			<tr>
				<td>Ord:</td>
				<td><input type="text" class="inputus" style="width:20px;" name="ord" value="'.$row->ord.'"/> <i>('.L_ASK_HINT.')</i></td>
			</tr>
			<tr>
				<td>'.L_ASK_SUGERAT.':</td>
				<td><input type="text" class="inputus" size="80" name="raspuns_sugerat" value="'.stripslashes($row->raspuns_sugerat).'"/></td>
			</tr>
			<tr>
				<td><input type="hidden" name="id" value="'.$id.'"/></td>
				<td><input type="submit" name="ok" value="'.L_MODIFICA.'" class="menu"/></td>
			</tr>
		</table>
		</form>';
        break;
        
    case'delete':
        $id = mysql_real_escape_string($_GET['id']);
        sterge_intrebare($id);
        $_SESSION['mesaj'] = '<span id="done">'.L_ASK_STERS.'!</span>';
        header("location:index.php?act=intrebari&id_lectie=$id_lectie");
        break;
        
    }
    ?>
    
</div>

<?php
$content = ob_get_clean();
ob_end_clean();
?>