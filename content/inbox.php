<?php
/**
 * File index.php
 * 
 * Page displaying a user's private messages from his mentor or another user
 * Allows sending messages to the mentor or mentees.
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */

ob_start();
acces(); 

if(!isset($_GET["tip"]) ) { $tip = "primite"; 
} else { $tip = $_GET['tip']; 
}
switch($tip) {
case'primite':
    $p_activ = 'bold';
    $t_activ='normal';
    $cine = L_INBOX_EXPEDITOR;
    $ce = "id_expeditor";
    $ce_read = "id_destinatar";
    break;
    
case'trimise':
    $p_activ = 'normal';
    $t_activ='bold';
    $cine = L_INBOX_DESTINATAR;
    $ce = "id_destinatar";
    $ce_read = "id_expeditor";
    break;
    
default:
    $p_activ = 'normal';
    $t_activ='normal';
    break;
}

if($_GET['op'] == "compose") { $c_activ='bold'; $p_activ='normal;'; 
} else {$c_activ='normal'; 
}
?>

<div id="content">

    <h1><?php echo L_MESAGERIE?></h1>
    <br/>
    
    <table width="100%">
            <tr>
                <td valign="top" id="submenu_msg" width="70">
                    <ul>
                        <li><a href="?act=inbox&op=compose" style="font-weight:<?php echo $c_activ?>">Mesazh i ri</a></li>
                        <li><a href="?act=inbox&tip=primite" style="font-weight:<?php echo $p_activ?>"><?php echo L_INBOX_PRIMITE?></a></li>
                        <li><a href="?act=inbox&tip=trimise" style="font-weight:<?php echo $t_activ?>"><?php echo L_INBOX_TRIMISE?></a></li>
                    </ul>
                </td>
                <td valign="top" style="padding-left:60px;">
                
        <?php
        echo $_SESSION['mesaj'];
        unset($_SESSION['mesaj']);
        switch($_GET['op']) {
        case'':
                        
            echo'';
                        
            if(!isset($_GET["page"]) ) { $page = 1; 
            } else { $page = mysql_real_escape_string($_GET["page"]); 
            }
            $id = $_SESSION['log_id'];
                            
            switch($tip) {
            case'primite':
                $q = "SELECT * FROM mesagerie WHERE id_destinatar=$id AND trimis=0 ORDER BY id DESC";
                break;
                                
            case'trimise':
                $q = "SELECT * FROM mesagerie WHERE id_expeditor=$id AND trimis=1 ORDER BY id DESC";
                break;
            }
                            
            $limit = 20;
            $query = mysql_query($q) or trigger_error(mysql_error(), E_USER_ERROR);
            $results= mysql_num_rows($query);
            $nrpages = ceil($results/$limit);
            $limitvalue = $page * $limit - ($limit); 
            $sql = mysql_query("$q LIMIT $limitvalue, $limit") or trigger_error(mysql_error(), E_USER_ERROR);
            $dela = $limitvalue +1 ;
            $la   = $limitvalue + $limit;
            if ($la > $results ) { $la = $results ; 
            }
            if(mysql_num_rows($sql) == 0) {echo''.L_INBOX_GOL.'!';
            }
            else {
                echo'
								<form method="post" action="index.php?act=inbox&op=delete_all&tip='.$tip.'" name="inbox" id="form">
								<table id="nice_table" width="100%" cellspacing="0" cellpadding="0">
									<tr class="thead">
										<td width="2"></td>
										<td width="10"><input type="checkbox" name="all" onclick="checkAll()"/></td>
										<td width="250">'.$cine.'</td>
										<td>'.L_INBOX_SUBIECT.'</td>
										<td width="80">'.L_INBOX_DATA.'</td>
										<td width="50"></td>
										<td width="2"></td>
									</tr>';
                while($row = mysql_fetch_object($sql)) {
                                    
                    $expeditor = getOneValue("useri", "id", $row->$ce, "user");
                    $nume = getOneValue("useri", "id", $row->$ce, "nume");
                                    
                    if($row->citit == 1) { $bg_row=''; 
                    } else { $bg_row='#E8D5C0'; 
                    }
                    echo'<tr bgcolor="'.$bg_row.'">
											<td width="2"></td>
											<td class="tbb"><input type="checkbox" name="id[]" value="'.$row->id.'"/></td>
											<td class="tbl tbb"><b>'.$expeditor.' ('.$nume.')</b></td>
											<td class="tbl tbb"><a href="?act=inbox&op=read&id='.$row->id.'&tip='.$tip.'" style="color:black;">'.stripslashes($row->subiect).'</a></td>
											<td class="tbl tbb" style="font-size:11px;">'.date('d.m H:i', $row->data).'</td>
											<td class="tbl tbb">';
                    if($tip == "trimise") {
                    }
                    else {
                        echo'<a href="?act=inbox&op=compose&destinatar='.$expeditor.'&subiect=RE: '.stripslashes($row->subiect).'"><img src="images/reply.png"/></a>&nbsp;';
                    }
                    echo'<a href="?act=inbox&op=delete&id='.$row->id.'&tip='.$tip.'" onclick="return confirm(\''.L_SIGUR.'?\');"><img src="images/delete.png"/></a>';
                      echo'</td>
											<td width="2"></td>
										</tr>';
                }
                $inapoi  = max($page-1, 1);
                $inainte = min($page+1, $nrpages);
                                
                if($page == $inapoi) { $inapoi = '';
                } else { 
                    $inapoi = '<span style="font-size:11px;">&laquo;</span> <a href="index.php?page='.$inapoi.'&act=inbox&tip='.$tip.'">'.L_PREV.'</a>&nbsp;&nbsp;';
                }
                if($page == $inainte) { $inainte = '';
                } else { 
                    $inainte = '&nbsp;&nbsp;<a href="index.php?page='.$inainte.'&act=inbox&tip='.$tip.'">'.L_NEXT.'</a> <span style="font-size:11px;">&raquo;</span>';
                }
                echo'<tr class="paginare"><td width="2"></td><td colspan="2"><a href="javascript:;" onclick="document.inbox.submit();">'.L_INBOX_STERGE.'</a></td><td colspan="3" align="right">'.$inapoi.'<b>'.$page.'</b> '.L_DIN.' <b>'.$nrpages.'</b>'.$inainte.'</td><td width="2"></td></tr>';
                echo'</table></form>';
            }
            mysql_free_result($sql);
            break;
                        
        case'read':
            $id = mysql_real_escape_string($_GET['id']);
            $id_destinatar = $_SESSION['log_id'];
            $sql = mysql_query("SELECT * FROM mesagerie WHERE $ce_read='$id_destinatar' AND id='$id' LIMIT 0,1") or trigger_error(mysql_error(), E_USER_ERROR);
            if(mysql_num_rows($sql) == 0) {
                 echo'<span id="error">'.L_INBOX_INEXISTENT.'!</span>';
            }
            else {
                 $row = mysql_fetch_object($sql);
                 mysql_query("UPDATE mesagerie SET citit=1 WHERE id='$id'");
                 $expeditor = getOneValue("useri", "id", $row->$ce, "user");
                 $nume = getOneValue("useri", "id", $row->$ce, "nume");
                            
                            
                 echo'
							<table id="nice_table" width="100%" cellspacing="0" cellpadding="0">
								<tr class="thead">
									<td style="font-weight:normal;"><b>'.stripslashes($row->subiect).'</b> de la <span class="red">'.$expeditor.' ('.$nume.')</span></td>
								</tr>
								<tr>
									<td style="line-height:21px;">
									<b>'.L_INBOX_DATA.'</b>: '.date('d.M.Y H:i', $row->data).'<br/>
									<b>'.L_INBOX_MESAJ.'</b>: '.stripslashes(strip_tags(htmlentities($row->mesaj))).'</td>
								</tr>
							</table>
							<br/>';
                            
                if($tip == "trimise") {
                }
                else {
                                
                    if(isset($_POST['ok'])) {
                            
                        $subiect = diacritice(strip_tags(mysql_real_escape_string($_POST['subiect'])));
                        $mesaj = diacritice(strip_tags(mysql_real_escape_string($_POST['mesaj'])));
                        $destinatar = $_POST['destinatar'];
                        $data = mktime();
                        $id_expeditor = $_SESSION['log_id'];
                                    
                        if(($subiect == "") OR ($mesaj == "") OR $destinatar == "") {
                            echo'<span id="error">'.L_CAMPURI_INCOMPLETE.'!</span>';
                        }
                        else {
                                        
                            $id_destinatar = getOneValue("useri", "user", $destinatar, "id");
                            mysql_query("INSERT INTO mesagerie SET id_expeditor='$id_expeditor',id_destinatar='$id_destinatar',subiect='$subiect',mesaj='$mesaj',data='$data',citit=0,trimis=0") or trigger_error(mysql_error(), E_USER_ERROR);
                            email_mesaj(mysql_insert_id());
                                        
                                        
                            mysql_query("INSERT INTO mesagerie SET id_expeditor='$id_expeditor',id_destinatar='$id_destinatar',subiect='$subiect',mesaj='$mesaj',data='$data',citit=1,trimis=1") or trigger_error(mysql_error(), E_USER_ERROR);
                                        
                            echo'<span id="done">'.L_INBOX_SUCCES.'!</span>';
                        }
                                    
                                    
                    }
                    else {
                    }
                        
                                
                    echo'
							<table id="nice_table" width="100%" cellspacing="0" cellpadding="0">
								<tr class="thead">
									<td>'.L_INBOX_RASPUNDE.':</td>
								</tr>
								<tr>
									<td>
										<form method="post" action="">
											<table width="100%"id="no_padding" cellspacing="3">
												<tr>
													<td width="150">'.L_INBOX_DESTINATAR.':</td>
													<td><input type="text" class="inputus" name="destinatar" value="'.$expeditor.'"/></td>
												</tr>
												<tr>
													<td>'.L_INBOX_SUBIECT.':</td>
													<td><input type="text" class="inputus" name="subiect" value="RE: '.$row->subiect.'"/></td>
												</tr>
												<tr>
													<td valign="top">'.L_INBOX_MESAJ.':</td>
													<td><textarea class="inputus" style="height:110px;" name="mesaj"></textarea></td>
												</tr>
												<tr>
													<td colspan="2" align="right" style="padding-top:5px"><input type="submit" name="ok" value="'.L_INBOX_SEND.'" class="menu menu_active"/></td>
												</tr>
											</table>
										</form>
									</td>
								</tr>
							</table>';
                                
                }
                            
            }
            break;
                        
        case'delete':
            $id = mysql_real_escape_string($_GET['id']);
            $tip = mysql_real_escape_string($_GET['tip']);
            mysql_query("DELETE FROM mesagerie WHERE id='$id'");
            $_SESSION['mesaj'] = '<span id="done">'.L_INBOX_STERS.'!</span>';
            header("location:index.php?act=inbox&tip=$tip");
            break;
                        
        case'delete_all':
            $id = $_POST['id'];
            $tip = mysql_real_escape_string($_GET['tip']);
            foreach($id AS $id_mesaj) {
                 mysql_query("DELETE FROM mesagerie WHERE id='$id_mesaj'");
            }
                        
            $_SESSION['mesaj'] = '<span id="done">'.L_INBOX_STERS.'!</span>';
            header("location:index.php?act=inbox&tip=$tip");
            break;
                        
        case'compose':
            if(isset($_POST['ok'])) {
                            
                 $subiect = diacritice(strip_tags(mysql_real_escape_string($_POST['subiect'])));
                 $mesaj = diacritice(strip_tags(mysql_real_escape_string($_POST['mesaj'])));
                 $destinatari = $_POST['destinatari'];
                 $data = mktime();
                 $id_expeditor = $_SESSION['log_id'];
                            
                if(($subiect == "") OR ($mesaj == "") OR count($destinatari) == 0) {
                    echo'<span id="error">'.L_CAMPURI_INCOMPLETE.'!</span>';
                }
                else {
                                
                    foreach($destinatari AS $id_destinatar) {
                        mysql_query("INSERT INTO mesagerie SET id_expeditor='$id_expeditor',id_destinatar='$id_destinatar',subiect='$subiect',mesaj='$mesaj',data='$data',citit=0,trimis=0") or trigger_error(mysql_error(), E_USER_ERROR);
                        email_mesaj(mysql_insert_id());
                                    
                        // salveaza in trimise
                        mysql_query("INSERT INTO mesagerie SET id_expeditor='$id_expeditor',id_destinatar='$id_destinatar',subiect='$subiect',mesaj='$mesaj',data='$data',citit=1,trimis=1") or trigger_error(mysql_error(), E_USER_ERROR);
                        // end salveaza
                    }
                                
                    echo'<span id="done">'.L_INBOX_SUCCES.'!</span>';
                }
                            
                            
            }
            else {
            }
                        
            if($_GET['subiect'] != "") { $pre_subiect=strip_tags(htmlentities(addslashes($_GET['subiect']))); 
            } else {
            }
            if($_GET['mesaj'] != "") { $pre_mesaj=strip_tags(htmlentities(addslashes($_GET['mesaj']))); 
            } else {
            }
                        
            echo'<form method="post" action="">
						<table cellspacing="3" class="nice" width="100%">
							<tr>
								<td>'.L_INBOX_SUBIECT.':</td>
								<td><input type="text" name="subiect" size="50" class="inputus" value="'.$pre_subiect.'"/></td>
								<td style="padding-left:30px;">'.L_INBOX_DESTINATAR.':</td>
							</tr>
							<tr>
								<td valign="top">'.L_INBOX_MESAJ.':</td>
								<td><textarea name="mesaj" class="inputus" style="height:200px;">'.$pre_mesaj.'</textarea></td>
								<td rowspan="3" valign="top" style="padding-left:30px;">
									<select name="destinatari[]" class="inputus" multiple style="height:190px;">';
            if($_GET['destinatar'] != "") {
                $id_dest = getOneValue("useri", "user", $_GET['destinatar'], "id");
                $nume_dest = getOneValue("useri", "user", $_GET['destinatar'], "nume");
                echo'<option value="'.$id_dest.'" selected>'.stripslashes($_GET['destinatar']).' ('.$nume_dest.')</option>';
            }    
            echo''.generate_destinatari($_SESSION['log_id']).'
								</select>
								<br/>
								<span style="font-size:11px;">* '.L_INBOX_HINT.'</span>
								</td>
							</tr>
							<tr>
								<td></td>
								<td><input type="submit" name="ok" value="'.L_INBOX_SEND.'" class="menu menu_active"/></td>
							</tr>
						</table>
						</form>';
            break;
        }
    ?>

            </td>
        </tr>
    </table>

</div>

<?php
$content = ob_get_clean();
ob_end_clean();
?>