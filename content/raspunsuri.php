<?php
/**
 * File raspunsuri.php
 * 
 * Page displaying a user's responses to questions.
 * Only accessible to mentors.
 * 
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */

ob_start();
acces(); 
if(nivel($_SESSION['log_id']) != 5) { header("location:/"); 
}
$id_intrebare = mysql_real_escape_string($_GET['id_intrebare']);
$intrebare = getOneValue("intrebari", "id", $id_intrebare, "titlu");
?>

<div id="content">
    <h1><?php echo L_RAS_PENTRU?> <i style="font-size:22px;"><?php echo $intrebare; ?></i> &raquo; <a href="?act=raspunsuri&op=add&id_intrebare=<?php echo $id_intrebare;?>"><?php echo L_RAS_NOU?></a></h1>
    <br/>
    
    <?php
    echo $_SESSION['mesaj'];
    unset($_SESSION['mesaj']);
    switch($_GET['op']) {
    case'':
        
        if(!isset($_GET["page"]) ) { $page = 1; 
        } else { $page = mysql_real_escape_string($_GET["page"]); 
        }    
        $q = "SELECT * FROM raspunsuri WHERE id_intrebare='$id_intrebare' ORDER BY id ASC";
        $limit = 20;
        $query = mysql_query($q) or trigger_error(mysql_error(), E_USER_ERROR);
        $results= mysql_num_rows($query);
        $nrpages = ceil($results/$limit);
        $limitvalue = $page * $limit - ($limit); 
        $sql = mysql_query("$q LIMIT $limitvalue, $limit") or trigger_error(mysql_error(), E_USER_ERROR);
        $dela = $limitvalue +1 ;
        $la   = $limitvalue + $limit;
        if ($la > $results ) { $la = $results ; 
        }
        if(mysql_num_rows($sql) == 0) {
        }
        else {
             echo'
			<table id="nice_table" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td width="10"></td>
					<td class="tbb"><b>'.L_RAS_RASPUNS.'</b></td>
					<td class="tbl tbb" width="80"><b>Op.</b></td>
					<td width="10"></td>
				</tr>';
            while($row = mysql_fetch_object($sql)) {
                if($row->corect == 1) { $corect=' - <span style="color:green; font-size:11px;">'.L_RAS_CORECT.'</span>'; 
                } else {$corect='';
                }
                echo'<tr>
						<td width="10"></td>
						<td>'.stripslashes($row->label).' '.$corect.'</td>
						<td align="center">
							<a href="?act=raspunsuri&op=edit&id='.$row->id.'&id_intrebare='.$id_intrebare.'"><img src="images/edit.gif"/></a>
							<a href="?act=raspunsuri&op=delete&id='.$row->id.'&id_intrebare='.$id_intrebare.'" onclick="return confirm(\''.L_SIGUR.'?\');"><img src="images/delete.png"/></a>
						</td>
						<td width="10"></td>
					</tr>';
            }
             $inapoi  = max($page-1, 1);
             $inainte = min($page+1, $nrpages);
            
            if($page == $inapoi) { $inapoi = '';
            } else { 
                 $inapoi = '<span style="font-size:11px;">&laquo;</span> <a href="index.php?page='.$inapoi.'&act=raspunsuri&id_intrebare='.$id_intrebare.'">'.L_PREV.'</a>&nbsp;&nbsp;';
            }
            if($page == $inainte) { $inainte = '';
            } else { 
                 $inainte = '&nbsp;&nbsp;<a href="index.php?page='.$inainte.'&act=raspunsuri&id_intrebare='.$id_intrebare.'">'.L_NEXT.'</a> <span style="font-size:11px;">&raquo;</span>';
            }
             echo'<tr><td width="10"></td><td colspan="2">'.$inapoi.'<b>'.$page.'</b> '.L_DIN.' <b>'.$nrpages.'</b>'.$inainte.'</td><td width="10"></td></tr>';
             echo'</table>';
        }
        mysql_free_result($sql);
        break;
        
    case'add':
        echo'&raquo; <a href="?act=raspunsuri&id_intrebare='.$id_intrebare.'">'.L_INAPOI_RASPUNSURI.'</a> &nbsp;&nbsp;&nbsp; &raquo; <a href="?act=intrebari&id_lectie='.getOneValue("intrebari", "id", $id_intrebare, "id_lectie").'">'.L_INAPOI_INTREBARI.'</a><br/><br/>';
        if(isset($_POST['ok'])) {
             $id_intrebare = mysql_real_escape_string($_POST['id_intrebare']);
             $label = diacritice(mysql_real_escape_string($_POST['label']));
             $corect = diacritice(mysql_real_escape_string($_POST['corect']));
            
            if(($label == "")) {
                echo'<span id="error">'.L_CAMPURI_INCOMPLETE.'!</span>';
            }
            else {
                mysql_query("INSERT INTO raspunsuri SET id_intrebare='$id_intrebare',label='$label',corect='$corect'");
                echo'<span id="done">'.L_RAS_ADAUGAT.'!</span>';
            }
            
            
        }
        else {
        }
        
        echo'<form method="post" action="">
		<table cellspacing="2" cellpadding="4" style="margin-top:5px;">
			<tr>
				<td width="100">'.L_RAS_RASPUNS.':</td>
				<td><input type="text" class="inputus" size="80" name="label" value=""/></td>
			</tr>
			<tr>
				<td>'.L_RAS_CORECT.'?</td>
				<td><input type="checkbox" name="corect" value="1"/></td>
			</tr>
			<tr>
				<td><input type="hidden" name="id_intrebare" value="'.$id_intrebare.'"/></td>
				<td><input type="submit" name="ok" value="'.L_ADAUGA.'" class="menu"/></td>
			</tr>
		</table>
		</form>';
        break;
        
    case'edit':
        echo'&raquo; <a href="?act=raspunsuri&id_intrebare='.$id_intrebare.'">'.L_INAPOI_RASPUNSURI.'</a><br/><br/>';
        if(isset($_POST['ok'])) {
            
             $label = diacritice(mysql_real_escape_string($_POST['label']));
             $corect = diacritice(mysql_real_escape_string($_POST['corect']));
             $id = mysql_real_escape_string($_POST['id']);
            
            if($label == "") {
                echo'<span id="error">'.L_CAMPURI_INCOMPLETE.'!</span>';
            }
            else {
                mysql_query("UPDATE raspunsuri SET label='$label',corect='$corect' WHERE id='$id'");
                echo'<span id="done">'.L_CU_SUCCES.'!</span>';
            }
            
            
        }
        else {
        }
        
        $id = mysql_real_escape_string($_GET['id']);
        $sql = mysql_query("SELECT * FROM raspunsuri WHERE id='$id'");
        $row = mysql_fetch_object($sql);
        if($row->corect == 1) { $check=' checked'; 
        } else {$check='';
        }
        echo'<form method="post" action="">
		<table cellspacing="2" cellpadding="4" style="margin-top:5px;">
			<tr>
				<td width="100">'.L_RAS_RASPUNS.':</td>
				<td><input type="text" class="inputus" size="80" name="label" value="'.stripslashes($row->label).'"/></td>
			</tr>
			<tr>
				<td>'.L_RAS_CORECT.'?</td>
				<td><input type="checkbox" name="corect" value="1"'.$check.'/></td>
			</tr>
			<tr>
				<td><input type="hidden" name="id" value="'.$id.'"/></td>
				<td><input type="submit" name="ok" value="'.L_MODIFICA.'" class="menu"/></td>
			</tr>
		</table>
		</form>';
        break;
        
    case'delete':
        $id = mysql_real_escape_string($_GET['id']);
        sterge_raspuns($id);
        $_SESSION['mesaj'] = '<span id="done">'.L_RAS_STERS.'!</span>';
        header("location:index.php?act=raspunsuri&id_intrebare=$id_intrebare");
        break;
        
    }
    ?>
    
</div>

<?php
$content = ob_get_clean();
ob_end_clean();
?>