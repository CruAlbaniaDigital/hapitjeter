<?php
/**
 * Teste.php File
 * 
 * Form for adding and modifying lessons and test questions.
 * Lesson data is stored in the 'teste' table in the database.
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */
ob_start();
acces(); 
if(nivel($_SESSION['log_id']) != 5) { header("location:/"); 
}

$sql = mysql_query("SELECT COUNT(id) AS tot FROM useri") or trigger_error(mysql_error(), E_USER_ERROR);
if(mysql_num_rows($sql) == 0) { $nr=0; 
}
else {
    $row = mysql_fetch_object($sql);
    $nr = $row->tot;
}
mysql_free_result($sql);
?>

<div id="content">
    <h1><?php echo L_TST_TITLU?> &raquo; <a href="?act=teste&op=add"><?php echo L_TST_NOU?></a></h1>
    <br/>
    
    <?php
    echo $_SESSION['mesaj'];
    unset($_SESSION['mesaj']);
    switch($_GET['op']) {
    case'':
        
        if(!isset($_GET["page"]) ) { $page = 1; 
        } else { $page = mysql_real_escape_string($_GET["page"]); 
        }
            
        $q = "SELECT * FROM teste ORDER BY id DESC";
        $limit = 20;
        $query = mysql_query($q) or trigger_error(mysql_error(), E_USER_ERROR);
        $results= mysql_num_rows($query);
        $nrpages = ceil($results/$limit);
        $limitvalue = $page * $limit - ($limit); 
        $sql = mysql_query("$q LIMIT $limitvalue, $limit") or trigger_error(mysql_error(), E_USER_ERROR);
        $dela = $limitvalue +1 ;
        $la   = $limitvalue + $limit;
        if ($la > $results ) { $la = $results ; 
        }
        if(mysql_num_rows($sql) == 0) {
        }
        else {
            echo'
				<table id="nice_table" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td width="10"></td>
						<td class="tbb"><b>'.L_TST_TITLU2.'</b></td>
						<td class="tbl tbb" width="30"><b>'.L_STAT_NIVEL.'</b></td>
						<td class="tbl tbb" width="80"><b>Op.</b></td>
						<td width="10"></td>
					</tr>';
            while($row = mysql_fetch_object($sql)) {
                echo'<tr>
							<td width="10"></td>
							<td>'.stripslashes($row->titlu).' &raquo; <a href="?act=lectii&id_test='.$row->id.'">'.L_TST_LECTII.'</a></td>
							<td align="center"><b>'.$row->nivel.'</b></td>
							<td align="center">
								<a href="?act=teste&op=edit&id='.$row->id.'"><img src="images/edit.gif"/></a>
								<a href="?act=teste&op=delete&id='.$row->id.'" onclick="return confirm(\''.L_SIGUR.'?\');"><img src="images/delete.png"/></a>
							</td>
							<td width="10"></td>
						</tr>';
            }
            $inapoi  = max($page-1, 1);
            $inainte = min($page+1, $nrpages);
                
            if($page == $inapoi) { $inapoi = '';
            } else { 
                $inapoi = '<span style="font-size:11px;">&laquo;</span> <a href="index.php?page='.$inapoi.'&act=teste">'.L_PREV.'</a>&nbsp;&nbsp;';
            }
            if($page == $inainte) { $inainte = '';
            } else { 
                $inainte = '&nbsp;&nbsp;<a href="index.php?page='.$inainte.'&act=teste">'.L_NEXT.'</a> <span style="font-size:11px;">&raquo;</span>';
            }
            echo'<tr><td width="10"></td><td colspan="3">'.$inapoi.'<b>'.$page.'</b> '.L_DIN.' <b>'.$nrpages.'</b>'.$inainte.'</td><td width="10"></td></tr>';
            echo'</table>';
        }
        mysql_free_result($sql);
        break;
        
    case'add':
        echo'&raquo; <a href="?act=teste">'.L_INAPOI_TESTE.'</a><br/><br/>';
        if(isset($_POST['ok'])) {
             $titlu = diacritice(mysql_real_escape_string($_POST['titlu']));
             $descriere = diacritice(mysql_real_escape_string($_POST['descriere']));
             $nivel = mysql_real_escape_string($_POST['nivel']);
            
            if(($titlu == "") OR ($nivel == "")) {
                echo'<span id="error">'.L_CAMPURI_INCOMPLETE.'!</span>';
            }
            else {
                mysql_query("INSERT INTO teste SET titlu='$titlu',descriere='$descriere',nivel='$nivel'");
                echo'<span id="done">'.L_TST_OK.'!</span>';
            }
            
            
        }
        else {
        }
        
        echo'<form method="post" action="">
		<table cellspacing="2" cellpadding="4" style="margin-top:5px;" width="100%">
			<tr>
				<td width="100">'.L_TST_TITLU2.':</td>
				<td><input type="text" class="inputus" size="40" name="titlu" value=""/></td>
			</tr>
			<tr>
				<td valign="top">'.L_TST_DESCRIERE.'</td>
				<td><textarea class="inputus" name="descriere" style="height:200px;"></textarea></td>
			</tr>
			<tr>
				<td>'.L_STAT_NIVEL.':</td>
				<td><input type="text" class="inputus" style="width:20px;" name="nivel" value=""/> (max <b>4</b>)</td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" name="ok" value="'.L_ADAUGA.'" class="menu"/></td>
			</tr>
		</table>
		</form>';
        break;
        
    case'edit':
        echo'&raquo; <a href="?act=teste">'.L_INAPOI_TESTE.'</a><br/><br/>';
        if(isset($_POST['ok'])) {
             $titlu = diacritice(mysql_real_escape_string($_POST['titlu']));
             $descriere = diacritice(mysql_real_escape_string($_POST['descriere']));
             $id = mysql_real_escape_string($_POST['id']);
            
            if($titlu == "") {
                echo'<span id="error">'.L_CAMPURI_INCOMPLETE.'!</span>';
            }
            else {
                mysql_query("UPDATE teste SET titlu='$titlu',descriere='$descriere' WHERE id='$id'");
                echo'<span id="done">'.L_CU_SUCCES.'!</span>';
            }
            
            
        }
        else {
        }
        
        $id = mysql_real_escape_string($_GET['id']);
        $sql = mysql_query("SELECT * FROM teste WHERE id='$id'");
        $row = mysql_fetch_object($sql);
        echo'<form method="post" action="">
		<table cellspacing="2" cellpadding="4" style="margin-top:5px;" width="100%">
			<tr>
				<td width="100">'.L_TST_TITLU2.':</td>
				<td><input type="text" class="inputus" size="40" name="titlu" value="'.stripslashes($row->titlu).'"/></td>
			</tr>
			<tr>
				<td valign="top">'.L_TST_DESCRIERE.'</td>
				<td><textarea class="inputus" name="descriere" style="height:200px;">'.stripslashes($row->descriere).'</textarea></td>
			</tr>
			<tr>
				<td><input type="hidden" name="id" value="'.$id.'"/></td>
				<td><input type="submit" name="ok" value="'.L_MODIFICA.'" class="menu"/></td>
			</tr>
		</table>
		</form>';
        break;
        
    case'delete':
        $id = mysql_real_escape_string($_GET['id']);
        sterge_test($id);
        $_SESSION['mesaj'] = '<span id="done">'.L_TST_STERS.'!</span>';
        header("location:index.php?act=teste");
        break;
    
    }
    ?>
    
</div>

<?php
$content = ob_get_clean();
ob_end_clean();
?>