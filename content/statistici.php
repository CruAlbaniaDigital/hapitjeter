<?php
/**
 * Statistici.php File
 * 
 * Statistics view file for admins to view what other users have done in the system
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */

ob_start();
acces(); 
if(nivel($_SESSION['log_id']) != 5) { header("location:/"); 
}

$sql = mysql_query("SELECT COUNT(id) AS tot FROM useri") or trigger_error(mysql_error(), E_USER_ERROR);
if(mysql_num_rows($sql) == 0) { $nr=0; 
}
else {
    $row = mysql_fetch_object($sql);
    $nr = $row->tot;
}
mysql_free_result($sql);
?>

<div id="content">
    <h1><?php echo L_STAT_TITLU?></h1>
    <p><i><?php echo $nr; ?> <?php echo L_STAT_UTILIZATORI?></i></p>
    <a href="excel.php" title="Salveaza" style="float:right; text-decoration:none; color:green; margin-top:-30px;"><img src="images/excel.png" alt="<?php echo L_STAT_SALVEAZA?>" style="vertical-align:middle; margin-top:-4px;"/> <?php echo L_STAT_SALVEAZA?></a>
    <br/>
    <?php
    switch($_GET['op']) {
    case'':
        $sql = mysql_query("SELECT MIN(varsta) AS minim, MAX(varsta) AS maxim, AVG(varsta) AS medie FROM useri") or trigger_error(mysql_error(), E_USER_ERROR);
        if(mysql_num_rows($sql) == 0) {
        }
        else {
             $row = mysql_fetch_object($sql);
             $varsta_minima = round($row->minim);
             $varsta_medie = round($row->medie);
             $varsta_maxima = round($row->maxim);
        }
        mysql_free_result($sql);
        echo'
		<table id="nice_table" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td width="10"></td>
				<td class="tbb" width="100">'.L_STAT_TITLU.'</td>
				<td class="tbl tbb">'.L_STAT_VALORI.'</td>
				<td width="10"></td>
			</tr>
			<tr>
				<td width="10"></td>
				<td class="tbb" width="100" valign="top"><b>'.L_STAT_VARSTA.'</b></td>
				<td class="tbl tbb">
					'.L_STAT_VARSTA_MIN.': '.$varsta_minima.'<br/>
					'.L_STAT_VARSTA_MED.': '.$varsta_medie.'<br/>
					'.L_STAT_VARSTA_MAX.': '.$varsta_maxima.'<br/>
				</td>
				<td width="10"></td>
			</tr>
			<tr>
				<td width="10"></td>
				<td class="tbb" width="100" valign="top"><b>'.L_STAT_LOCATII.'</b></td>
				<td class="tbl tbb">';
        $sql = mysql_query("SELECT DISTINCT(oras) FROM useri");
        while($row = mysql_fetch_object($sql)) {
            $sql1 = mysql_query("SELECT COUNT(id) AS tot FROM useri WHERE LOWER(oras)='$row->oras'") or trigger_error(mysql_error(), E_USER_ERROR);
            if(mysql_num_rows($sql1) == 0) {$nr=0;
            }
            else {
                $row1 = mysql_fetch_object($sql1);
                $nr = $row1->tot;
            }
            mysql_free_result($sql1);
            if($row->oras == "") { $oras="-"; 
            } else { $oras = $row->oras; 
            }
            echo''.$oras.' (<b>'.$nr.'</b> '.L_STAT_UTILIZATORI.')<br/>';
        }
        mysql_free_result($sql);
        echo'</td>
				<td width="10"></td>
			</tr>
			<tr>
				<td width="10"></td>
				<td class="tbb" width="100" valign="top"><b>'.L_STAT_NIVELE.'</b></td>
				<td class="tbl tbb">';
        for($i=1;$i<=5;$i++) {
            $sql = mysql_query("SELECT COUNT(id) AS tot FROM useri WHERE nivel='$i'") or trigger_error(mysql_error(), E_USER_ERROR);
            if(mysql_num_rows($sql) == 0) {$nr=0;
            }
            else {
                $row= mysql_fetch_object($sql);
                $nr = $row->tot;
            }
            echo''.L_STAT_NIVEL.' '.$i.': <b>'.$nr.'</b> '.L_STAT_UTILIZATORI.'<br/>';
        }
        echo'</td>
				<td width="10"></td>
			</tr>
		</table>';
    }
    ?>
</div>

<?php
$content = ob_get_clean();
ob_end_clean();
?>