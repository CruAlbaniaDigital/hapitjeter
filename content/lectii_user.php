<?php
/**
 * File lectii_user.php
 * 
 * Page showing the user's current lessons and completion status
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */

ob_start();
acces(); 
unset($_SESSION['intrebari']);
$nivel = nivel($_SESSION['log_id']);
?>

<div id="content">
    <h1><?php echo L_LECTII?></h1>
    <br/>
    <?php
    if($nivel == 1) {
    }
    else {
        echo'&bull; <a href="?act=lectii_user">'.L_LU_REZOLVAT.'</a>&nbsp;&nbsp;&nbsp;&nbsp;';
        for($i=1;$i<$nivel;$i++) {
            echo'&bull; <a href="?act=lectii_user&nivel='.$i.'">'.L_LU_ARHIVA.' '.$i.'</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        }
        echo'<br/><br/>';
    }
    
    if(isset($_GET['nivel']) AND ($_GET['nivel'] <= $nivel)) { $nivel = $_GET['nivel']; 
    } else { $nivel=$nivel; 
    }
    $sql = mysql_query("SELECT id,titlu,descriere FROM teste WHERE nivel='$nivel'") or trigger_error(mysql_error(), E_USER_ERROR);
    if(mysql_num_rows($sql) == 0) {
        echo'<p>'.L_LU_ZERO.'!</p>';
    }
    else {
        $check=3;
        while($row = mysql_fetch_object($sql)) {
            echo'<fieldset><legend>'.stripslashes($row->titlu).'</legend>';
            echo'<p style="margin-top:5px; padding-left:3px;">'.stripslashes($row->descriere).'</p>';
            echo'
			<table id="nice_table" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td width="10"></td>
					<td width="30"></td>
					<td class="tbb"><b>'.L_LU_DENUMIRE.'</b></td>
					<td class="tbb" width="150"></td>
					<td class="tbb" width="150"></td>
					<td width="10"></td>
				</tr>';
            
            $sql1 = mysql_query("SELECT id,titlu FROM lectii WHERE id_test='$row->id' ORDER BY ord ASC") or trigger_error(mysql_error(), E_USER_ERROR);
            if(mysql_num_rows($sql1) == 0) {
            }
            else {
                $i=1;
                $check=3;
                while($row1 = mysql_fetch_object($sql1)) {
                    $stare = stare_lectie($row1->id, $_SESSION['log_id']);
                    echo'<tr>
						<td width="10"></td>
						<td class="tbb"><div class="nr">'.$i++.'</div></td>
						<td class="tbb">'.stripslashes($row1->titlu).'</td>
						<td class="tbb">'.$stare['text'].'</td>
						<td class="tbb" align="center">';
                    if($check == 3) {
                        echo'<span class="menu menu_active"><a href="?act=exam&id='.$row1->id.'">'.$stare['buton'].'</a></span>';
                    }
                    else {
                    }
                    echo'</td>
						<td width="10"></td>
					</tr>';
                    $check = $stare['val'];
                }
            }
            mysql_free_result($sql1);
            
            
            echo'</table>';
            
            echo'</fieldset><br/>';
        }
    }
    mysql_free_result($sql);
    ?>
    
</div>

<?php
$content = ob_get_clean();
ob_end_clean();
?>