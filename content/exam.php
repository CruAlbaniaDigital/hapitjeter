<?php
/**
 * Exam.php File
 * 
 * Displays a lesson by ID, providing a form for the user to post back their answers to the lesson.
 * On post advances to the next question, until the user finishes the lesson.
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */

ob_start();
acces(); 

///////////////////////
// INSERARE STATUS LECTIE (daca nu exista) //
///////////////////////
$id_lectie = mysql_real_escape_string($_GET['id']);
$id_user = $_SESSION['log_id'];
$sql = mysql_query("SELECT id,stare FROM status_lectie WHERE id_user='$id_user' AND id_lectie='$id_lectie'") or trigger_error(mysql_error(), E_USER_ERROR);
if(mysql_num_rows($sql) == 0) {
    $stare=1;
    mysql_query("INSERT INTO status_lectie SET id_lectie='$id_lectie',id_user='$id_user',stare='1'") or trigger_error(mysql_error(), E_USER_ERROR);
} else {
    $row = mysql_fetch_object($sql);
    $acum = mktime();
    $stare = $row->stare;
    mysql_query("UPDATE status_lectie SET last_update='$acum' WHERE id='$row->id'") or trigger_error(mysql_error(), E_USER_ERROR);
}
mysql_free_result($sql);

///////////////////////
// GENERARE SESIUNE CU INTREBARI //
///////////////////////

if(!isset($_SESSION['intrebari'])) {
    $sql = mysql_query("SELECT id FROM intrebari WHERE id_lectie='$id_lectie' ORDER BY ord ASC") or trigger_error(mysql_error(), E_USER_ERROR);
    if(mysql_num_rows($sql) == 0) {
    }
    else {
        while($row = mysql_fetch_object($sql)) {
            $_SESSION['intrebari'][] = $row->id;
        }
    }
    mysql_free_result($sql);
    $_SESSION['intrebari_total'] = count($_SESSION['intrebari']);
    $_SESSION['intrebari_activ'] = 1;
}
else {
}

///////////////////////
// PRELUCRARE INTREBARE //
///////////////////////

if(isset($_POST['ok'])) {
    $id_intrebare = mysql_real_escape_string($_POST['id_intrebare']);
    $id_user = $_SESSION['log_id'];
    $tip = getOneValue("intrebari", "id", $id_intrebare, "tip");
    // sterge intrebarea la care s-a raspuns din sesiune
    $k = array_search($id_intrebare, $_SESSION['intrebari']);
    unset($_SESSION['intrebari'][$k]);
    $nou = array_values($_SESSION['intrebari']);
    unset($_SESSION['intrebari']);
    $_SESSION['intrebari'] = $nou;
    $_SESSION['intrebari_activ']++;
    // preluare si insert raspunsuri
    $stare = stare_lectie($id_lectie, $id_user);
    if($stare['val'] == 3) {
    }
    else {
        $raspunsuri = preluare_raspunsuri($tip, $_POST);
        mysql_query("DELETE FROM rezolvari WHERE id_user='$id_user' AND id_intrebare='$id_intrebare'") or trigger_error(mysql_error(), E_USER_ERROR);
        $sql = mysql_query("SELECT id FROM status_intrebare WHERE id_user='$id_user' AND id_intrebare='$id_intrebare'") or trigger_error(mysql_error(), E_USER_ERROR);
        if(mysql_num_rows($sql) == 0) {
            mysql_query("INSERT INTO status_intrebare SET id_user='$id_user',id_intrebare='$id_intrebare',corect=''");
        }
        else {
        }
        
        foreach($raspunsuri['raspuns'] AS $key=>$val) {
            $id_raspuns = $val;
            $valoare = addslashes($raspunsuri['valoare'][$key]);
            if($tip == "grila_raspuns") {
                $corect = getOneValue("raspunsuri", "id", $id_raspuns, "corect");
                if($corect == 1) {
                    mysql_query("UPDATE status_intrebare SET corect=1 WHERE id_user='$id_user' AND id_intrebare='$id_intrebare'");
                }
                else {
                }
            }
            mysql_query("INSERT INTO rezolvari SET id_user='$id_user',id_intrebare='$id_intrebare',id_raspuns='$id_raspuns',valoare='$valoare'") or trigger_error(mysql_error(), E_USER_ERROR);
        }
        
        $corect = corectare_intrebare($id_intrebare, $id_user);
        if($corect == 0) { 
            mysql_query("UPDATE status_intrebare SET corect='' WHERE id_user='$id_user' AND id_intrebare='$id_intrebare'");
        }
        else {
        }
        
        
    }

    if(count($_SESSION['intrebari']) == 0) {
        $stare = stare_lectie($id_lectie, $id_user);

        if($stare['val'] == 3) {
        }
        else {
            if($stare['val'] <= 2) {
                // trimite mail mentorului
                $id_parinte = getOneValue("useri", "id", $_SESSION['log_id'], "parent_id");
                email_mentor($id_parinte);
            }
            else {
            }
            mysql_query("UPDATE status_lectie SET stare='2' WHERE id_lectie=$id_lectie AND id_user=$id_user") or trigger_error(mysql_error(), E_USER_ERROR);    
        }
    }
    else {
    }
}
else {
}

///////////////////////
// AFISARE INTREBARE //
///////////////////////
if(isset($_POST['ok'])) { 
    $display_lectie='none'; 
    $display_hide = "none"; 
    $display_show = "block"; 
} 
else { 
    $display_lectie='block'; 
    $display_hide = "block"; 
    $display_show = "none"; 
}
$descriere = getOneValue("lectii", "id", $id_lectie, "descriere");

?>

<div id="content">

    <table width="100%">
        <tr>
            <td><h1><?php echo getOneValue("lectii", "id", $id_lectie, "titlu")?></h1></td>
            <td align="right" width="120"><span class="menu_mic"><img src="images/down_icon.png"/>&nbsp;&nbsp;<a href="?act=exam&id=<?php echo $id_lectie?>#chestionar"><?php echo L_JUMP_SURVEY?></a></span></td>
            <td align="right" width="90">
                <span class="menu_mic menu_mic_red" id="btn_hide" style="display:<?php echo $display_hide?>"><img src="images/error_icon.png"/>&nbsp;&nbsp;<a href="javascript:;" onclick="expand('expand');"><?php echo L_EXAM_CLICK_RES?></a></span>
                <span class="menu_mic menu_mic_red" id="btn_show" style="display:<?php echo $display_show?>"><a href="javascript:;" onclick="expand('collapse');"><?php echo L_EXAM_CLICK_EXT?></a></span>
                
            </td>
        </tr>
    </table>

    <br/>
    <div id="lectie" style="display:<?php echo $display_lectie?>; margin-top:10px;"><?php echo biblie($descriere)?></div>
    
    <?php
    $id_intrebare = $_SESSION['intrebari'][0];
    if($id_intrebare == "") {    
        //s-a terminat testul
        $parent_id = getOneValue("useri", "id", $id_user, "parent_id");
        $mentor = ucfirst(getOneValue("useri", "id", $parent_id, "nume"));
        $succes = str_replace("[mentor]", $mentor, L_EXAM_SUCCES);
        echo'<br/><br/><div align="center">
			<span style="color:green; font-size:15px;">'.$succes.'!</span><br/><br/>
			&laquo; <a href="?act=lectii_user">'.L_INAPOI_LECTII.'</a>
		</div><br/><br/>';
    }
    else {
        
        $parcurs = str_replace('[activ]', $_SESSION['intrebari_activ'], L_EXAM_PARCURS);
        $parcurs = str_replace('[total]', $_SESSION['intrebari_total'], $parcurs);
        
        $titlu = getOneValue("intrebari", "id", $id_intrebare, "titlu");
        $descriere = getOneValue("intrebari", "id", $id_intrebare, "descriere");
        $tip = getOneValue("intrebari", "id", $id_intrebare, "tip");
        
        if(count($_SESSION['intrebari']) == 1) { $val=L_EXAM_FINALIZARE; 
        } else {$val=L_EXAM_NEXT; 
        }
        
        $parent_id = getOneValue("useri", "id", $id_user, "parent_id");
        $mentor = getOneValue("useri", "id", $parent_id, "user");
        $ym = getOneValue("useri", "id", $parent_id, "ym");
        $intrebare = getOneValue("intrebari", "id", $id_intrebare, "titlu");
        $link = '?act=inbox&op=compose&destinatar='.$mentor.'&subiect='.$intrebare.'';
        $help = str_replace('[link]', $link, L_RAS_HELP);
        
        if($ym == "") { $ym=''; 
        } else { $ym='<a href="ymsgr:sendIM?'.$ym.'"><img src="http://opi.yahoo.com/online?u='.$ym.'&m=g&t=1" border="0" alt="Yahoo! Messenger" style="vertical-align:middle; margin-top:-2px;"></a>'; 
        }
        
        echo'
		<a name="chestionar"></a>
		<form method="post" action="" onsubmit="return validare();">
		<table id="nice_table" width="100%" cellspacing="0" cellpadding="0">
			<tr class="thead" style="font-weight:normal;">
				<td colspan="2">
					<table width="100%">
						<tr>
							<td width="30"><div class="nr">'.$_SESSION['intrebari_activ'].'</div></td>
							<td style="padding-left:10px;">
								<h2 style="padding:0; margin:0;">'.stripslashes($titlu).'</h2>
							</td>
						</tr>';
        
        if($descriere == "") {
        }
        else {
            echo'<tr>
					<td colspan="2">
						<table class="quote" width="100%">
							<tr>
								<td valign="top" width="20"><img src="images/quote.png"/></td>
								<td class="tbl">'.biblie($descriere).'</td>
							</tr>
						</table>
					</td>
				</tr>';
        }
                    
        echo'</table>
					
					'.raspunsuri($tip, $id_intrebare, $id_lectie).'
					
				</td>
			</tr>
			<tr class="thead" style="font-weight:normal;">
				<td valign="top">
					<p style="margin:0; padding:0;"><img src="images/help_icon.png"/> <i>'.$help.' '.$ym.'</i></p>
				</td>
				<td align="right"><input type="submit" class="menu menu_active" value="'.$val.'" name="ok"/></td>
			</tr>
		</table>
		<input type="hidden" name="id_intrebare" value="'.$id_intrebare.'"/>
		</form>
		<div align="right"><p>'.$parcurs.'</p></div>';
    
    }
    ?>

    
</div>

<?php
$content = ob_get_clean();
ob_end_clean();
?>