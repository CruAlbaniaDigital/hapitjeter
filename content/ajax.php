<?php
/**
 * File ajax.php
 * 
 * Responds to ajax requests from the web client.
 * Ajax operations:
 *   'info_user': gets information on a specific user by database ID in query string
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */

switch($_GET['op']) {
case'info_user':
    $id = intval($_GET['id']);
    $sql = mysql_query("SELECT poza,descriere FROM useri WHERE id='$id' AND afis_mentor='1' LIMIT 0,1");
    if(mysql_num_rows($sql) == 0) {
        echo'error';
    }
    else {
        $row = mysql_fetch_object($sql);
        if($row->descriere == "") { 
            echo'error';
        }
        else {
            if($row->poza == "") { $poza=''; 
            } else { $poza='<img src="upload/fckfiles/'.$row->poza.'" align="left" style="margin-right:5px; margin-bottom:5px;" width="150"/>'; 
            }
            echo''.$poza.' '.stripslashes($row->descriere).'<br class="clear"/>';
        }
    }
    mysql_free_result($sql);
    break;
}

die();
?>