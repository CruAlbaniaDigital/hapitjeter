<?php
/**
 * Rezolvari.php File
 * 
 * Unfortunately I'm not entirely sure what this does yet...
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */

ob_start();
acces(); 
?>

<div id="content">
    <h1><?php echo L_REZ_TITLU?></h1>
    <br/>
    
    <?php
    switch($_GET['op']) {
    case'':
        $parent_id = $_SESSION['log_id'];
        if(!isset($_GET["page"]) ) { $page = 1; 
        } else { $page = mysql_real_escape_string($_GET["page"]); 
        }
        
        $q = "SELECT a.nume,b.id_lectie,b.last_update,b.id,b.stare FROM useri a,status_lectie b WHERE a.parent_id='$parent_id' AND a.id=b.id_user AND b.stare>1 ORDER BY last_update DESC";
        $limit = 20;
        $query = mysql_query($q) or trigger_error(mysql_error(), E_USER_ERROR);
        $results= mysql_num_rows($query);
        $nrpages = ceil($results/$limit);
        $limitvalue = $page * $limit - ($limit); 
        $sql = mysql_query("$q LIMIT $limitvalue, $limit") or trigger_error(mysql_error(), E_USER_ERROR);
        $dela = $limitvalue +1 ;
        $la   = $limitvalue + $limit;
        if ($la > $results ) { $la = $results ; 
        }
        if(mysql_num_rows($sql) == 0) {
             echo'<p>'.L_REZ_NO.'!</p>';
        }
        else {
             echo'
			<table id="nice_table" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td width="10"></td>
					<td class="tbb" width="160"><b>'.L_REZ_RESPONDENT.'</b></td>
					<td class="tbl tbb"><b>'.L_ASK_LECTIE.'</b></td>
					<td class="tbl tbb" width="140"><b>'.L_REZ_LAST.'</b></td>
					<td class="tbl tbb" width="120"></td>
					<td width="10"></td>
				</tr>';
            
            while($row = mysql_fetch_object($sql)) {    
                $titlu_lectie = getOneValue("lectii", "id", $row->id_lectie, "titlu");
            
                if($row->stare == 3 OR $row->stare == 1) {
                     // corectat
                     $stare='<span class="menu menu_active"><a href="?act=rezolvari&op=view&id='.$row->id.'">'.L_REZ_COR.'</a></span>';
                }
                else {
                     // de corectat
                     $stare='<span class="menu menu_red"><a href="?act=rezolvari&op=view&id='.$row->id.'">'.L_REZ_DECOR.'</a></span>';
                }
                echo'
			<tr>
				<td width="10"></td>
				<td>'.$row->nume.'</td>
				<td>'.$titlu_lectie.'</td>
				<td>'.date('d.m.Y H:i', $row->last_update).'</td>
				<td>'.$stare.'</td>
				<td width="10"></td>
			</tr>';
                
            }
             $inapoi  = max($page-1, 1);
             $inainte = min($page+1, $nrpages);
            
            if($page == $inapoi) { $inapoi = '';
            } else { 
                 $inapoi = '<span style="font-size:11px;">&laquo;</span> <a href="index.php?page='.$inapoi.'&act=rezolvari">'.L_PREV.'</a>&nbsp;&nbsp;';
            }
            if($page == $inainte) { $inainte = '';
            } else { 
                 $inainte = '&nbsp;&nbsp;<a href="index.php?page='.$inainte.'&act=rezolvari">'.L_NEXT.'</a> <span style="font-size:11px;">&raquo;</span>';
            }
             echo'<tr><td width="10"></td><td colspan="4">'.$inapoi.'<b>'.$page.'</b> '.L_DIN.' <b>'.$nrpages.'</b>'.$inainte.'</td><td width="10"></td></tr>';
             echo'</table>';
        }
        mysql_free_result($sql);
        break;
        
    case'view':
        
        if(isset($_POST['buton'])) {
             $id_lectie = $_POST['id_lectie'];
             $id_user = $_POST['id_user'];
            if($_POST['buton'] == ''.L_REZ_PROMOVAT.'') { 
                email_subordonat($id_user, 1);
                mysql_query("UPDATE status_lectie SET stare=3 WHERE id_lectie='$id_lectie' AND id_user='$id_user'") or trigger_error(mysql_error(), E_USER_ERROR);
            }
            elseif($_POST['buton'] == ''.L_REZ_NEPROMOVAT.'') {
                email_subordonat($id_user, 0);
                mysql_query("UPDATE status_lectie SET stare=0 WHERE id_lectie='$id_lectie' AND id_user='$id_user'") or trigger_error(mysql_error(), E_USER_ERROR);
            }
            else {
            }
             header("location:index.php?act=rezolvari");
        }
        else {
        }
        
        echo'&raquo; <a href="?act=rezolvari">'.L_INAPOI_REZOLVARI.'</a><br/><br/>';
        $id = mysql_real_escape_string($_GET['id']);
        $id_lectie = getOneValue("status_lectie", "id", $id, "id_lectie");
        $id_user = getOneValue("status_lectie", "id", $id, "id_user");
        $stare = getOneValue("status_lectie", "id", $id, "stare");
        $nume = getOneValue("useri", "id", $id_user, "nume");
        $user = getOneValue("useri", "id", $id_user, "user");
        $nivel = getOneValue("useri", "id", $id_user, "nivel");
        echo'
		
		<table width="100%">
			<tr>
				<td valign="top">
				
					<table id="nice_table" width="100%" cellspacing="0" cellpadding="0">
						<tr class="thead">
							<td width="2"></td>
							<td>'.L_REZ_IR.'</td>
							<td width="70"></td>
							<td width="2"></td>
						</tr>';
        
        $sql = mysql_query("SELECT id,titlu,tip,raspuns_sugerat FROM intrebari WHERE id_lectie='$id_lectie'") or trigger_error(mysql_error(), E_USER_ERROR);
        if(mysql_num_rows($sql) == 0) {
        }
        else {
            while($row = mysql_fetch_object($sql)) {
                $raspunsuri = array();
                $corect= corectare_intrebare($row->id, $id_user);
                $sql1 = mysql_query("SELECT valoare,id_raspuns FROM rezolvari WHERE id_intrebare='$row->id' AND id_user='$id_user'") or trigger_error(mysql_error(), E_USER_ERROR);
                if(mysql_num_rows($sql1) == 0) { $raspunsuri[] = '-'; 
                }
                else {
                    while($row1 = mysql_fetch_object($sql1)) {
                        if($row->tip == "raspuns_deschis") { $corectare=''; 
                        }
                        else {
                            $corectare = getOneValue("raspunsuri", "id", $row1->id_raspuns, "corect");
                            if($corectare == 1) {
                                $corectare=' <img src="images/corect.png" alt="Corect" style="vertical-align:middle;"/>';
                            }
                            else {
                                $corectare=' <img src="images/gresit.png" alt="Gresit" style="vertical-align:middle;"/>';
                            }
                        }
                        $raspunsuri[] = ''.$row1->valoare.''.$corectare.'';
                    }
                }
                mysql_free_result($sql1);
        
                if($row->raspuns_sugerat == "") { $raspuns_sugerat=''; 
                }
                else {
                    $raspuns_sugerat='<img src="images/light.png" style="vertical-align:middle;" class="sugerat" rel="'.stripslashes($row->raspuns_sugerat).'"/>';
                }
        
                if($stare == 3) {
                }
                else {
            
                    if($corect == '') {
                        $butoane='<form method="post" action="?act=rezolvari&op=corectare">
				<input type="submit" value=" " class="btn_done" name="'.L_REZ_CORECT.'"/>
				<input type="submit" value=" " class="btn_wrong" name="'.L_REZ_GRESIT.'"/>
				<input type="hidden" name="id_intrebare" value="'.$row->id.'"/>
				<input type="hidden" name="id_user" value="'.$id_user.'"/>
				<input type="hidden" name="back" value="'.$id.'"/>
				</form>';
                    }
                    elseif($corect==0) {
                        $butoane='<img src="images/wrong_on.png"/> <img src="images/done_off.png"/>';
                    }
                    elseif($corect==1) {
                        $butoane='<img src="images/done_on.png"/> <img src="images/wrong_off.png"/>';
                    }
                }
        
                echo'<tr>
				<td width="2"></td>
				<td class="tbb" style="line-height:20px;">
					<b>I: '.stripslashes($row->titlu).'</b> '.$raspuns_sugerat.'<br/>
					<span class="red">R: '.implode(', ', $raspunsuri).'</span>
				</td>
				<td class="tbl tbb">'.$butoane.'</td>
				<td width="2"></td>
			</tr>';
            }
        }
        mysql_free_result($sql);
        
        echo'<tr>
							<td colspan="3" align="right">';
        
        if($stare == 3) { echo'<span style="color:green;">'.L_REZ_PROMOVAT.'</span>'; 
        }
        else {
             echo'<form method="post" action="">
				<input type="submit" class="menu menu_active" value="'.L_REZ_PROMOVAT.'" name="buton" style="float:right; margin-left:10px;"/>
				<input type="submit" class="menu menu_red" value="'.L_REZ_NEPROMOVAT.'" style="float:right;" name="buton"/>
				<input type="hidden" name="id_user" value="'.$id_user.'"/>
				<input type="hidden" name="id_lectie" value="'.$id_lectie.'"/>
				</form>';
        }
        
        echo'</td>
							<td width="2"></td>
						</tr>
					</table>

				</td>
				<td valign="top" width="200" style="padding-left:10px;">';
        
        $sql2 = mysql_query("SELECT COUNT(l.id) AS tot FROM teste t LEFT JOIN lectii l ON l.id_test=t.id WHERE t.nivel=$nivel") or trigger_error(mysql_error(), E_USER_ERROR);
        if(mysql_num_rows($sql2) == 0) {$total_lectii = 0;
        }
        else {
            $row2 = mysql_fetch_object($sql2);
            $total_lectii = $row2->tot;
        }
        mysql_free_result($sql2);
                
        $sql3 = mysql_query("SELECT COUNT(l.id) AS tot FROM teste t LEFT JOIN lectii l ON l.id_test=t.id LEFT JOIN status_lectie s ON s.id_lectie=l.id WHERE t.nivel=$nivel AND id_user=$id_user") or trigger_error(mysql_error(), E_USER_ERROR);
        if(mysql_num_rows($sql3) == 0) {$total_completate = 0;
        }
        else {
            $row3 = mysql_fetch_object($sql3);
            $total_completate = $row3->tot;
        }
        mysql_free_result($sql3);
                
        $procent = round((100*$total_completate)/$total_lectii);
                
                    
        echo'<table id="nice_table" width="100%" cellspacing="0" cellpadding="0">
						<tr class="thead">
							<td style="font-weight:normal;"><b>'.L_REZ_UTILIZATOR.'</b></td>
						</tr>
						<tr>
							<td>
								<b>'.$nume.'</b> - NIVEL '.$nivel.'
								<p style="margin:0;">'.$total_completate.' '.L_REZ_NR.' '.$total_lectii.'</p>
								<div class="tbb" style="margin:10px 0px;"></div>
								'.L_REZ_PROCENT.'
								<h3 style="font-size:30px; margin:0; padding:0;">'.$procent.'%</h3>
								<div class="tbb" style="margin:10px 0px;"></div>
								'.L_REZ_AVANS.':<br/>
								<form method="post" action="?act=rezolvari&op=nivel">
									'.generate_nivel($nivel).'
									<input type="hidden" name="id_user" value="'.$id_user.'"/>
									<input type="hidden" name="back" value="'.$id.'"/>
									<br/>
									<input type="submit" name="ok" class="menu" value="'.L_REZ_AVANSEAZA.'" style="margin-top:10px;"/>
								</form>
								<div class="tbb" style="margin:10px 0px;"></div>
								<a href="?act=inbox&op=compose&destinatar='.$user.'" style="text-decoration:none; color:#988a78;"><img src="images/email.png" style="vertical-align:middle;"/> '.L_REZ_TRIMITE.'</a>
							</td>
						</tr>
					</table>
					
				</td>
			</tr>
		</table>';
        
        break;
        
    case'nivel':
        $id_user = $_POST['id_user'];
        $nivel = $_POST['nivel'];
        $back = $_POST['back'];
        if($nivel == "") {
        }
        else {
             mysql_query("UPDATE useri SET nivel='$nivel' WHERE id='$id_user'") or trigger_error(mysql_error(), E_USER_ERROR);
             // automatically make them a mentor if they reached level 3
            if ($nivel == 3) { 
                mysql_query("UPDATE useri SET afis_mentor='1' WHERE id='$id_user'") or trigger_error(mysql_error(), E_USER_ERROR);
            }
             email_nivel($id_user, $nivel);
        }
        header("location:?act=rezolvari&op=view&id=$back");
        break;
        
    case'corectare':
        $id_user = $_POST['id_user'];
        $id_intrebare = $_POST['id_intrebare'];
        $back = $_POST['back'];
        
        if(isset($_POST[L_REZ_CORECT])) {
             $corect=1;
        }
        elseif(isset($_POST[L_REZ_GRESIT])) {
             $corect=0;
        }
        mysql_query("UPDATE status_intrebare SET corect='$corect' WHERE id_intrebare='$id_intrebare' AND id_user='$id_user'") or trigger_error(mysql_error(), E_USER_ERROR);
        header("location:?act=rezolvari&op=view&id=$back");
        break;
    }
    
    ?>
    
</div>

<?php
$content = ob_get_clean();
ob_end_clean();
?>