<?php
/**
 * Lectii.php File
 * 
 * Lessions view page - included from ../index.php
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */

ob_start();
acces(); 
if(nivel($_SESSION['log_id']) != 5) { header("location:/"); 
}
require "includes/fckeditor/fckeditor_php5.php";
$id_test = mysql_real_escape_string($_GET['id_test']);
$test = getOneValue("teste", "id", $id_test, "titlu");
?>

<div id="content">
    <h1><?php echo L_LEC_PENTRU?> <i style="font-size:22px;"><?php echo $test; ?></i> &raquo; <a href="?act=lectii&op=add&id_test=<?php echo $id_test;?>"><?php echo L_LEC_NOUA?></a></h1>
    <br/>
    
    <?php
    echo $_SESSION['mesaj'];
    unset($_SESSION['mesaj']);
    switch($_GET['op']) {
    case'':
        
        if(!isset($_GET["page"]) ) { $page = 1; 
        } else { $page = mysql_real_escape_string($_GET["page"]); 
        }    
        $q = "SELECT * FROM lectii WHERE id_test='$id_test' ORDER BY ord ASC";
        $limit = 20;
        $query = mysql_query($q) or trigger_error(mysql_error(), E_USER_ERROR);
        $results= mysql_num_rows($query);
        $nrpages = ceil($results/$limit);
        $limitvalue = $page * $limit - ($limit); 
        $sql = mysql_query("$q LIMIT $limitvalue, $limit") or trigger_error(mysql_error(), E_USER_ERROR);
        $dela = $limitvalue +1 ;
        $la   = $limitvalue + $limit;
        if ($la > $results ) { $la = $results ; 
        }
        if(mysql_num_rows($sql) == 0) {
        }
        else {
             echo'
			<table id="nice_table" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td width="10"></td>
					<td class="tbb"><b>'.L_LEC_TITLU.'</b></td>
					<td class="tbl tbb" width="80"><b>Op.</b></td>
					<td width="10"></td>
				</tr>';
            while($row = mysql_fetch_object($sql)) {
                echo'<tr>
						<td width="10"></td>
						<td>'.stripslashes($row->titlu).' &raquo; <a href="?act=intrebari&id_lectie='.$row->id.'">pyetjet</a></td>
						<td align="center">
							<a href="?act=lectii&op=edit&id='.$row->id.'&id_test='.$id_test.'"><img src="images/edit.gif"/></a>
							<a href="?act=lectii&op=delete&id='.$row->id.'&id_test='.$id_test.'" onclick="return confirm(\''.L_SIGUR.'\');"><img src="images/delete.png"/></a>
						</td>
						<td width="10"></td>
					</tr>';
            }
             $inapoi  = max($page-1, 1);
             $inainte = min($page+1, $nrpages);
            
            if($page == $inapoi) { $inapoi = '';
            } else { 
                 $inapoi = '<span style="font-size:11px;">&laquo;</span> <a href="index.php?page='.$inapoi.'&act=lectii&id_test='.$id_test.'">'.L_PREV.'</a>&nbsp;&nbsp;';
            }
            if($page == $inainte) { $inainte = '';
            } else { 
                 $inainte = '&nbsp;&nbsp;<a href="index.php?page='.$inainte.'&act=teste&act=lectii&id_test='.$id_test.'">'.L_NEXT.'</a> <span style="font-size:11px;">&raquo;</span>';
            }
             echo'<tr><td width="10"></td><td colspan="2">'.$inapoi.'<b>'.$page.'</b> '.L_DIN.' <b>'.$nrpages.'</b>'.$inainte.'</td><td width="10"></td></tr>';
             echo'</table>';
        }
        mysql_free_result($sql);
        break;
        
    case'add':
        echo'&raquo; <a href="?act=lectii&id_test='.$id_test.'">'.L_INAPOI_LECTII.'</a><br/><br/>';
        if(isset($_POST['ok'])) {
             $id_test = mysql_real_escape_string($_POST['id_test']);
             $titlu = diacritice(mysql_real_escape_string($_POST['titlu']));
             $descriere = diacritice(mysql_real_escape_string($_POST['descriere']));
             $ord = mysql_real_escape_string($_POST['ord']);
            
            if(($id_test == "") OR ($titlu == "") OR ($ord == "")) {
                echo'<span id="error">'.L_CAMPURI_INCOMPLETE.'!</span>';
            }
            else {
                mysql_query("INSERT INTO lectii SET id_test='$id_test',titlu='$titlu',descriere='$descriere',ord='$ord'");
                echo'<span id="done">'.L_LEC_SUCCES.'!</span>';
            }
            
        }
        else {
        }
        
        echo'<form method="post" action="">
		<table cellspacing="2" cellpadding="4" style="margin-top:5px;">
			<tr>
				<td width="100">'.L_LEC_TEST.':</td>
				<td><select name="id_test" class="selectus">
						<option value=""> -- </option>';
        $sql = mysql_query("SELECT id,titlu FROM teste ORDER BY id DESC") or trigger_error(mysql_error(), E_USER_ERROR);
        if(mysql_num_rows($sql) == 0) {
        }
        else {
            while($row = mysql_fetch_object($sql)) {
                if($row->id == $id_test) { $sel=" selected"; 
                } else {$sel="";
                }
                echo'<option value="'.$row->id.'"'.$sel.'>'.$row->titlu.'</option>';
            }
        }
        mysql_free_result($sql);
        echo'</select></td>
			</tr>
			<tr>
				<td>'.L_LEC_TITLU.':</td>
				<td><input type="text" class="inputus" size="40" name="titlu" value=""/></td>
			</tr>
			<tr>
				<td valign="top">'.L_LEC_DESCRIERE.'</td>
				<td>';
        $oFCKeditor = new FCKeditor('descriere');
        $oFCKeditor->BasePath = 'includes/fckeditor/' ;
        $oFCKeditor->Width     = 800;
        $oFCKeditor->Height     = 400;
        $oFCKeditor->Create();
        echo'</td>
			</tr>
			<tr>
				<td>Ord:</td>
				<td><input type="text" class="inputus" style="width:20px;" name="ord" value=""/> ('.L_LEC_HINT.')</td>
			</tr>
			<tr>
				<td><input type="hidden" name="id_test" value="'.$id_test.'"/></td>
				<td><input type="submit" name="ok" value="'.L_ADAUGA.'" class="menu"/></td>
			</tr>
		</table>
		</form>';
        break;
        
    case'edit':
        echo'&raquo; <a href="?act=lectii&id_test='.$id_test.'">'.L_INAPOI_LECTII.'</a><br/><br/>';
        if(isset($_POST['ok'])) {
             $titlu = diacritice(mysql_real_escape_string($_POST['titlu']));
             $descriere = diacritice(mysql_real_escape_string($_POST['descriere']));
             $id = mysql_real_escape_string($_POST['id']);
             $id_test = mysql_real_escape_string($_POST['id_test']);
             $ord = mysql_real_escape_string($_POST['ord']);
            
            if(($id_test == "") OR ($titlu == "") OR ($ord == "")) {
                echo'<span id="error">'.L_CAMPURI_INCOMPLETE.'!</span>';
            }
            else {
                mysql_query("UPDATE lectii SET titlu='$titlu',descriere='$descriere',id_test='$id_test',ord='$ord' WHERE id='$id'");
                echo'<span id="done">'.L_CU_SUCCES.'!</span>';
            }
            
            
        }
        else {
        }
        
        $id = mysql_real_escape_string($_GET['id']);
        $sql = mysql_query("SELECT * FROM lectii WHERE id='$id'");
        $row = mysql_fetch_object($sql);
        echo'<form method="post" action="">
		<table cellspacing="2" cellpadding="4" style="margin-top:5px;">
			<tr>
				<td width="100">'.L_LEC_TEST.':</td>
				<td><select name="id_test" class="selectus">
						<option value=""> -- </option>';
        $sql1 = mysql_query("SELECT id,titlu FROM teste ORDER BY id DESC") or trigger_error(mysql_error(), E_USER_ERROR);
        if(mysql_num_rows($sql1) == 0) {
        }
        else {
            while($row1 = mysql_fetch_object($sql1)) {
                if($row1->id == $row->id_test) { $sel=" selected"; 
                } else {$sel="";
                }
                echo'<option value="'.$row1->id.'"'.$sel.'>'.$row1->titlu.'</option>';
            }
        }
        mysql_free_result($sql);
        echo'</select></td>
			</tr>
			<tr>
				<td>'.L_LEC_TITLU.':</td>
				<td><input type="text" class="inputus" size="40" name="titlu" value="'.stripslashes($row->titlu).'"/></td>
			</tr>
			<tr>
				<td valign="top">'.L_LEC_DESCRIERE.'</td>
				<td>';
        $oFCKeditor = new FCKeditor('descriere');
        $oFCKeditor->BasePath = 'includes/fckeditor/' ;
        $oFCKeditor->Value     = $row->descriere;
        $oFCKeditor->Width     = 800;
        $oFCKeditor->Height     = 400;
        $oFCKeditor->Create();
        echo'</td>
			</tr>
			<tr>
				<td>Ord:</td>
				<td><input type="text" class="inputus" style="width:20px;" name="ord" value="'.$row->ord.'"/> ('.L_LEC_HINT.')</td>
			</tr>
			<tr>
				<td><input type="hidden" name="id" value="'.$id.'"/></td>
				<td><input type="submit" name="ok" value="'.L_MODIFICA.'" class="menu"/></td>
			</tr>
		</table>
		</form>';
        break;
        
    case'delete':
        $id = mysql_real_escape_string($_GET['id']);
        sterge_lectie($id);
        $_SESSION['mesaj'] = '<span id="done">'.L_LEC_STERS.'!</span>';
        header("location:index.php?act=lectii&id_test=$id_test");
        break;
        
    }
    ?>
</div>

<?php
$content = ob_get_clean();
ob_end_clean();
?>