-- MySQL dump 10.13  Distrib 5.6.31, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: hapitjeter_dump
-- ------------------------------------------------------
-- Server version	5.6.31-0ubuntu0.15.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `biblie`
--

DROP TABLE IF EXISTS `biblie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `biblie` (
  `id` int(6) NOT NULL DEFAULT '0',
  `testament` tinyint(1) NOT NULL DEFAULT '1',
  `book_id` int(2) NOT NULL DEFAULT '0',
  `capitol` int(3) NOT NULL DEFAULT '0',
  `verset` int(3) NOT NULL DEFAULT '0',
  `continut` text COLLATE utf8_romanian_ci NOT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_romanian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `biblie_sursa`
--

DROP TABLE IF EXISTS `biblie_sursa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `biblie_sursa` (
  `id` int(5) NOT NULL DEFAULT '0',
  `titlu` varchar(40) NOT NULL,
  UNIQUE KEY `book_id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `intrebari`
--

DROP TABLE IF EXISTS `intrebari`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `intrebari` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_lectie` int(11) NOT NULL,
  `tip` text COLLATE utf8_romanian_ci NOT NULL,
  `titlu` text COLLATE utf8_romanian_ci NOT NULL,
  `descriere` longtext COLLATE utf8_romanian_ci NOT NULL,
  `raspuns_sugerat` longtext COLLATE utf8_romanian_ci NOT NULL,
  `ord` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_romanian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lectii`
--

DROP TABLE IF EXISTS `lectii`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lectii` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_test` int(11) NOT NULL,
  `titlu` text COLLATE utf8_romanian_ci NOT NULL,
  `descriere` longtext COLLATE utf8_romanian_ci NOT NULL,
  `ord` varchar(2) COLLATE utf8_romanian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_romanian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mesagerie`
--

DROP TABLE IF EXISTS `mesagerie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mesagerie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_expeditor` int(11) NOT NULL,
  `id_destinatar` int(11) NOT NULL,
  `subiect` text COLLATE utf8_romanian_ci NOT NULL,
  `mesaj` longtext COLLATE utf8_romanian_ci NOT NULL,
  `data` varchar(20) COLLATE utf8_romanian_ci NOT NULL,
  `citit` varchar(1) COLLATE utf8_romanian_ci NOT NULL,
  `trimis` varchar(1) COLLATE utf8_romanian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=319 DEFAULT CHARSET=utf8 COLLATE=utf8_romanian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raspunsuri`
--

DROP TABLE IF EXISTS `raspunsuri`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raspunsuri` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_intrebare` int(11) NOT NULL,
  `label` text COLLATE utf8_romanian_ci NOT NULL,
  `corect` varchar(1) COLLATE utf8_romanian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=169 DEFAULT CHARSET=utf8 COLLATE=utf8_romanian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rezolvari`
--

DROP TABLE IF EXISTS `rezolvari`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rezolvari` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_intrebare` int(11) NOT NULL,
  `id_raspuns` int(11) NOT NULL,
  `valoare` longtext COLLATE utf8_romanian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1827 DEFAULT CHARSET=utf8 COLLATE=utf8_romanian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `setari`
--

DROP TABLE IF EXISTS `setari`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `setari` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `mesaj` longtext NOT NULL,
  `lang` varchar(2) NOT NULL DEFAULT 'ro',
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `status_intrebare`
--

DROP TABLE IF EXISTS `status_intrebare`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status_intrebare` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_intrebare` int(11) NOT NULL,
  `corect` varchar(1) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=914 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `status_lectie`
--

DROP TABLE IF EXISTS `status_lectie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status_lectie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_lectie` int(11) NOT NULL,
  `stare` varchar(1) COLLATE utf8_romanian_ci NOT NULL COMMENT '1- incomplet - 2-in asteptare 3- complet',
  `last_update` varchar(20) COLLATE utf8_romanian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=151 DEFAULT CHARSET=utf8 COLLATE=utf8_romanian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `teste`
--

DROP TABLE IF EXISTS `teste`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teste` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titlu` text COLLATE utf8_romanian_ci NOT NULL,
  `descriere` longtext COLLATE utf8_romanian_ci NOT NULL,
  `nivel` varchar(1) COLLATE utf8_romanian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_romanian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `useri`
--

DROP TABLE IF EXISTS `useri`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `useri` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `nivel` varchar(1) COLLATE utf8_romanian_ci NOT NULL,
  `user` text COLLATE utf8_romanian_ci NOT NULL,
  `parola` text COLLATE utf8_romanian_ci NOT NULL,
  `nume` text COLLATE utf8_romanian_ci NOT NULL,
  `email` text COLLATE utf8_romanian_ci NOT NULL,
  `ym` text COLLATE utf8_romanian_ci NOT NULL,
  `varsta` int(2) NOT NULL,
  `oras` text COLLATE utf8_romanian_ci NOT NULL,
  `profesie` text COLLATE utf8_romanian_ci NOT NULL,
  `telefon` text COLLATE utf8_romanian_ci NOT NULL,
  `de_unde` text COLLATE utf8_romanian_ci NOT NULL,
  `last_login` varchar(20) COLLATE utf8_romanian_ci NOT NULL,
  `poza` text COLLATE utf8_romanian_ci NOT NULL,
  `descriere` text COLLATE utf8_romanian_ci NOT NULL,
  `afis_mentor` varchar(1) COLLATE utf8_romanian_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=84 DEFAULT CHARSET=utf8 COLLATE=utf8_romanian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-27 15:50:31
