#! /bin/bash

set -e

# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

print_help() {
  echo "deploy.sh [opts] user@host.com location 
    Deploys hapitjeter to a remote server using rsync over ssh
    
  -p    the SSH port, defaults to 22
  -i    the SSH identity file, defaults to ~/.ssh/id_rsa
  -f    force the install, overwriting the directory on the remote server 
          even if it doesn't look like hapitjeter
  
  user@host.com  The required hostname
  location       the location on the server to deploy, like 'public_html/hapitjeter'
  "
}

# Initialize our own variables:
SSH_IDENTITY_FILE="~/.ssh/id_rsa"
SSH_PORT=22

while getopts "hf?p:i:" opt; do
    case "$opt" in
    h|\?)
        print_help
        exit 0
        ;;
    p)  SSH_PORT=$OPTARG
        ;;
    i)  SSH_IDENTITY_FILE=$OPTARG
        ;;
    f)  FORCE=true
        ;;
    esac
done

shift $((OPTIND-1))

[ "$1" = "--" ] && shift

HOST=$1
[[ -z "$HOST" ]] && (>&2 echo "You need to provide a hostname in a format like bob@hapitjeter.net") && exit -1
LOCATION=$2
[[ -z "$LOCATION" ]] && (>&2 echo "You need to provide a location on the server, like 'public_html/hapitjeter'") && exit -1

# if the directory doesnt exist, is empty, or has a VERSION file it's ok to overwrite.  In either of these 3 cases this returns an empty string.
EXISTING=$(ssh -i $SSH_IDENTITY_FILE -p $SSH_PORT $HOST "([[ ! -d "$LOCATION" ]] || [[ -f "$LOCATION/VERSION" ]]) || ls $LOCATION")
if [[ ! -z "$EXISTING" ]]; then
  if [[ ! $FORCE ]]; then
    (>&2 echo -e "\033[1;31mThis doesn't look like a hapitjeter install!  Are you sure you want to overwrite this?  Rerun with -f to force overwrite.\033[0m"; echo $EXISTING)
    exit -1
  else
    echo -e "\033[1;33mForcing deploy over the following files\033[0m"
    echo $EXISTING
  fi
fi 

# prep the location for rsync, doing a backup if necessary
OLD_VERSION=$(ssh -i $SSH_IDENTITY_FILE -p $SSH_PORT $HOST \
  "mkdir -p ~/backups; \
  ([[ -d "$LOCATION" ]] && tar -C $LOCATION -czf ~/backups/$(basename $LOCATION)-\`cat $LOCATION/VERSION\`-\`date +'%Y%m%d'\`.tar.gz .); \
  mkdir -p $LOCATION;  \
  (cat $LOCATION/VERSION || echo 'nothing')")

# rsync over the files to the location
rsync -avz -e "ssh -i $SSH_IDENTITY_FILE -p $SSH_PORT" --cvs-exclude --exclude=config.ini --progress . $HOST:$LOCATION

VERSION=$(ssh -i $SSH_IDENTITY_FILE -p $SSH_PORT $HOST "chmod -R g-w $LOCATION; cat $LOCATION/VERSION")
echo -e "\033[1;32msuccessfully deployed $VERSION to $HOST:$LOCATION, overwriting $OLD_VERSION\033[0m"