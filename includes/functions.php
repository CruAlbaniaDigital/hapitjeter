<?php
/**
 * Functions.php File
 *
 * Defines global functions used during the entire PHP lifecycle
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */

session_start();
set_time_limit(0);
error_reporting(0);

// end config. de aici s-a zis cu editarea :)

require "class.mail.php";
require "class.smtp.php";

/**
 * Connects to mysql using defined constants
 * The following constants must be defined:
 *   MYSQL_HOST: the hostname of the MySQL server
 *   MYSQL_USER: the username to use in connecting to mysql
 *   MYSQL_PASS: the password to use in connecting to mysql
 *   MYSQL_NAME: the name of the schema to use in mysql
 *
 * Raises E_USER_ERROR if cannot connect
 *
 * @return void
 */
function conectare()
{
    $link = mysql_connect(MYSQL_HOST, MYSQL_USER, MYSQL_PASS) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db(MYSQL_NAME) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_set_charset('utf8', $link);
}

/**
 * Executes a select query returning a single value from the top result
 *
 * @param string $tabel         the table to select from
 * @param string $camp          the columnt on which to select
 * @param string $valoare       the value to match to the select column
 * @param string $camp_returnat the column to return from the result row
 *
 * @return object the single value returned from the result row
 */
function getOneValue($tabel,$camp,$valoare,$camp_returnat)
{
    $sql = mysql_query("SELECT * FROM $tabel WHERE $camp='$valoare'") or trigger_error(mysql_error(), E_USER_ERROR);
    if(mysql_num_rows($sql) == 0) {return 0;
    }
    else {
        $row = mysql_fetch_object($sql);
        return stripslashes($row->$camp_returnat);
    }
}

/**
 * Tests an email against a regex to ensure it is valid
 *
 * @param string $email The email to test
 *
 * @return bool true if the email matches the validation regex
 */
function is_valid($email)
{
    if (!eregi("^[a-z0-9\._-]+@+[a-z0-9\._-]+\.+[a-z]{2,6}$", $email)) { return false;
    } else { return true;
    }
}

/**
 * Sends an email to the given email address
 *
 * @param string $text    The text of the email
 * @param string $from    The from address of the email
 * @param string $to      The to address of the email
 * @param string $subject The subject line of the email
 *
 * @return void
 */
function sendHTMLemail($text,$from,$to,$subject)
{
    /*
    $mail             = new PHPMailer();
    $mail->IsSMTP();
    $mail->SMTPDebug  = 2;
    $mail->SMTPAuth   = true;
    $mail->Host       = "smtp.gmail.com";
    $mail->Port       = 25;
    $mail->Username   = "lavdiafrim26@gmail.com";
    $mail->Password   = "";
    $mail->SetFrom($from, DENUMIRE);
    $mail->AddReplyTo($from,DENUMIRE);
    $mail->Subject    = $subject;
    $mail->AltBody    = strip_tags($text);
    $mail->MsgHTML($text);
    $mail->AddAddress($to);

    if(!$mail->Send()) {
    //echo "Mailer Error: " . $mail->ErrorInfo;
    return 0;
    } else {
    return 1;
    }
    */

    // trying php default mailing system (JDV)

    // To send HTML mail, the Content-type header must be set, if utf-8 doesn't work, use iso-8859-1
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

    // Additional headers
    $headers .= 'To: ' . $to . "\r\n";
    $headers .= 'From: ' . $from . "\r\n";

    if (mail($to, $subject, $text, $headers)) {
        return 1;
    }
    else {
        echo "mail error";
        return 0;
    }
}

/**
 * Checks the _SESSION variable to see if the user is logged in
 *
 * @return bool true if the 'logged' indicator is set
 */
function is_logged()
{
    if($_SESSION['logged'] != "") {
        return true;
    }
    else {
        return false;
    }
}

/**
 * Checks login status, and if not logged in redirects the user to the homepage.
 *
 * @return void
 */
function acces()
{
    if(is_logged()) {
    }
    else {
        header("location:index.php?act=login");
    }
}

/**
 * Gets the user's level from the database
 *
 * @param int $id_user the database ID of the user
 *
 * @return void
 */
function nivel($id_user)
{
    return getOneValue("useri", "id", $id_user, "nivel");
}

function generate_tree($parent_id)
{
    $nume = getOneValue("useri", "id", $parent_id, "nume");
    $user = getOneValue("useri", "id", $parent_id, "user");
    $sql = mysql_query("SELECT id,nume,user,nivel FROM useri WHERE parent_id=$parent_id") or trigger_error(mysql_error(), E_USER_ERROR);
    if(mysql_num_rows($sql) == 0) {
        //echo'[{v:\'last'.$parent_id.'\', f:\'<div style="color:red">0 subordonati</div>\'}, \''.$user.'\'],';
    }
    else {
        while($row = mysql_fetch_object($sql)) {
            // nr lectii de la nivel
            $sql1 = mysql_query("SELECT COUNT(s.id) AS nr_lectii FROM teste t,lectii l,status_lectie s WHERE t.nivel=$row->nivel AND t.id=l.id_test AND s.id_lectie=l.id AND s.id_user=$row->id") or trigger_error(mysql_error(), E_USER_ERROR);
            if(mysql_num_rows($sql1) == 0) { $nr_lectii='0';
            }
            else {
                $row1 = mysql_fetch_object($sql1);
                $nr_lectii = $row1->nr_lectii;
            }

            echo'[{v:\''.$row->user.'\', f:\''.$row->nume.'<div style="color:#666;">'.L_STAT_NIVEL.': <b>'.$row->nivel.'</b><br/>'.L_TST_LECTII.': <b>'.$nr_lectii.'</b><br/><a href="?act=inbox&op=compose&destinatar='.$row->user.'" title="Trimite mesaj"><img src="images/mesaj.png"/></a></div>\'}, \''.$user.'\'],';
            generate_tree($row->id);
        }
    }
    mysql_free_result($sql);
}

function sterge_test($id)
{
    mysql_query("DELETE FROM teste WHERE id='$id'");
    $sql = mysql_query("SELECT id FROM lectii WHERE id_test='$id'");
    if(mysql_num_rows($sql) == 0) {
    }
    else {
        while($row = mysql_fetch_object($sql)) {
            sterge_lectie($row->id);
        }
    }
    mysql_free_result($sql);
}

function sterge_lectie($id)
{
    mysql_query("DELETE FROM lectii WHERE id='$id'");
    mysql_query("DELETE FROM status_lectie WHERE id_lectie='$id'");
    $sql = mysql_query("SELECT id FROM intrebari WHERE id_lectie='$id'");
    if(mysql_num_rows($sql) == 0) {
    }
    else {
        while($row = mysql_fetch_object($sql)) {
            sterge_intrebare($row->id);
        }
    }
    mysql_free_result($sql);
}

function sterge_intrebare($id)
{
    mysql_query("DELETE FROM intrebari WHERE id='$id'");
    $sql = mysql_query("SELECT id FROM raspunsuri WHERE id_intrebare='$id'");
    if(mysql_num_rows($sql) == 0) {
    }
    else {
        while($row = mysql_fetch_object($sql)) {
            sterge_raspuns($row->id);
        }
    }
    mysql_free_result($sql);
    mysql_query("DELETE FROM rezolvari WHERE id_intrebare='$id'");
}

function sterge_raspuns($id)
{
    mysql_query("DELETE FROM raspunsuri WHERE id='$id'");
    mysql_query("DELETE FROM rezolvari WHERE id_raspuns='$id'");
}

function tip_intrebare($tip='',$selected='')
{
    $arr = array(
    'raspuns_deschis'=>TIP1,
    'grila_raspuns'=>TIP2,
    'grila_raspunsuri'=>TIP3
    );

    if($selected == 1) {
        $return='<select name="tip" class="selectus"><option value=""> -- </option>';
        foreach($arr AS $key=>$val) {
            if($tip == $key) {$sel=" selected";
            } else {$sel="";
            }
            $return.='<option value="'.$key.'"'.$sel.'>'.$val.'</option>';
        }
        $return.='</select>';
    }
    else {
        $return= $arr[$tip];
    }
    return $return;
}

function raspunsuri($tip,$id_intrebare,$id_lectie)
{
    $return='';
    $id_user = $_SESSION['log_id'];

    $sql = mysql_query("SELECT id,label FROM raspunsuri WHERE id_intrebare='$id_intrebare' ORDER BY id ASC") or trigger_error(mysql_error(), E_USER_ERROR);
    if(mysql_num_rows($sql) == 0) {
        $return.'<i>'.L_RAS_NO.'!</i>';
    }
    else {
        $return.='<table cellspacing="3" cellpadding="2" width="100%">';
        $corect = 1;
        while($row = mysql_fetch_object($sql)) {
            $sql1 = mysql_query("SELECT id,valoare FROM rezolvari WHERE id_intrebare='$id_intrebare' AND id_raspuns='$row->id' AND id_user='$id_user' LIMIT 0,1");
            if(mysql_num_rows($sql1) == 0) {
                $valoare='';
            }
            else {
                $row1 = mysql_fetch_object($sql1);
                $valoare = stripslashes(strip_tags($row1->valoare));
            }
            switch($tip) {
            case'raspuns_deschis':
                $return.='
					<input type="hidden" name="raspuns[]" value="'.$row->id.'"/>
					<tr>
						<td>'.stripslashes($row->label).'</td>
					</tr>
					<tr>
						<td><textarea name="valoare[]" class="inputus" style="height:90px">'.$valoare.'</textarea></td>
					</tr>';
                $js='<script type="text/javascript">
							function validare() {
								var check=0
								$("textarea[name^=\'valoare\']").each(
								    function() {
								    	if(this.value == "") { check++ }
								    }
								);
								if(check == 0) {}
								else {
									alert(\''.L_RAS_JS.'!\');
									return false;
								}
							}
						</script>';
                break;

            case'grila_raspuns':
                if($valoare == $row->label) { $sel=' checked';
                } else { $sel='';
                }
                $return.='
					<tr>
						<td width="20"><input type="radio" name="raspuns" value="'.$row->id.'" id="r'.$row->id.'"'.$sel.'/></td>
						<td><label for="r'.$row->id.'">'.stripslashes($row->label).'</label></td>
					</tr>';
                $js='<script type="text/javascript">
							function validare() {
								var check=0
								if($("input[name=\'raspuns\']").is(":checked")) {}
								else { check++; }

								if(check == 0) {}
								else {
									alert(\''.L_RAS_JS.'!\');
									return false;
								}
							}
						</script>';
                break;

            case'grila_raspunsuri':
                if($valoare == $row->label) { $sel=' checked';
                } else { $sel='';
                }
                $return.='
					<tr>
						<td width="20"><input type="checkbox" name="raspuns[]" value="'.$row->id.'" id="r'.$row->id.'"'.$sel.'/></td>
						<td><label for="r'.$row->id.'">'.stripslashes($row->label).'</label></td>
					</tr>';
                $js='<script type="text/javascript">
							function validare() {
								var check=0
								if($("input[name^=\'raspuns\']").is(":checked")) {}
								else { check++ }


								if(check == 0) {}
								else {
									alert(\''.L_RAS_JS.'\');
									return false;
								}
							}
						</script>';
                break;
            }




        }

        $sql0 = mysql_query("SELECT stare FROM status_lectie WHERE id_lectie='$id_lectie' AND id_user='$id_user' LIMIT 0,1");
        if(mysql_num_rows($sql0) == 0) {$afis_c='';
        }
        else {
            $row0 = mysql_fetch_object($sql0);
            $afis_c=$row0->stare;
        }
        mysql_free_result($sql0);

        $sql1 = mysql_query("SELECT corect FROM status_intrebare WHERE id_intrebare='$id_intrebare' AND id_user='$id_user' LIMIT 0,1") or trigger_error(mysql_error(), E_USER_ERROR);
        if(mysql_num_rows($sql1) == 0) {$corect='';
        }
        else {
            $row1 = mysql_fetch_object($sql1);
            if($row1->corect == 1) {
                $corect='<span style="color:white; background:green; padding:5px;">'.L_RAS_COR.'!</span>';
            }
            elseif($row1->corect == 0) {
                $corect='<span style="color:white; background:red; padding:5px;">'.L_RAS_GRE.'!</span>';
            }
            else { $corect='';
            }
        }
        mysql_free_result($sql1);


        if($afis_c == 0) {
            // lectie incorecta
            $corect = $corect;
        }
        elseif($afis_c == 1) {
            // lectie incomplet
            $corect = '';
        }
        elseif($afis_c == 2) {
            // lectie in asteptare
            $corect='';
        }
        elseif($afis_c == 3) {
            // lectie complet
            $corect = $corect;
        }
        else {
            $corect='';
        }

        $return.='<tr><td>'.$corect.'</td></tr></table>'.$js.'';
    }
    mysql_free_result($sql);

    return $return;
}

function preluare_raspunsuri($tip,$post)
{
    $return = array();
    switch($tip) {
    case'raspuns_deschis':
        $return['raspuns'] = $post['raspuns'];
        $return['valoare'] = $post['valoare'];
        break;

    case'grila_raspuns':
        $return['raspuns'][] = $post['raspuns'];
        $return['valoare'][] = getOneValue("raspunsuri", "id", $post['raspuns'], "label");
        break;

    case'grila_raspunsuri':
        $return['raspuns'] = $post['raspuns'];
        $valori = array();
        foreach($post['raspuns'] AS $raspuns) {
             $valori[] = getOneValue("raspunsuri", "id", $raspuns, "label");
        }
        $return['valoare'] = $valori;
        break;
    }
    return $return;
}

function stare_lectie($id_lectie,$id_user)
{
    $return = array();
    $sql = mysql_query("SELECT stare FROM status_lectie WHERE id_lectie='$id_lectie' AND id_user='$id_user' LIMIT 0,1") or trigger_error(mysql_error(), E_USER_ERROR);
    if(mysql_num_rows($sql) == 0) {
        $return['text']='<b>'.L_RAS_NECOMPLETAT.'</b>';
        $return['val'] = 0;
        $return['buton'] = L_RAS_COMPLETEAZA;

    }
    else {
        $row = mysql_fetch_object($sql);
        switch($row->stare) {
        case'0';
            $return['text']='<b>'.L_RAS_INCORECT.'</b>';
            $return['buton'] = L_RAS_COMPLETEAZA;
            break;

        case'1';
            if(procent_lectie($id_lectie, $id_user) == 100) {
                 $return['text']=''.L_RAS_ASTEPTARE.'';
                 $return['buton'] = L_RAS_REVIZUIRE;
            }
            else {
                 $return['text']=''.L_RAS_DECAT.' <b>'.procent_lectie($id_lectie, $id_user).'%</b>';
                 $return['buton'] = L_RAS_COMPLETEAZA;
            }
            break;

        case'2';
            $return['text']=''.L_RAS_ASTEPTARE.'';
            $return['buton'] = L_RAS_REVIZUIRE;
            break;

        case'3';
            $return['text']=''.L_RAS_COMPLETAT.'';
            $return['buton'] = '<img src="images/done_icon.png"/> '.L_RAS_COMPLETAT;
            break;
        }
        $return['val'] = $row->stare;
    }
    mysql_free_result($sql);
    return $return;
}

function procent_lectie($id_lectie,$id_user)
{
    $sql = mysql_query("SELECT id FROM intrebari WHERE id_lectie='$id_lectie'") or trigger_error(mysql_error(), E_USER_ERROR);
    $nr_intrebari = mysql_num_rows($sql);
    //
    $sql1 = mysql_query("SELECT DISTINCT(a.id_intrebare) FROM rezolvari a,intrebari b WHERE b.id_lectie='$id_lectie' AND b.id=a.id_intrebare AND a.id_user=$id_user") or trigger_error(mysql_error(), E_USER_ERROR);
    $rezolvate = mysql_num_rows($sql1);
    $procent = round(($rezolvate/$nr_intrebari)*100);
    return $procent;
}

function mesaje_noi()
{
    $id = $_SESSION['log_id'];
    $sql = mysql_query("SELECT id FROM mesagerie WHERE id_destinatar='$id' AND citit=0 AND trimis=0") or trigger_error(mysql_error(), E_USER_ERROR);
    $nr = mysql_num_rows($sql);
    if($nr == 0) {
    }
    else {
        return'(<b>'.$nr.'</b>)';
    }
}

function generate_destinatari($parent_id)
{
    $return='';
    $sql = mysql_query("SELECT id,nume,user FROM useri WHERE parent_id=$parent_id ORDER BY user ASC") or trigger_error(mysql_error(), E_USER_ERROR);
    if(mysql_num_rows($sql) == 0) {
    }
    else {
        while($row = mysql_fetch_object($sql)) {
            $nume = getOneValue("useri", "id", $parent_id, "nume");
            if($_GET['destinatar'] == $row->user) {
            }
            else {
                $return.='<option value="'.$row->id.'"'.$sel.'>'.$row->user.' ('.$row->nume.')</option>';
            }
            $return.=generate_destinatari($row->id);
        }
    }
    mysql_free_result($sql);
    return $return;
}

function generate_nivel($min_nivel)
{
    $min_nivel = $min_nivel+1;
    $max_nivel = getOneValue("useri", "id", $_SESSION['log_id'], "nivel");
    $return='<select name="nivel" class="selectus" style="margin-top:5px;"><option value="">--</option>';
    for($i=$min_nivel;$i<=$max_nivel;$i++) {
        if($i == $min_nivel) { $sel=" selected";
        } else {$sel='';
        }
        $return.='<option value="'.$i.'"'.$sel.'>'.$i.'</option>';
    }
    $return.='</select>';
    return $return;
}

function email_mesaj($id)
{
    $id_destinatar = getOneValue("mesagerie", "id", $id, "id_destinatar");
    $destinatar = getOneValue("useri", "id", $id_destinatar, "nume");
    $to = getOneValue("useri", "id", $id_destinatar, "email");

    $mesaj = str_replace("[destinatar]", $destinatar, L_INBOX_MAIL);
    $mesaj = str_replace("[site]", DENUMIRE, $mesaj);
    $mesaj = str_replace("[link]", ''.URL_ABSOLUT.'?act=inbox&op=read&id='.$id.'&tip=primite', $mesaj);

    sendHTMLemail($mesaj, "no-reply@".DENUMIRE."", $to, L_INBOX_SUBJECT);
}


function email_nivel($id_user,$nivel)
{
    $nume = getOneValue("useri", "id", $id_user, "nume");
    $to = getOneValue("useri", "id", $id_user, "email");

    $mesaj = str_replace("[nume]", $nume, L_NIVEL_MAIL);
    $mesaj = str_replace("[nivel]", $nivel, $mesaj);
    $mesaj = str_replace("[site]", DENUMIRE, $mesaj);
    $mesaj = str_replace("[link]", ''.URL_ABSOLUT.'?act=login&new_nivel='.$nivel, $mesaj);

    sendHTMLemail($mesaj, "no-reply@".DENUMIRE."", $to, L_NIVEL_SUBJECT);
}

function email_mentor($id_user)
{
    $nume = getOneValue("useri", "id", $id_user, "nume");
    $to = getOneValue("useri", "id", $id_user, "email");

    $mesaj = str_replace("[nume]", $nume, L_MENTOR_MAIL);
    $mesaj = str_replace("[site]", DENUMIRE, $mesaj);
    $mesaj = str_replace("[link]", ''.URL_ABSOLUT.'?act=login', $mesaj);

    sendHTMLemail($mesaj, "no-reply@".DENUMIRE."", $to, ''.L_MENTOR_SUBJECT.'');
}

function email_subordonat($id_user,$ce)
{
    $nume = getOneValue("useri", "id", $id_user, "nume");
    $to = getOneValue("useri", "id", $id_user, "email");
    switch($ce) {
    case'1':
        // lectie promovata
        $mesaj = str_replace("[nume]", $nume, L_SUB_CORECT_MAIL);
        $mesaj = str_replace("[site]", DENUMIRE, $mesaj);
        $mesaj = str_replace("[link]", ''.URL_ABSOLUT.'?act=login', $mesaj);
        sendHTMLemail($mesaj, "no-reply@".DENUMIRE."", $to, ''.L_SUB_CORECT_SUBJECT.'');
        break;

    case'0':
        //lectie gresita
        $mesaj = str_replace("[nume]", $nume, L_SUB_GRESIT_MAIL);
        $mesaj = str_replace("[site]", DENUMIRE, $mesaj);
        $mesaj = str_replace("[link]", ''.URL_ABSOLUT.'?act=login', $mesaj);
        sendHTMLemail($mesaj, "no-reply@".DENUMIRE."", $to, ''.L_SUB_GRESIT_SUBJECT.'');
        break;
    }

}

function rezolvari_noi()
{
    $return='';
    $id_parent = $_SESSION['log_id'];
    $sql = mysql_query("SELECT b.id FROM useri a,status_lectie b WHERE a.parent_id='$id_parent' AND a.id=b.id_user AND b.stare='2'") or trigger_error(mysql_error(), E_USER_ERROR);
    $nr = mysql_num_rows($sql);

    if($nr == 0) {
    }
    else {
        $return='(<b>'.$nr.'</b>)';
    }
    return $return;
}

function corectare_intrebare($id_intrebare,$id_user)
{
    $tip = getOneValue("intrebari", "id", $id_intrebare, "tip");

    $sql = mysql_query("SELECT corect FROM status_intrebare WHERE id_intrebare='$id_intrebare' AND id_user='$id_user'") or trigger_error(mysql_error(), E_USER_ERROR);
    if(mysql_num_rows($sql) == 0) {$i='';
    }
    else {
        while($row = mysql_fetch_object($sql)) {
            $i = $row->corect;
        }
    }
    mysql_free_result($sql);
    return $i;
}

function biblie($descriere)
{
    $descriere = stripslashes($descriere);
    $plus='';
    preg_match_all('#\{(.+?)\}#s', strip_tags($descriere), $biblie);

    foreach($biblie[0] AS $elem_orig) {
        $q='';
        $elem = str_replace('{', '', $elem_orig);
        $elem = str_replace('}', '', $elem);
        $exp = explode(' ', $elem);
        $nr = count($exp);
        if($nr == 3) {
            //are cifra in fata
            $book=''.$exp[0].' '.$exp[1].'';
            $exp1 = explode(':', $exp[2]);
            $capitol = $exp1[0];
            $verset = $exp1[1];

        }
        else {
            $book=''.$exp[0].'';
            $exp1 = explode(':', $exp[1]);
            $capitol = $exp1[0];
            $verset = $exp1[1];
        }
        $exp2 = explode('-', $verset);
        if(count($exp2) == 2) {
            // are mai multe versete
            $min = $exp2[0];
            $max = $exp2[1];
            $q=" AND b.verset >= $min AND b.verset <= $max";
        }
        else {
            $exp3 = explode(',', $exp2[0]);
            if(count($exp3) > 1) {
                // mai multe dar pe sarite
                $q='AND (';
                $i=0;
                foreach($exp3 AS $vers) {
                    $i++;
                    $q.="b.verset='$vers'";
                    if($i == count($exp3)) {
                    }
                    else {$q.=' OR ';
                    }
                }
                $q.=')';
            }
            else {
                $q=" AND b.verset='$exp2[0]'";
            }
        }


        $sql = mysql_query("SELECT b.continut,b.verset FROM biblie b,biblie_sursa s WHERE s.titlu='$book' AND s.id=b.book_id AND b.capitol='$capitol'$q") or trigger_error(mysql_error(), E_USER_ERROR);
        if(mysql_num_rows($sql) == 0) {
             //echo "SELECT b.continut,b.verset FROM biblie b,biblie_sursa s WHERE s.titlu='$book' AND s.id=b.book_id AND b.capitol='$capitol'$q";
        }
        else {
            $string='';
            while($row = mysql_fetch_object($sql)) {
                $string.=''.$row->verset.'. '.$row->continut.'<br/>';

            }
            $final = '<div class="verse_title">'.$elem.'</div><div class="verse_text">'.$string.'</div>';
            $final = iconv("ISO-8859-2", "UTF-8", $final);
            $rand = rand(434, 54565);
            $replace = '<span class="biblie"><a href="#b'.$rand.'" onclick="return false;">'.$elem.'</a></span>';
            $plus.='<div id="b'.$rand.'" style="display:none;">'.$final.'</div>';
            $descriere = str_replace($elem_orig, $replace, $descriere);

        }
        mysql_free_result($sql);

    }
    $descriere = ''.$plus.''.$descriere.'';
    return $descriere;
}

function diacritice( $string )
{

    $balarii = array(
    "\xC4\x82",
    "\xC4\x83",
    "\xC3\x82",
    "\xC3\xA2",
    "\xC3\x8E",
    "\xC3\xAE",
    "\xC8\x98",
    "\xC8\x99",
    "\xC8\x9A",
    "\xC8\x9B",
    "\xC5\x9E",
    "\xC5\x9F",
    "\xC5\xA2",
    "\xC5\xA3"
    );

    $clean_letters = array("A", "a", "A", "a", "I", "i", "S", "s", "T", "t", "S", "s", "T", "t");

    return str_replace($balarii, $clean_letters, $string);
}

?>