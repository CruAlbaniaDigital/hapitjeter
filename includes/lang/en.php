<?php
/**
 * En.php File
 * 
 * English language translation file for the site
 * Defines all language constants as strings in the English language
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */

// general
define(DENUMIRE, 'HapiTjeter.net');
define(DESCRIERE, 'Studim online i bibles');

define(TIP1, 'Question with open answer'); // in admin interface
define(TIP2, 'One choice answer'); // in admin interface
define(TIP3, 'Multiple choice answers'); // in admin interface

// index.php
define(L_NIVEL, "Level");
define(L_LINK_INREG, "Register link:");
define(L_LECTII, "Lessons");
define(L_REZOLVARI_UCENICI, "Correct lessons");
define(L_STRUCTURA, "Structure");
define(L_MESAGERIE, "Messages");
define(L_DECONECTARE, "Log out");
define(L_CONTUL_MEU, "My account");
define(L_TESTE, "Tests");
define(L_USERI, "users");
define(L_STATISTICI, "statistics");
define(L_BUN_VENIT, "welcome message");
define(L_LIMBA, "language");
define(L_REGISTER, "Nu ai cont?");

// content/contul_meu.php

define(L_CONT_CONTUL_MEU, 'My account');
define(L_CAMPURI_INCOMPLETE, 'Incomplete fields');
define(L_CU_SUCCES, 'Your new setting was saved');
define(L_CONT_PAROLA_GRESITA, 'The old password is incorrect');
define(L_EMAIL, 'Email');
define(L_YAHOO, 'Yahoo');
define(L_NUME, 'Your name');
define(L_MODIFICA, 'Save the changes');
define(L_CONT_PAROLA_VECHE, 'Your old password');
define(L_CONT_PAROLA_NOUA, 'Your new password');
define(L_CONT_SCHIMBA_PAROLA, 'Change the password');
define(L_CONT_STERGE_CONT, 'Delete account');
define(L_CONT_OP_IREVERSIBILA, 'Irreversible action');
define(L_SIGUR, 'Are you sure?');
define(L_DATE_PERSONALE, 'Personal info'); // add la redesign
define(L_STERGE_CONT, 'Delete account'); // add la redesign
define(L_SCHIMBA_PAROLA, 'Change password'); // add la redesign
define(L_DESCRIERE, 'Description');// add la redesign
define(L_POZA, 'Picture');// add la redesign
define(L_STERGE_POZA, 'delete picture');// add la redesign

// content/exam.php

define(L_EXAM_CLICK_EXT, 'click here to read the lesson');
define(L_EXAM_CLICK_RES, 'click here to view the questions');
define(L_EXAM_SUCCES, '<b>[mentor]</b> va revizui raspunsurile cat mai curand, dupa care vei fi anuntat ca poti parcurge urmatoarea lectie'); // // ce e intre paranteze nu se traduce
define(L_INAPOI_LECTII, 'back to lessons');
define(L_EXAM_PARCURS, 'You are to the question <b>[activ]</b> from <b>[total]</b>'); // don\t translate the code
define(L_EXAM_FINALIZARE, 'Finish the test');
define(L_EXAM_NEXT, 'Next question');

// content/inbox.php

define(L_INBOX_EXPEDITOR, 'Expeditor');
define(L_INBOX_DESTINATAR, 'Destinatar');
define(L_INBOX_PRIMITE, 'Primite');
define(L_INBOX_TRIMISE, 'Trimise');
define(L_INBOX_GOL, 'Niciun mesaj');
define(L_INBOX_SUBIECT, 'Subiect');
define(L_INBOX_DATA, 'Data');
define(L_PREV, 'Pagina precedenta');
define(L_NEXT, 'Pagina urmatoare');
define(L_DIN, 'din'); // utilizare: pagina 3 DIN 4
define(L_INBOX_INEXISTENT, 'Mesaj inexistent');
define(L_INBOX_MESAJ, 'Mesaj');
define(L_INBOX_RASPUNDE, 'Raspunde la acest mesaj');
define(L_INBOX_STERS, 'Mesaj sters cu succes');
define(L_INAPOI_MESAGERIE, 'inapoi la mesagerie');
define(L_INBOX_SUCCES, 'Mesaj trimis cu succes');
define(L_INBOX_HINT, 'CTRL + CLICK pentru selectie multipla');
define(L_INBOX_SEND, 'Trimite acum');
define(L_INBOX_STERGE, 'Sterge');

// content/intrebari.php

define(L_ASK_PENTRU, 'Intrebari pentru');
define(L_ASK_NOUA, 'intrebare noua');
define(L_ASK_INTREBARE, 'Intrebare');
define(L_ASK_RASPUNSURI, 'raspunsuri');
define(L_INAPOI_INTREBARI, 'inapoi la intrebari');
define(L_ASK_SUCCES, 'Intrebare adaugata');
define(L_ASK_LECTIE, 'Lectie');
define(L_ASK_DESCRIERE, 'Descriere');
define(L_ASK_TIP, 'Tip');
define(L_ASK_HINT, 'cel mai mic apare primul');
define(L_ASK_SUGERAT, 'Raspuns sugerat');
define(L_ADAUGA, 'Adauga');
define(L_ASK_STERS, 'Intrebare stearsa cu succes');

// content/lectii.php

define(L_LEC_PENTRU, 'Lectii pentru');
define(L_LEC_NOUA, 'Lectie noua');
define(L_LEC_TITLU, 'Titlu lectie');
define(L_INAPOI_LECTII, 'inapoi la lectii');
define(L_LEC_SUCCES, 'Lectie adaugata');
define(L_LEC_TEST, 'Test');
define(L_LEC_DESCRIERE, 'Descriere');
define(L_LEC_HINT, 'afisarea se face in ordine crescatoare');
define(L_LEC_STERS, 'Lectie stearsa cu succes');

// content/lectii_user.php

define(L_LU_REZOLVAT, 'de rezolvat');
define(L_LU_ARHIVA, 'arhiva nivel'); // utilizare: arhiva nivel #1
define(L_LU_ZERO, 'Niciun test de rezolvat inca');
define(L_LU_DENUMIRE, 'Denumire');

// content/limba.php

define(L_LIM_TITLU, 'Limba aplicatie');
define(L_LIM_ALEGE, 'Alege o limba din cele disponibile');

// content/login.php

define(L_LOG_CONECTARE, 'Conectare');
define(L_LOG_GRESIT, 'Date incorecte');
define(L_LOG_NICKNAME, 'Nickname');
define(L_LOG_PAROLA, 'Parola');
define(L_LOG_RECUPERARE, 'recuperare parola');

// content/main.php

define(L_MAIN_BUN, 'Bun venit');
define(
    L_MAIN_MESAJ, '<b>INFORMATII</b>
<p>Foloseste meniul de mai sus pentru navigare.<br><br>Esti la inceputul unei calatorii, o calatorie interactiva in studiul invataturilor de baza ale credintei crestine. Sistemul de fata este structurat pe 4 nivele, fiecare nivel cuprinzand un numar de lectii. Poti incepe parcurgerea lectiilor folosind butonul <i>Lectii</i>. Dupa parcurgerea unei lectii, mentorul tau va fi anuntat prin email si va evalua raspunsurile tale. Dupa fiecare corectare, mentorul iti va promova lectia sau ti-o va trimite inapoi spre corectare, daca raspunsurile tale nu au fost corecte.<br><br>Cand vei parcurge cu succes toate lectiile dintr-un nivel (spre exemplu primul nivel are 5 lectii), vei fi promovat la nivelul urmator. Incepand cu nivelul 2 vei primi automat un link de forma www.hapitjeter.net/numeletau . In acest fel, vei putea oferi acest link prietenilor tai carora le vei fi mentor, daca acestia se vor inscrie pentru studii. <br><br>
<b>Important</b>: foloseste cu incredere sistemul intern de mesagerie, pentru a comunica cu invitatii tai. In locurile unde vezi referinte biblice (culoare verde), daca aduci mausul deasupra referintei, textul biblic se va deschide intr-o fereastra. Verifica intotdeauna si folderul Spam/Junk, este posibil ca unele emailuri din partea sistemului sa ajunga in Spam. Fii sigur ca marchezi acele mesaje ca nefiind spam.</p>'
);

// content/mesaj.php

define(L_MESAJ_TITLU, 'Mesaj de bun venit');

// content/parola.php

define(L_PASS_TITLU, 'Recuperare parola');
define(L_PASS_TRIMITE, 'TRIMITE PAROLA PE EMAIL');
define(L_PASS_SUCCES, 'Verifica adresa de email');
define(L_PASS_EMAIL, 'Salut <b>[nume]</b>,<br/>Parola ta pe site-ul [url] este: <b>[parola]</b><br/><br/><a href="[url]?act=login">Click aici pentru a te conecta!</a>'); // nu se traduce ce e intre parantezele patrate

// content/raspunsuri.php

define(L_RAS_HELP, 'Daca nu intelegi intrebarea, trimite-mi un mesaj <a href="[link]" target="_blank">aici</a>!');
define(L_RAS_PENTRU, 'Raspunsuri pentru');
define(L_RAS_NOU, 'raspuns nou');
define(L_RAS_RASPUNS, 'Raspuns');
define(L_RAS_CORECT, 'CORECT');
define(L_INAPOI_RASPUNSURI, 'inapoi la raspunsuri');
define(L_RAS_ADAUGAT, 'Raspuns adaugat');
define(L_RAS_STERS, 'Raspuns sters cu succes');
define(L_RAS_NO, 'Niciun raspuns');
define(L_RAS_JS, 'Nu ati raspuns la intrebare');
define(L_RAS_COR, 'Ai raspuns corect la aceasta intrebare');
define(L_RAS_GRE, 'Ai raspuns gresit la aceasta intrebare');
define(L_RAS_NECOMPLETAT, 'Necompletat');
define(L_RAS_COMPLETEAZA, 'completeaza');
define(L_RAS_INCORECT, 'Incorect');
define(L_RAS_ASTEPTARE, 'In asteptare');
define(L_RAS_REVIZUIRE, 'Incorect');
define(L_RAS_COMPLETAT, 'Completat');
define(L_RAS_VEZI, 'vezi');
define(L_RAS_DECAT, 'Completat decat'); // utilizare:completat decat 45%

// content/register.php

define(L_REG_INREGISTRARE, 'Inregistrare');
define(L_REG_NU, 'Nu te poti inregistra');
define(L_REG_ERROR, 'Aveti urmatoarele erori');
define(L_REG_ERROR_NICK, 'Nickname necompletat/deja existent');
define(L_REG_ERROR_PAROLA, 'Parola de verificare nu corespunde cu parola introdusa');
define(L_REG_ERROR_EMAIL, 'Email-ul de verificare nu corespunde cu adresa de email introdusa');
define(L_REG_ERROR_NUME, 'Nume necompletat');
define(L_REG_ERROR_VARSTA, 'Varsta necompletata');
define(L_REG_ERROR_ORAS, 'Oras necompletat');
define(L_REG_ERROR_PROFESIE, 'Profesie necompletata');
define(L_REG_ERROR_TELEFON, 'Telefon necompletat');
define(L_REG_ERROR_REF, 'Nu ai completat rubrica "de unde ai aflat de noi"');
define(L_REG_ERROR_SPAM, 'Valoarea antispam incorecta"');
define(L_REG_MESAJ_EMAIl, 'Salut! O persoana s-a inregistrat folosind linkul tau.');
define(L_REG_NUME, 'Nume real');
define(L_REG_NICK, 'Nickname');
define(L_REG_PAROLA, 'Parola');
define(L_REG_PAROLA2, 'Parola din nou');
define(L_REG_EMAIL, 'Email');
define(L_REG_EMAIL2, 'Email din nou');
define(L_REG_TELEFON, 'Telefon');
define(L_REG_VARSTA, 'Varsta');
define(L_REG_ORAS, 'Oras');
define(L_REG_PROFESIE, 'Profesie');
define(L_REG_MESAJ, 'Mesaj');
define(L_REG_OPTIONAL, 'optional');
define(L_REG_REF, 'De unde ai auzit de noi');
define(L_REG_MENTOR, 'Alege un mentor'); //add la redesign
define(L_REG_MENTOR_WHY, 'Alege-ti un mentor din lista de mai sus, el iti va corecta lectiile si va tine legatura cu tine pentru ajutor specific.'); //add la redesign
define(L_REG_ERROR_PARENT, 'Nu ai selectat niciun mentor'); //add la redesign

// content/rezolvari.php

define(L_REZ_TITLU, 'Rezolvari de corectat');
define(L_REZ_NO, 'Niciun test de corectat inca');
define(L_REZ_RESPONDENT, 'Respondent');
define(L_REZ_LAST, 'Ultima modificare');
define(L_REZ_COR, 'CORECTAT');
define(L_REZ_DECOR, 'DE CORECTAT');
define(L_REZ_REZOLVARE, 'rezolvare');
define(L_INAPOI_REZOLVARI, 'inapoi la rezolvari');
define(L_REZ_PROMOVAT, 'LECTIE PROMOVATA');
define(L_REZ_NEPROMOVAT, 'Trimite inapoi spre completare');
define(L_REZ_IR, 'Intrebari si rezolvari');
define(L_REZ_UTILIZATOR, 'Utilizator');
define(L_REZ_CORECT, 'corect');
define(L_REZ_GRESIT, 'gresit');
define(L_REZ_RCORECT, 'RASPUNS CORECT');
define(L_REZ_RGRESIT, 'RASPUNS GRESIT');
define(L_REZ_PROCENT, 'Procentul utilizatorului la acest nivel');
define(L_REZ_NR, 'Lectii din'); // utilizare #3 lectii din #5
define(L_REZ_AVANS, 'Avanseaza pe');
define(L_REZ_AVANSEAZA, 'Avanseaza');
define(L_REZ_TRIMITE, 'Trimite mesaj');

// content/statistici.php

define(L_STAT_TITLU, 'Statistici');
define(L_STAT_UTILIZATORI, 'utilizatori');
define(L_STAT_SALVEAZA, 'Salveaza');
define(L_STAT_VALORI, 'Valori');
define(L_STAT_VARSTA, 'Varsta');
define(L_STAT_VARSTA_MIN, 'Varsta minima');
define(L_STAT_VARSTA_MED, 'Varsta medie');
define(L_STAT_VARSTA_MAX, 'Varsta maxima');
define(L_STAT_LOCATII, 'Locatii');
define(L_STAT_NIVELE, 'Nivele');
define(L_STAT_NIVEL, 'Nivel');

// content/teste.php

define(L_TST_TITLU, 'Teste');
define(L_TST_TITLU2, 'Titlu');
define(L_TST_NOU, 'test nou');
define(L_TST_LECTII, 'Lectii');
define(L_INAPOI_TESTE, 'inapoi la teste');
define(L_TST_OK, 'Test adaugat');
define(L_TST_OK, 'Test adaugat');
define(L_TST_DESCRIERE, 'Descriere');
define(L_TST_STERS, 'Test sters cu succes');

// content/useri.php

define(L_USER_TITLU, 'Utilizatori');
define(L_USER_CONFIRM, 'Esti sigur ca doresti stergerea acestui user? Ai transferat subalternii altcuiva?');
define(L_INAPOI_USERI, 'inapoi la useri');
define(L_USER_ERROR, 'User sau email deja existent');
define(L_USER_ERROR_MUTA, 'Nu ai selectat cine sa preia subalternii');
define(L_USER_MUTA_OK, 'Subalterni mutati cu succes');
define(L_USER_MUTA, 'Muta subalternii acestui user altcuiva');
define(L_USER_MUT, 'Muta');
define(L_USER_NO, 'Acest user nu are niciun subaltern');
define(L_USER_NR, 'Acest user are <b>[nr]</b> subalterni');
define(L_USER_PARENT, 'Utilizator parinte');
define(L_USER_LOGIN, 'Ultima logare');
define(L_USER_STERS, 'User sters cu succes');
define(L_MENTOR_RECOMANDAT, 'mentor recomandat'); // add la redesign

// includes/functions.php

define(L_INBOX_SUBJECT, 'You have a new private message');
define(
    L_INBOX_MAIL, 'Hello <b>[destinatar]</b><br><br/>You just received a new private message on the website <u>[site]</u><br/><br/>
<a href="[link]">Click here to read your message</a>'
); // mailul care se trimite la un mesaj privat nou - nu se traducece e intre paranteze

define(L_NIVEL_SUBJECT, 'You have acces to the next level of lessons');
define(
    L_NIVEL_MAIL, 'Hello <b>[nume]</b><br><br/>You are promoted and now you have acces to to the level <b>[nivel]</b> on <u>[site]</u><br/><br/>
<a href="[link]">Click here to login</a>'
); //nu se traducece ce e intre paranteze

define(L_MENTOR_SUBJECT, 'You have a new test to review');
define(L_MENTOR_MAIL, 'Hello <b>[nume]</b><br><br/>You have a test for review <u>[site]</u><br/><br/><a href="[link]">Click here to login and review the lesson</a>'); //nu se traducece ce e intre paranteze

define(L_SUB_CORECT_SUBJECT, 'Your lesson was reviewed and it is correct!');
define(L_SUB_CORECT_MAIL, 'Hello <b>[nume]</b><br><br/>Your last test on <u>[site]</u> was reviewed and it is correct. Now you can log in and and continue the tests.<br/><br/><a href="[link]">Click here to log in and start the next lesson</a>'); //nu se traducece ce e intre paranteze

define(L_SUB_GRESIT_SUBJECT, 'Revizuire lectie');
define(L_SUB_GRESIT_MAIL, 'Salut <b>[nume]</b><br><br/>Ultima lectie completata pe <u>[site]</u> a fost completata incorect. Pentru revizuire urmati linkul de mai jos. <br/><br/><a href="[link]">Click aici pentru conectare</a>'); //nu se traducece ce e intre paranteze
?>