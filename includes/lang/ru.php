<?php
/**
 * Ru.php File
 * 
 * Russian language translation file for the site
 * Defines all language constants as strings in the Russian language
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */

// general
define(DENUMIRE,'GrowOnline.in.ua');
define(DESCRIERE,'Online bible study');

define(TIP1,'Открытый вопрос'); // in admin interface
define(TIP2,'Однозначный ответ'); // in admin interface
define(TIP3,'Многозначный выбор ответа'); // in admin interface

// index.php
define(L_NIVEL,'Уровень');
define(L_LINK_INREG,'Ссылка для регистрации:');
define(L_LECTII,'Уроки');
define(L_REZOLVARI_UCENICI,'Correct lessons');
define(L_STRUCTURA,'Структура');
define(L_MESAGERIE,'Сообщения');
define(L_DECONECTARE,'Выйти');
define(L_CONTUL_MEU,'Мой аккаунт');
define(L_TESTE,'Тесты');
define(L_USERI,'Пользователи');
define(L_STATISTICI,'статистика');
define(L_BUN_VENIT,'Приветственное письмо');
define(L_LIMBA,'Язык');

// content/contul_meu.php

define(L_CONT_CONTUL_MEU,'Мой акаунт');
define(L_CAMPURI_INCOMPLETE,'Незаполненные поля');
define(L_CU_SUCCES,'Ваши новые настройки были сохранены');
define(L_CONT_PAROLA_GRESITA,'Старый пароль неверен');
define(L_EMAIL,'Имэйл');
define(L_YAHOO,'Yahoo');
define(L_NUME,'Ваше имя');
define(L_MODIFICA,'Сохранить изменения');
define(L_CONT_PAROLA_VECHE,'Ваш старый пароль');
define(L_CONT_PAROLA_NOUA,'Ваш новый пароль');
define(L_CONT_SCHIMBA_PAROLA,'Изменить пароль');
define(L_CONT_STERGE_CONT,'Удалить аккаунт');
define(L_CONT_OP_IREVERSIBILA,'Необратимое действие');
define(L_SIGUR,'Вы уверены?');

// content/exam.php

define(L_EXAM_CLICK_EXT,'нажмите здесь, чтобы прочитать урок');
define(L_EXAM_CLICK_RES,'нажмите здесь, чтобы просмотреть вопросы');
define(L_EXAM_SUCCES,'<b>[mentor]</b> просмотрит ваш ответ как можно быстрее.Когда проверка будет окончена вам сообщат по почте'); // // ce e intre paranteze nu se traduce
define(L_INAPOI_LECTII,'назад к урокам');
define(L_EXAM_PARCURS,'Вы переходите к вопросу <b>[activ]</b> от <b>[total]</b>'); // don\t translate the code
define(L_EXAM_FINALIZARE,'Закончите текст');
define(L_EXAM_NEXT,'Следующий ответ');

// content/inbox.php

define(L_INBOX_EXPEDITOR,'Отправитель');
define(L_INBOX_DESTINATAR,'К');
define(L_INBOX_PRIMITE,'Полученные');
define(L_INBOX_TRIMISE,'Отправленные');
define(L_INBOX_GOL,'Нет сообщения');
define(L_INBOX_SUBIECT,'Тема');
define(L_INBOX_DATA,'Дата');
define(L_PREV,'Предыдущая страница');
define(L_NEXT,'следующая страница');
define(L_DIN,'от'); // utilizare: pagina 3 DIN 4
define(L_INBOX_INEXISTENT,'Сообщение не найдено');
define(L_INBOX_MESAJ,'Сообщение');
define(L_INBOX_RASPUNDE,'Ответить на это сообщение');
define(L_INBOX_STERS,'Сообщение удалено');
define(L_INAPOI_MESAGERIE,'Назад во Входящие');
define(L_INBOX_SUCCES,'Сообщение отправлено успешно');
define(L_INBOX_HINT,'CTRL + CLICK для многозначного выбора');
define(L_INBOX_SEND,'ОТправить сейчас');

// content/intrebari.php

define(L_ASK_PENTRU,'Вопросы для');
define(L_ASK_NOUA,'новый вопрос');
define(L_ASK_INTREBARE,'Вопрос');
define(L_ASK_RASPUNSURI,'ответы');
define(L_INAPOI_INTREBARI,'назад к вопросам');
define(L_ASK_SUCCES,'Вопросы опубликованы');
define(L_ASK_LECTIE,'Урок');
define(L_ASK_DESCRIERE,'Содержание');
define(L_ASK_TIP,'стиль');
define(L_ASK_HINT,'наименьший -первый');
define(L_ASK_SUGERAT,'Решение');
define(L_ADAUGA,'Опубликовать');
define(L_ASK_STERS,'Вопрос успешно удален');

// content/lectii.php

define(L_LEC_PENTRU,'Уроки для');
define(L_LEC_NOUA,'Новый урок');
define(L_LEC_TITLU,'Заголовок для урока');
define(L_INAPOI_LECTII,'назад к урокам');
define(L_LEC_SUCCES,'Урок опубликован');
define(L_LEC_TEST,'Тест');
define(L_LEC_DESCRIERE,'Содержание');
define(L_LEC_HINT,'отсортировано от низшего к высшему');
define(L_LEC_STERS,'Урок удален успешно');

// content/lectii_user.php

define(L_LU_REZOLVAT,'для просмотра');
define(L_LU_ARHIVA,'архив для уровня'); // utilizare: arhiva nivel #1
define(L_LU_ZERO,'Нет теста для просмотра');
define(L_LU_DENUMIRE,'Заголовок');

// content/limba.php

define(L_LIM_TITLU,'Язык');
define(L_LIM_ALEGE,'Выбрать язык для анкеты');

// content/login.php

define(L_LOG_CONECTARE,'Связаться');
define(L_LOG_GRESIT,'Неверная информация');
define(L_LOG_NICKNAME,'Краткое имя');
define(L_LOG_PAROLA,'Пароль');
define(L_LOG_RECUPERARE,'восстановить ваш пароль');

// content/main.php

define(L_MAIN_BUN,'Добро пожаловать');
define(L_MAIN_MESAJ,'<b>INFO</b>
<p>Use the top menu to use this website.<br><br> You are at the begginign of an interactive trip in the study of the Christian faith. This system is made from 4 levels, and each level has a number of lessons. You can start to read a lesson and answer the questions if you access the button <i>Lessons from the top menu</i>. After you complete a lesson and each answer, your mentor will be notified by email to review your answers. When the review will be done, your mentor will promote your lesson (and you can access the next one) or will send it back to you to correct it, if some answers were wrong<br><br> After you will succesfully complete all lessons from one level, you will be promoted to the next level and so on. Starting with the level 2, you will automatically receive a link (www.tanitvanyvagyok.hu/YourNickname ). In this way you can invite your frind to start the study and you will be their mentor. <br><br>
<b>Very Important</b>: Use the private mail system included to comunicate with your disciples. In the places where you can notice Bible references (green color), you can mouse over the Bible reference to read the verses. Check all the time you Spam/Junk folder to be sure that no message from your disciples/mentor is there. Be sure to mark that messages as not spam</p>');

// content/mesaj.php

define(L_MESAJ_TITLU,'Приветственное письмо');

// content/parola.php

define(L_PASS_TITLU,'Восстановление пароля');
define(L_PASS_TRIMITE,'ПОСЛАТЬ ПАРОЛЬ На ИМЭЙЛ');
define(L_PASS_SUCCES,'Проверьте ваш имэйл');
define(L_PASS_EMAIL,'Приветствую Вас <b>[nume]</b>,<br/>Ваш пароль для вебсайта [url] -: <b>[parola]</b><br/><br/><a href="[url]?act=login">Нажмите здесь, чтбы войти!</a>'); // nu se traduce ce e intre parantezele patrate

// content/raspunsuri.php

define(L_RAS_HELP,'Если вы не понимаете вопрос, напшите мне <a href="[link]" target="_blank">here</a>!');
define(L_RAS_PENTRU,'Ответы на');
define(L_RAS_NOU,'новый ответ');
define(L_RAS_RASPUNS,'Ответ');
define(L_RAS_CORECT,'ВЕРНО');
define(L_INAPOI_RASPUNSURI,'назад к ответам');
define(L_RAS_ADAUGAT,'Ответ опубликован');
define(L_RAS_STERS,'Ответ удален');
define(L_RAS_NO,'Нет завершенного ответа');
define(L_RAS_JS,'Вы не ответили на этот вопрос');
define(L_RAS_COR,'Ваш ответ был правильным');
define(L_RAS_GRE,'Ваш ответ был неверным');
define(L_RAS_NECOMPLETAT,'Урок не начат');
define(L_RAS_COMPLETEAZA,'Начать урок');
define(L_RAS_INCORECT,'Неверно');
define(L_RAS_ASTEPTARE,'Ожидать просмотра');
define(L_RAS_REVIZUIRE,'редактировать ваши ответы');
define(L_RAS_COMPLETAT,'Завершено');
define(L_RAS_VEZI,'проверить');
define(L_RAS_DECAT,'Завершено'); // utilizare:completat decat 45%

// content/register.php

define(L_REG_INREGISTRARE,'Зарегистрироваться');
define(L_REG_NU,'Вам не разрешено регистрироваться.Вам нужно получить от кого-то приглашение. Проверьте свою ссылку!');
define(L_REG_ERROR,'У вас есть ошибки в регистрационной форме');
define(L_REG_ERROR_NICK,'Такое краткое имя уже есть/ Выберите другое краткое имя');
define(L_REG_ERROR_PAROLA,'Пароль неверен, используйте одинаковый пароль в обоих полях');
define(L_REG_ERROR_EMAIL,'Email is not the same / Check your email address');
define(L_REG_ERROR_NUME,'Имя не вписано / Впишите свое имя в конкретное поле');
define(L_REG_ERROR_VARSTA,'Возраст не указан');
define(L_REG_ERROR_ORAS,'Город не указан');
define(L_REG_ERROR_PROFESIE,'Работа не указана');
define(L_REG_ERROR_TELEFON,'Номер телефона не указан');
define(L_REG_ERROR_REF,'Вы не указали, откуда вы о нас узнали');
define(L_REG_ERROR_SPAM,'Antispam value is not correct/ Check the rsult"');
define(L_REG_MESAJ_EMAIl,'Привет! Кто-то зарегистрировался как ученик, использовав вашу ссылку.');
define(L_REG_NUME,'Имя');
define(L_REG_NICK,'Краткое имя');
define(L_REG_PAROLA,'Пароль');
define(L_REG_PAROLA2,'Еще раз пароль');
define(L_REG_EMAIL,'Имэйл');
define(L_REG_EMAIL2,'Еще раз имэйл');
define(L_REG_TELEFON,'Телефон');
define(L_REG_VARSTA,'Возраст');
define(L_REG_ORAS,'Город');
define(L_REG_PROFESIE,'Работа');
define(L_REG_MESAJ,'Сообщение');
define(L_REG_OPTIONAL,'по выбору');
define(L_REG_REF,'Откуда вы узнали о нас?');

// content/rezolvari.php

define(L_REZ_TITLU,'Уроки для просмотра');
define(L_REZ_NO,'НЕт уроков для просмотра');
define(L_REZ_RESPONDENT,'Ученик');
define(L_REZ_LAST,'Последнее редактирование');
define(L_REZ_COR,'Исправленное');
define(L_REZ_DECOR,'Исправить');
define(L_REZ_REZOLVARE,'Просмотреть этот урок');
define(L_INAPOI_REZOLVARI,'Назад к просмотрам');
define(L_REZ_PROMOVAT,'Урок передвинут');
define(L_REZ_NEPROMOVAT,'Послать урок обратно для исправлений');
define(L_REZ_IR,'Вопросы и ответы');
define(L_REZ_UTILIZATOR,'Пользователь');
define(L_REZ_CORECT,'верно');
define(L_REZ_GRESIT,'неверно');
define(L_REZ_RCORECT,'ПРАВИЛЬНЫЙ ОТВЕТ');
define(L_REZ_RGRESIT,'НЕПРАВИЛЬНЫЙ ОТВЕТ');
define(L_REZ_PROCENT,'Этот пользователь окончил свой уровень');
define(L_REZ_NR,'Уроки завершены от'); // utilizare #3 lectii din #5
define(L_REZ_AVANS,'Перевести пользователя');
define(L_REZ_AVANSEAZA,'Перевести');
define(L_REZ_TRIMITE,'Отправить сообщение');

// content/statistici.php

define(L_STAT_TITLU,'Статистика');
define(L_STAT_UTILIZATORI,'пользователи');
define(L_STAT_SALVEAZA,'Сохранить');
define(L_STAT_VALORI,'Ценности');
define(L_STAT_VARSTA,'Возраст');
define(L_STAT_VARSTA_MIN,'Мин. возраст');
define(L_STAT_VARSTA_MED,'Сред. возраст');
define(L_STAT_VARSTA_MAX,'Макс.возраст');
define(L_STAT_LOCATII,'Города');
define(L_STAT_NIVELE,'Уровни');
define(L_STAT_NIVEL,'Уровень');

// content/teste.php

define(L_TST_TITLU,'Тесты');
define(L_TST_TITLU2,'Заголовок');
define(L_TST_NOU,'новый тест');
define(L_TST_LECTII,'Уроки');
define(L_INAPOI_TESTE,'назад к тестам');
define(L_TST_OK,'Тест добавлен');
define(L_TST_OK,'Тест добавлен');
define(L_TST_DESCRIERE,'Описание');
define(L_TST_STERS,'Тест удален');

// content/useri.php

define(L_USER_TITLU,'Пользователи');
define(L_USER_CONFIRM,'Вы уверены, что хотите удалить этого пользователя?Вы перевели его учеников к другому пользователю?');
define(L_INAPOI_USERI,'назад к пользователям');
define(L_USER_ERROR,'Пользователь или мэйл уже использованы');
define(L_USER_ERROR_MUTA,'Вы не выбрали другого пользователя, который возьмет этих учеников');
define(L_USER_MUTA_OK,'Ученики перемещены успешно');
define(L_USER_MUTA,'Переместить учеников этого пользователя другому пользователю');
define(L_USER_MUT,'Переместить');
define(L_USER_NO,'У этого пользователя нет учеников');
define(L_USER_NR,'У этого пользователя <b>[nr]</b> учеников');
define(L_USER_PARENT,'Ментор');
define(L_USER_LOGIN,'Последний вход');
define(L_USER_STERS,'Пользователь успешно удален');

// includes/functions.php

define(L_INBOX_SUBJECT,'У вас есть новое личное сообщение');
define(L_INBOX_MAIL,'Здравствуйте <b>[destinatar]</b><br><br/>Вы только что получили новое личное сообщение <u>[site]</u><br/><br/>
<a href="[link]">Нажмите здесь, чтобы прочитать ваше сообщение</a>'); // mailul care se trimite la unesaj privat nou - nu se traducece e intre paranteze


define(L_REG_MESAJ_EMAIL,'Новый ученик только что зарегистрировался');
define(L_NIVEL_SUBJECT,'У вас есть доступ к следующему уровню уроков');
define(L_NIVEL_MAIL,'Здравствуйте <b>[nume]</b><br><br/>Вас переместили и сейчас у вас есть доступ к уровню <b>[nivel]</b> on <u>[site]</u><br/><br/>
<a href="[link]">Нажмите здесь, чтобы войти</a>'); //nu se traducece ce e intre paranteze

define(L_MENTOR_SUBJECT,'У вас есть новый тест для просмотра');
define(L_MENTOR_MAIL,'Здравствуйте <b>[nume]</b><br><br/>У вас есть тест для просмотра <u>[site]</u><br/><br/><a href="[link]">Нажмите здесь, чтобы войти и просмотреть урок</a>'); //nu se traducece ce e intre paranteze

define(L_SUB_CORECT_SUBJECT,'Ваш урок был просмотрен и он выполнен правильно!');
define(L_SUB_CORECT_MAIL,'Здравствуйте <b>[nume]</b><br><br/>Ваш последний тест по <u>[site]</u> был просмотрен и он выполнен правильно. Сейчас вы можете войти и продолжить тесты.<br/><br/><a href="[link]">Нажмите здесь, чтобы войти и начать следующий урок</a>'); //nu se traducece ce e intre paranteze

define(L_SUB_GRESIT_SUBJECT,'Урок для просмотра');
define(L_SUB_GRESIT_MAIL,'Здравтсвуйте! <b>[nume]</b><br><br/>Ваш последний урок, который вы завершили по <u>[site]</u> был завершен неверно. Чтобы вновь просмотеть его и исправить ответы, пожалуйста, нажмите здесь. <br/><br/><a href="[link]">Нажмите здесь, чтобы соединиться</a>'); //nu se traducece ce e intre paranteze
?>