<?php
/**
 * Al.php File
 * 
 * Albanian language translation file for the site
 * Defines all language constants as strings in the Albanian language
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */

// general
define(DENUMIRE, 'HapiTjeter.net');
define(DESCRIERE, 'Një vështrim interaktiv rreth Krishtit');

define(TIP1, 'Pyetje të hapura'); // in admin interface
define(TIP2, 'Pyetje me një zgjedhje'); // in admin interface
define(TIP3, 'Pyetje me shumë zgjedhje'); // in admin interface

// index.php
define(L_NIVEL, "Niveli");
define(L_LINK_INREG, "Rregjistro linkun:");
define(L_LECTII, "Mësimet");
define(L_REZOLVARI_UCENICI, "Mësimet e sakta");
define(L_STRUCTURA, "Struktura");
define(L_MESAGERIE, "Mesazhet");
define(L_DECONECTARE, "Dil nga llogaria");
define(L_CONTUL_MEU, "Llogaria ime");
define(L_TESTE, "Seritë");
define(L_USERI, "Përdoruesit");
define(L_STATISTICI, "Statistikat");
define(L_BUN_VENIT, "Përshëndetje");
define(L_LIMBA, "Gjuha");
define(L_REGISTER, "Nuk keni një llogari?");

// content/contul_meu.php

define(L_CONT_CONTUL_MEU, 'Llogaria ime');
define(L_CAMPURI_INCOMPLETE, 'Fusha jo të plota');
define(L_CU_SUCCES, 'Cilësimi yt i ri u ruajt');
define(L_CONT_PAROLA_GRESITA, 'Kodi i vjetër i hyrjes është i pasaktë');
define(L_EMAIL, 'Email');
define(L_YAHOO, 'Emrin në Facebook');
define(L_NUME, 'Emri juaj’');
define(L_MODIFICA, 'Ruani ndryshimet');
define(L_CONT_PAROLA_VECHE, 'Kodi juaj i vjetër');
define(L_CONT_PAROLA_NOUA, 'Kodi juaj i ri');
define(L_CONT_SCHIMBA_PAROLA, 'Ndryshoni kodin');
define(L_CONT_STERGE_CONT, 'Fshini llogarinë');
define(L_CONT_OP_IREVERSIBILA, 'Veprim i pakthyeshëm');
define(L_SIGUR, 'A jeni i sigurt?');
define(L_DATE_PERSONALE, 'Informacioni personal'); // add la redesign
define(L_STERGE_CONT, 'Fshini llogarinë'); // add la redesign
define(L_SCHIMBA_PAROLA, 'Ndryshoni kodin'); // add la redesign
define(L_DESCRIERE, 'Përshkrim');// add la redesign
define(L_POZA, 'Fotografi');// add la redesign
define(L_STERGE_POZA, 'Fshij fotografinë');// add la redesign

// content/exam.php

define(L_EXAM_CLICK_EXT, 'Klikoni këtu për të lexuar mësimin');
define(L_EXAM_CLICK_RES, 'Klikoni këtu për të parë pyetjet');
define(L_EXAM_SUCCES, "<b>[mentor]</b> do të shqyrojë përgjigjet tuaja dhe do t'ju lajmërojë se si të vijoni më tej"); // // ce e intre paranteze nu se traduce
define(L_INAPOI_LECTII, 'Kthehuni te mësimet');
define(L_EXAM_PARCURS, 'Jeni te pyetja <b>[activ]</b> nga <b>[total]</b>'); // don\t translate the code
define(L_EXAM_FINALIZARE, 'Përfundoni testin');
define(L_EXAM_NEXT, 'Pyetja tjetër');
define(L_JUMP_SURVEY, 'Kalo tek pyetjet');

// content/inbox.php

define(L_INBOX_EXPEDITOR, 'Dërguesi');
define(L_INBOX_DESTINATAR, 'Marrësi');
define(L_INBOX_PRIMITE, 'Marrë');
define(L_INBOX_TRIMISE, 'Dërguar');
define(L_INBOX_GOL, 'Asnjë mesazh');
define(L_INBOX_SUBIECT, 'Subjekti');
define(L_INBOX_DATA, 'Data');
define(L_PREV, 'Faqja paraardhëse');
define(L_NEXT, 'Faqja vijuese');
define(L_DIN, 'din'); // utilizare: pagina 3 DIN 4
define(L_INBOX_INEXISTENT, 'Mesazhi nuk egziston');
define(L_INBOX_MESAJ, 'Mesazh');
define(L_INBOX_RASPUNDE, 'Përgjigjuni këtij postimi');
define(L_INBOX_STERS, 'Mesazhi u fshi me sukses');
define(L_INAPOI_MESAGERIE, 'Ktheu te mesazhet');
define(L_INBOX_SUCCES, 'Mesazhi u dërgua me sukses');
define(L_INBOX_HINT, 'Kliko CTRL + CLICK për të përzgjedhur disa subjekte');
define(L_INBOX_SEND, 'Dërgo tani');
define(L_INBOX_STERGE, 'Fshij');

// content/intrebari.php

define(L_ASK_PENTRU, 'Të gjitha pyetjet');
define(L_ASK_NOUA, 'Pyetjet e reja');
define(L_ASK_INTREBARE, 'Pyetja');
define(L_ASK_RASPUNSURI, 'Përgjigjet');
define(L_INAPOI_INTREBARI, 'Kthehu te pyetjet');
define(L_ASK_SUCCES, 'Pyetjet shtesë');
define(L_ASK_LECTIE, 'Mësimi');
define(L_ASK_DESCRIERE, 'Përshkrimi');
define(L_ASK_TIP, 'Lloji');
define(L_ASK_HINT, 'E fundit shfaqet e para');
define(L_ASK_SUGERAT, 'Përgjigjet e sygjeruara');
define(L_ADAUGA, 'Shto');
define(L_ASK_STERS, 'Pyetje e fshirë me sukses');

// content/lectii.php

define(L_LEC_PENTRU, 'Mësimet');
define(L_LEC_NOUA, 'Mësim i ri');
define(L_LEC_TITLU, 'Titulli i mësimit');
define(L_INAPOI_LECTII, 'Kthehu te mësimet');
define(L_LEC_SUCCES, 'Leksioni u shtua');
define(L_LEC_TEST, 'Test');
define(L_LEC_DESCRIERE, 'Përshkruaj');
define(L_LEC_HINT, 'Dukja është në rend zbritës');
define(L_LEC_STERS, 'Mësimi u hoq me sukses');

// content/lectii_user.php

define(L_LU_REZOLVAT, 'I zgjidhur');
define(L_LU_ARHIVA, 'Arkivat e nivelit'); // utilizare: arhiva nivel #1
define(L_LU_ZERO, 'Asnjë mësim i pambaruar');
define(L_LU_DENUMIRE, 'Emri');

// content/limba.php

define(L_LIM_TITLU, 'Gjuha e programit');
define(L_LIM_ALEGE, 'Zgjidh një nga gjuhët e mundshme');

// content/login.php

define(L_LOG_CONECTARE, 'Hyr');
define(L_LOG_GRESIT, '’Të dhëna të pasakta');
define(L_LOG_NICKNAME, 'Pseudonimi');
define(L_LOG_PAROLA, ’Fjalëkalimi);
define(L_LOG_RECUPERARE, 'Harruat passwordin');

// content/main.php

define(L_MAIN_BUN, 'Mire se vini');
define(
    L_MAIN_MESAJ, '<b>Informacion</b>
<p>Jemi të gëzuar që keni vendosur ti bashkoheni këtij udhëtimi bashkë

me shqipfolës të tjerë.

Më poshtë vijon informacion që mund t’jua lehtësojë udhëtimin te hapitjeter.net

Ju lutem përdorni menunë e mësipërme për të lundruar në faqe.

Ju jeni në fillim të një udhëtimi, një studim-ndërveprues me mësimet bazë të besimit të 

krishterë.

Sistemi është i strukturuar me dy seri. Seria e parë, Bindjet Bazë, përmban katër mësime. 

Ndërsa seria e dytë, Parimet e Jetës, përmban pesë mësime. 

Ju sygjerojmë të filloni me serinë e parë. Ju mund të filloni duke ndjekin mësimet sipas 

rradhës. Pasi të keni mbaruar leksionin e parë (mësimi, dhe testi përkatës), personi që 

keni zgjedhur si kujdestar do të njoftohet me e-mail dhe do të vlerësojë përgjigjet tuaja. 

Për fusha ku keni nevojë të qartësoheni, mentori do t’ju lajmërojë që ta rishikoni mësimin 

edhe njëherë. Nëse e keni kaluar mësimin, mentori juaj mund t’ju kalojë te mësimi i dytë.

Pasi të mbaroni të dyja seritë, do të promovoheni për nivelin e ardhshëm (në 

ndërtim akoma). Nga niveli i dytë ju do të merrni automatikisht një lidhje në formë 

www.hapitjeter.net/emrijuaj. Në këtë mënyrë , ju mund të jepni këtë link miqve tuaj, në 

mënyrë që jut ë mund të kujdeseni për ta, pasi të rregjistrohen në sistem.</p>'
);

// content/mesaj.php

define(L_MESAJ_TITLU, 'Mesazhi i mirëseardhjes');

// content/parola.php

define(L_PASS_TITLU, 'Rigjej fjalekalimin');
define(L_PASS_TRIMITE, 'Dërgoni Fjalëkalimin me Email');
define(L_PASS_SUCCES, 'Kontrolloni emailin tend per te marr passwordin te ri.');
define(L_PASS_EMAIL, 'Përshëndetje <b>[nume]</b>,<br/>Fjalëkalimi juaj në faqen e internetit [url] është: <b>[parola]</b><br/><br/><a href="[url]?act=login">Klikoni këtu për tu lidhur!</a>'); // nu se traduce ce e intre parantezele patrate

// content/raspunsuri.php

define(L_RAS_HELP, 'Nëse nuk kuptoni pyetjen, më dërgoni një mesazh <a href="[link]" target="_blank">aici</a>!');
define(L_RAS_PENTRU, 'Përgjigjuni...');
define(L_RAS_NOU, 'Përgjigjet e reja');
define(L_RAS_RASPUNS, 'Përgjigje');
define(L_RAS_CORECT, 'E saktë');
define(L_INAPOI_RASPUNSURI, 'Kthehuni te përgjigjet');
define(L_RAS_ADAUGAT, 'Përgjigja e shtuar');
define(L_RAS_STERS, 'Përgjigja e fshirë me sukses');
define(L_RAS_NO, 'Asnjë Përgjigje');
define(L_RAS_JS, 'Nuk i je përgjigjur pyetjes');
define(L_RAS_COR, 'I je përgjigjur saktë kësaj pyetje');
define(L_RAS_GRE, 'I je përgjigjur gabim kësaj pyetje');
define(L_RAS_NECOMPLETAT, 'E paplotësuar');
define(L_RAS_COMPLETEAZA, 'Plotëso');
define(L_RAS_INCORECT, 'E pasakatë');
define(L_RAS_ASTEPTARE, 'Në pritje');
define(L_RAS_REVIZUIRE, 'E pasaktë');
define(L_RAS_COMPLETAT, 'E Plotësar');
define(L_RAS_VEZI, 'Shih');
define(L_RAS_DECAT, 'Plotëso sepse'); // utilizare:completat decat 45%

// content/register.php

define(L_REG_INREGISTRARE, "Rregjistrohu");
define(L_REG_NU, 'Keni gabimë ne linkun rregjistrimet');
define(L_REG_ERROR, 'Keni gabimet e mëposhtme');
define(L_REG_ERROR_NICK, 'Pseudonimi bosh/tashmë egziston');
define(L_REG_ERROR_PAROLA, 'Fjalëkalimi i verifikuar nuk përputhet me fjalëkalimin e përdorur');
define(L_REG_ERROR_EMAIL, 'Emaili verifikues nuk përputhet me adresën hyrëse të Emailit');
define(L_REG_ERROR_NUME, 'Emri bosh');
define(L_REG_ERROR_VARSTA, 'Bosh');
define(L_REG_ERROR_ORAS, 'Hapësirë boshe');
define(L_REG_ERROR_PROFESIE, 'Hapësira për profesionin bosh');
define(L_REG_ERROR_TELEFON, 'Hapësira për të plotësuar numrin e tel bosh');
define(L_REG_ERROR_REF, 'Nuk keni plotësuar seksionin "de unde ai aflat de noi"');
define(L_REG_ERROR_SPAM, 'Vlera anti-spam e pasaktë"');
define(L_REG_MESAJ_EMAIL, 'Një person është rregjistruar duke përdorur lidhjen tuaj.');
define(L_REG_NUME, 'Emri juaj real');
define(L_REG_NICK, 'Pseudonimi');
define(L_REG_PAROLA, 'Fjalëkalimi');
define(L_REG_PAROLA2, 'Fjalëkalimi i përsëritur');
define(L_REG_EMAIL, 'Email');
define(L_REG_EMAIL2, 'Emaili i përsëritur');
define(L_REG_TELEFON, 'Telefoni');
define(L_REG_VARSTA, 'Mosha');
define(L_REG_ORAS, 'Qyteti');
define(L_REG_PROFESIE, 'Profesioni');
define(L_REG_MESAJ, 'Mesazhi');
define(L_REG_OPTIONAL, 'Opsional');
define(L_REG_REF, 'Si dëgjuat për ne');
define(L_REG_MENTOR, 'Zgjidhni një mbikqyrës/kujdestar'); //add la redesign
define(L_REG_MENTOR_WHY, "Zgjidhni një mbikqyrës/kujdestar nga lista e mësipërme. Ai do t'ju korrigjojë leksionet dhe do të jetë në kontakt me ju për ndihmë të mëtejshme."); //add la redesign
define(L_REG_ERROR_PARENT, 'Nuk keni zgjedhur asnjë mentor'); //add la redesign
define(L_MENTOR, 'Mbikqyrës/Kujdestar');

// content/rezolvari.php

define(L_REZ_TITLU, 'Korrigjimi i problemeve');
define(L_REZ_NO, 'Akoma nuk është korrigjuar asnjë test');
define(L_REZ_RESPONDENT, 'Personi përgjegjës për tu përgjigjur');
define(L_REZ_LAST, 'Ultima modificare');
define(L_REZ_COR, 'I korrigjuar');
define(L_REZ_DECOR, 'Korrigjuar');
define(L_REZ_REZOLVARE, 'Zgjidhja');
define(L_INAPOI_REZOLVARI, 'Kthehu te zgjidhjet e problemit');
define(L_REZ_PROMOVAT, 'Mësimi i promovuar');
define(L_REZ_NEPROMOVAT, 'Dërgo mbrapsht për tu riplotësuar');
define(L_REZ_IR, 'Pyetje dhe çështje');
define(L_REZ_UTILIZATOR, 'Përdorues');
define(L_REZ_CORECT, 'Saktë');
define(L_REZ_GRESIT, 'Gabim');
define(L_REZ_RCORECT, 'Përgjigjet e sakta');
define(L_REZ_RGRESIT, 'Përgjigjet e gabuara');
define(L_REZ_PROCENT, 'Përqindja që ke arritur në këtë nivel');
define(L_REZ_NR, 'Mësimet nga'); // utilizare #3 lectii din #5
define(L_REZ_AVANS, 'Shko te mësimi');
define(L_REZ_AVANSEAZA, 'Përparimi');
define(L_REZ_TRIMITE, 'Dërgo mesazhin');

// content/statistici.php

define(L_STAT_TITLU, 'Statistikat');
define(L_STAT_UTILIZATORI, 'Përdoruesit');
define(L_STAT_SALVEAZA, 'Ruaj');
define(L_STAT_VALORI, 'Vlerat');
define(L_STAT_VARSTA, 'Mosha');
define(L_STAT_VARSTA_MIN, 'Mosha minimale');
define(L_STAT_VARSTA_MED, 'Mosha mesatare');
define(L_STAT_VARSTA_MAX, 'Mosha maksimale');
define(L_STAT_LOCATII, 'Vendi');
define(L_STAT_NIVELE, 'Nivelet');
define(L_STAT_NIVEL, 'Niveli');

// content/teste.php

define(L_TST_TITLU, 'Testet');
define(L_TST_TITLU2, 'Titulli');
define(L_TST_NOU, 'Test i ri');
define(L_TST_LECTII, 'Mësimet');
define(L_INAPOI_TESTE, 'Kthehuni te testet');
define(L_TST_OK, 'Test i shtuar');
define(L_TST_OK, 'Test i shtuar');
define(L_TST_DESCRIERE, 'Përshkrimi');
define(L_TST_STERS, 'Testi i fshirë me sukses');

// content/useri.php

define(L_USER_TITLU, 'Përdoruesit');
define(L_USER_CONFIRM, 'A jeni i sigurt që doni ta fshini këtë përdorues? A mund ta transferoni diku tjetër?');
define(L_INAPOI_USERI, 'Kthehuni te përdoruesit');
define(L_USER_ERROR, 'Adresa e emailit të përdoruesit nuk egziston');
define(L_USER_ERROR_MUTA, 'Ju keni zgjedhur të merrni përsipër vartësit');
define(L_USER_MUTA_OK, 'Vartësi u lëviz me sukses');
define(L_USER_MUTA, 'Lëvizni këtë përdorues nga vartësi');
define(L_USER_MUT, 'Lëviz');
define(L_USER_NO, 'Ky përdorues nuk ka vartës');
define(L_USER_NR, 'Ky përdorues ka <b>[nr]</b> vartës');
define(L_USER_PARENT, 'Emri i përdoruesit kujdestar');
define(L_USER_LOGIN, 'Hyrja e fundit në sistem');
define(L_USER_STERS, 'Përdorues i fshirë me sukses');
define(L_MENTOR_RECOMANDAT, 'Mbikqyrës i sygjeruar'); // add la redesign

// includes/functions.php

define(L_INBOX_SUBJECT, 'Ju ka ardhur nje mesazh privat');
define(
    L_INBOX_MAIL, 'Përshëndetje <b>[destinatar]</b><br><br/>Sapo ju ka ardhur një mesazh privat në faqen e internetit <u>[site]</u><br/><br/>
<a href="[link]">Klikoni këtu për të lexuar mesazhin tuaj</a>'
); // mailul care se trimite la un mesaj privat nou - nu se traducece e intre paranteze

define(L_NIVEL_SUBJECT, 'Tashme keni hyrje ne nivelin tjeter te mesimeve');
define(
    L_NIVEL_MAIL, 'Përshëndetje <b>[nume]</b><br><br/>Jeni promovuar dhe tani keni hyrje në nivelin tjetër <b>[nivel]</b> në <u>[site]</u><br/><br/>
<a href="[link]">Klikoni këtu për të hyrë</a>'
); //nu se traducece ce e intre paranteze

define(L_MENTOR_SUBJECT, 'Ju keni nje test te ri per te shikuar');
define(L_MENTOR_MAIL, 'Përshëndetje <b>[nume]</b><br><br/>Ju keni një test për të vlerësuar <u>[site]</u><br/><br/><a href="[link]">Klikoni këtu për të hyrë në llogari dhe të shikoni mësimin</a>'); //nu se traducece ce e intre paranteze

define(L_SUB_CORECT_SUBJECT, 'Mesimi juaj u shikua dhe eshte i sakte!');
define(L_SUB_CORECT_MAIL, 'Përshëndetje <b>[nume]</b><br><br/>Testi juaj i fundit në <u>[site]</u> u shikua dhe është i saktë. Tani ju mund të hyni në llogari dhe të vazhdoni me testet.<br/><br/><a href="[link]">Klikoni këtu për të hyrë në llogari dhe filloni mësimin tjetër</a>'); //nu se traducece ce e intre paranteze

define(L_SUB_GRESIT_SUBJECT, 'Rishikoni mesimin ');
define(L_SUB_GRESIT_MAIL, 'Përshëndetje <b>[nume]</b><br><br/>Mësimi juaj i fundit i dërguar në <u>[site]</u> është plotësuar gabim. Për ta rishikuar edhe njëherë mësimin, klikoni në linkun më poshtë. <br/><br/><a href="[link]">Klikoni këtu për të hyrë te mësimi</a>'); //nu se traducece ce e intre paranteze
?>