<?php
/**
 * Excel.php File Doc Comment
 * 
 * Page building a table of user information for export
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */

require "includes/functions.php";
conectare();

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename="useri.xls"');

$export='';
$sql = mysql_query("SELECT * FROM useri ORDER BY id ASC") or trigger_error(mysql_error(), E_USER_ERROR);
if(mysql_num_rows($sql) == 0) {
    $export.='Nu sunt informatii!';
}
else {
    $export.='<table>
		<tr>
			<td>User</td>
			<td>Nivel</td>
			<td>Nume</td>
			<td>Varsta</td>
			<td>Oras</td>
			<td>Profesie</td>
			<td>Email</td>
			<td>Telefon</td>
		</tr>';
    while($row = mysql_fetch_object($sql)) {
        $export.='<tr>
				<td>'.$row->user.'</td>
				<td>'.$row->nivel.'</td>
				<td>'.$row->nume.'</td>
				<td>'.$row->varsta.'</td>
				<td>'.$row->oras.'</td>
				<td>'.$row->profesie.'</td>
				<td>'.$row->email.'</td>
				<td>'.$row->telefon.'</td>
			</tr>';
    }
    $export.='</table>';
}
mysql_free_result($sql);
echo $export;
?>