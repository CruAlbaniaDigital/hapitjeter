<?php
/**
 * Setup.php File Doc Comment
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */
error_reporting(E_ERROR);

$errors = array();
if (isset($_POST['action'])) {
    if ($_POST['action'] == 'configfile') {

        $errors = validateConfigForm();
        if (empty($errors)) {
             $ini_file = makeConfigIni($_POST);
    
             header('Content-Type: text/plain');
             header('Content-Disposition: attachment;filename=config.ini');
             header('Content-Length: ' . strlen($ini_file));
             header('Connection: close');

             echo $ini_file;
             exit;
        }
    }
}

$AcceptTypes = Array ();
// Accept header is case insensitive, and whitespace isn’t important
$accept = strtolower(str_replace(' ', '', $_SERVER['HTTP_ACCEPT']));
// divide it into parts in the place of a ","
$accept = explode(',', $accept);
if (in_array('application/json', $accept)) {
    $configExists = include "config.php";
    $ret = array(
        'php_version' => PHP_VERSION,
    );

    if (file_exists('./VERSION')) {
        $ret['version']=trim(file_get_contents('./VERSION'));
    } else {
        $ret['version']='';
    }

    if (!$configExists) {
        http_response_code(500);
    } else {
        $ret['url.canonical'] = URL_ABSOLUT;
        $ret['url.relative'] = URL_RELATIVE;
        $ret['mysql.host'] = MYSQL_HOST;
        $ret['mysql.user'] = MYSQL_USER;
        $ret['mysql.schema'] = MYSQL_NAME;
        $ret['mysql.pass'] = defined('MYSQL_PASS') ? '********' : 'no password';
        $ret['site.lang'] = SITE_LANG;

        if (!mysql_connect(MYSQL_HOST, MYSQL_USER, MYSQL_PASS)) {
            http_response_code(500);
            $ret['error'] = mysql_error();
        } else if (!mysql_select_db(MYSQL_NAME)) {
            http_response_code(500);
            $ret['error'] = mysql_error();
        } else {
            $ret['mysql'] = mysql_get_server_info();
        }
    }
    
    header('Content-Type: application/json');
    echo json_encode($ret);
    exit;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>hapitjeter setup</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <style type="text/css">
        .error {
            color: red;
        }

        .box {
            border: 2px solid black;
        }

        table {
            border: 1px solid black;
        }

        td {
            border-bottom: 1px solid black;
            padding: 2px;
        }
    </style>
  </head>
  <body>
    <h1>Hapitjeter setup page</h1>
    <div class='test'>
      <h3> Hapitjeter version: </h3>
      <div class='test-body'>
        <?php 
            if (file_exists('./VERSION')) {
                $hapitjeter_version = file_get_contents('./VERSION');
            } else {
                $hapitjeter_version = `git describe --tags`;
            }

            if (!empty($hapitjeter_version)) {
                list($version, $revision, $git_hash) = explode('-', $hapitjeter_version);
                if ($revision == "0") {
                    $hapitjeter_version = $version;
                }
                $git_hash = trim($git_hash, 'g');
                if (empty($git_hash)) {
                    echo "<a href='https://gitlab.com/CruAlbaniaDigital/hapitjeter/tree/$version'>$hapitjeter_version</a>";
                } else {
                    echo "<a href='https://gitlab.com/CruAlbaniaDigital/hapitjeter/tree/$git_hash'>$hapitjeter_version</a>";
                }
            } else {
                echo "<a href='https://gitlab.com/CruAlbaniaDigital/hapitjeter'>UNKNOWN</a>";
            }

        ?>
      </div>
    </div>

    <div class='test'>
      <h3> PHP version: </h3>
      <div class='test-body'>
      <pre><?php echo PHP_VERSION ?></pre>
        <?php if (version_compare(PHP_VERSION, '5.4.45', '<')) { 
        
        ?><div class='error'>Please upgrade to at least PHP version 5.4.45</div><?php

        }

if (version_compare(PHP_VERSION, '7.0.0', '>=')) {?>
        
        <div class='error'>Please use PHP5, PHP7 is not yet supported and we're not sure it will work.</div>
        
        <?php
}
        ?>

      </div>
    </div>

    <div class='test'>
        <?php
        $configExists = include "config.php";
        if (!$configExists) {
            ?>
            <h3>No config file exists.</h3>
            Fill in the form to create a config.ini file, then place it at the root of the application, next to 'config_sample.ini'
            <form method="POST" action="<?PHP echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" accept-charset="UTF-8">
              <input type='hidden' name='action' value='configfile' />
              <p>
                <label>Canonical URL:</label>
                <input type='text' name='canonical' placeholder='<?php echo getFullUrl()?>' value='<?PHP 
                if (isset($_POST['canonical'])) { 
                    echo htmlspecialchars($_POST['canonical']);
                } 
                ?>' />
                <?php echo errorFor('canonical', $errors) ?>
              </p>
              <p>
                <label>relative path:</label>
                <input type='text' name='relative' placeholder='<?php echo dirname($_SERVER['REQUEST_URI'])?>' value='<?PHP
                if (isset($_POST['relative'])) { 
                    echo htmlspecialchars($_POST['relative']);
                }
                ?>'></input>
                <?php echo errorFor('relative', $errors) ?>
              </p>
              <p>
                <label>Mysql server hostname:</label>
                <input type='text' name='mysql_host' placeholder='localhost' value='<?PHP
                if (isset($_POST['mysql_host'])) { 
                    echo htmlspecialchars($_POST['mysql_host']);
                }
                ?>'></input>
                <?php echo errorFor('mysql_host', $errors) ?>
              </p>
              <p>
                <label>Mysql username:</label>
                <input type='text' name='mysql_user' placeholder='hapitjeter' value='<?PHP
                if (isset($_POST['mysql_user'])) { 
                    echo htmlspecialchars($_POST['mysql_user']);
                } ?>'></input>
                <?php echo errorFor('mysql_user', $errors) ?>
              </p>

              <p>
                <label>Mysql password:</label>
                <input type='password' name='mysql_pass'></input><span class='error'>
                <?php echo errorFor('mysql_pass', $errors); ?>
                </p>

                <p>
                <label>Mysql schema name:</label>
                <input type='text' name='mysql_db' placeholder='hapitjeter' value='<?PHP
                if (isset($_POST['mysql_db'])) { 
                    echo htmlspecialchars($_POST['mysql_db']);
                }
                ?>'></input>
                <?php echo errorFor('mysql_db', $errors) ?>
                </p>

                <?php echo errorFor('mysql', $errors) ?>

                <p>
                <label>Site language:</label>
                <select name='lang'>
                <?php 
                $langOptions = getLangOptions();
                foreach ( $langOptions as $lang ) {
                    $selected = '';
                        if (isset($_POST['lang']) && $lang == $_POST['lang']) { 
                        $selected = 'selected';
                    }

                    echo "<option value=\"$lang\" $selected>$lang</option>";
                }
                ?>
                </select>
                <?php echo errorFor('lang', $errors) ?>

                </p>

                  <button type=submit>Download my config.ini file</button>
                </form>

                <?php

        }
      
        if ($configExists) {
            ?>
            <h3>config values:</h3>
            <table>
            <tr>
            <td>url.canonical</td><td><?php echo URL_ABSOLUT?></td>
            </tr>
            <tr>
            <td>url.relative</td><td><?php echo URL_RELATIVE?></td>
            </tr>
            <tr>
            <td>mysql.host</td><td><?php echo MYSQL_HOST?></td>
            </tr>
            <tr>
            <td>mysql.user</td><td><?php echo MYSQL_USER?></td>
            </tr>
            <tr>
            <td>mysql.schema</td><td><?php echo MYSQL_NAME?></td>
            </tr>
            <tr>
            <td>mysql.pass</td><td><?php echo defined('MYSQL_PASS') ? '********' : 'no password'  ?></td>
            </tr>
            <tr>
            <td>site.lang</td><td><?php echo SITE_LANG  ?></td>
            </tr>
          </table>

          <h3>MySQL connection info</h3>
        <?php

          
          if (!mysql_connect(MYSQL_HOST, MYSQL_USER, MYSQL_PASS)) {
              echo "<span class='error'>" . mysql_error() . "</span>";
          } else if (!mysql_select_db(MYSQL_NAME)) {
              echo "<span class='error'>" . mysql_error() . "</span>";
          } else {
              echo mysql_get_server_info();
          }

        } // if $configExists

        ?>
    </div>

  </body>

</html>

<?php

/**
 * Gets the possible languages in the language folder
 * 
 * @return array an array of all the language options in the includes/lang folder
 */
function getLangOptions() 
{
    $langdir = dirname(__FILE__) . '/includes/lang/';
    $files = array_diff(scandir($langdir), array('.', '..'));

    $strip_suffix = function ($file) {
        return pathinfo($file, PATHINFO_FILENAME);
    };

    return array_map($strip_suffix, $files);
}

/**
 * Gets the server's full URL based on HTTP headers
 * 
 * @return url of the server according to the client
 */
function getFullUrl() 
{
    return (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]/";
}

/**
 * Gets a span describing an error out of the errors array
 * 
 * @param string $name the name of the input field
 * @param array  $errs the array of errors
 * 
 * @return a string including an HTML span describing the error if it exists
 */
function errorFor($name, $errs) 
{
    if (!isset($errs) || !isset($errs[$name])) {
        return '';
    }
    return "<span class='error'>{$errs[$name]}</span>";
}

/**
 * Validates the POST data from the config form.
 * 
 * @return array string => string input names to error messages
 */
function validateConfigForm() 
{
    $errs = array();
    
    if (!empty($_POST['canonical'])) {
        if (substr($_POST['canonical'], 0, 7) != "http://" && substr($_POST['canonical'], 0, 8) != "https://") {
            $errs['canonical'] = 'The canonical URL should start with "http://" or "https://"';
        }
    }

    if (empty($_POST['mysql_user'])) {
        $errs['mysql_user'] = 'Mysql username cannot be empty';
    }

    if (empty($_POST['mysql_host'])) {
        $errs['mysql_host'] = 'Mysql server hostname cannot be empty';
    }


    if (empty($_POST['mysql_db'])) {
        $errs['mysql_db'] = 'database name cannot be empty';
    }

    if (empty($errs)) {
        if (!mysql_connect($_POST['mysql_host'], $_POST['mysql_user'], $_POST['mysql_pass']) ) {
            $errs['mysql'] = mysql_error();
        } else if (!mysql_select_db($_POST['mysql_db'])) {
            $errs['mysql'] = mysql_error();
        }
    }

    if (empty($_POST['lang'])) {
        $errs['lang'] = 'you must select a language';
    }

    if (empty($_POST['lang'])) {
        $errs['lang'] = 'you must select a language';
    } else if (!in_array($_POST['lang'], getLangOptions())) {
        $errs['lang'] = 'you did not select a valid language';
    }



    return $errs;
}

/**
 * Generates a config.ini file based on POST data from the config form
 * 
 * @param array $data POST data
 * 
 * @return string the config.ini file to be written out to the client
 */
function makeConfigIni($data) 
{

    if (empty($data['canonical'])) {
        $data['canonical'] = getFullUrl();
    }
    $data['canonical'] = rtrim($data['canonical'], '/') . '/'; // ensure a trailing slash

    if (empty($data['relative'])) {
        $data['relative'] = '/';
    } else if ($data['relative'] != '/') {
        $data['relative'] = '/' . trim($data['relative'], '/') . '/'; // ensure there is a slash on both ends
    }


    $template = "[host]
 ; The canonical root address of your application, including relative path
url.canonical = '${data['canonical']}'
url.relative_path = '${data['relative']}'     ; The relative path of the root of your application

[mysql]
mysql.host   = '${data['mysql_host']}' ; the hostname where the mysql server is located, usually localhost
mysql.user   = '${data['mysql_user']}' ; the username in the mysql server that the application uses to connect
mysql.pass   = '${data['mysql_pass']}' ; the password of the user
mysql.schema = '${data['mysql_db']}' ; the name of the schema that hapitjeter will use

[site]
site.lang  = '${data['lang']}'  ; the language of the site - translations in includes/lang/";

    return $template;
}
?>