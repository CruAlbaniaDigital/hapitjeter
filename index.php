<?php
/**
 * Index.php File Doc Comment
 * 
 * The entry point for Hapitjeter
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */

require_once "logging.php";
require_once "config.php";
require_once "includes/functions.php";
conectare();
ob_start();
//echo ($limba);
require_once "includes/lang/" . SITE_LANG . ".php";
// check if user is mentor
$userismentor = getOneValue("useri", "id", $_SESSION['log_id'], "afis_mentor");
if (isset($_GET['act'])) {
        $cale = 'content/'.$_GET['act'].'.php';
    if (file_exists($cale)) { 
        include $cale; 
    } else { 
        include "content/error.php"; 
    }
} else {  
    include "content/main.php";  
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="<?php echo $url_absolut; ?>"/>
<title><?php echo DENUMIRE; ?></title>
<link rel="SHORTCUT ICON" href="images/favicon.ico"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style<?php if (is_logged()) { ?>-old<?php } ?>.css" media="screen" />
<link href="http://fonts.googleapis.com/css?family=Dancing+Script:regular" rel="stylesheet" type="text/css" >
<link rel="stylesheet" type="text/css" href="css/tooltip.css" media="screen" />
<?php if ((is_logged()) && ($_GET["new_nivel"]) != "") { ?>
<link rel="stylesheet" type="text/css" href="css/newlevel.css" media="screen" />
<?php } ?>
<?php if (!is_logged()) { ?>
<link rel="stylesheet" type="text/css" href="css/series.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/videobackground.css" media="screen" />
<link href='https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700italic' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/videobackground.js"></script>
<?php } ?>
<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400<?php if (is_logged()) { ?>,400italic,700,700italic<?php 
} ?> ?>' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/tooltip.js"></script>
<script type="text/javascript" src="js/library.js"></script>
<link rel="shortcut icon" href="images/favicon.ico" />
<?php if (!is_logged()) { ?>
<script src="js/jquery.min.js"></script>
<script src="js/jquery.magnific-popup.js"></script>
<link href="css/magnific-popup.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
/**
 * Simple fade transition,
 */
.mfp-fade.mfp-bg {
opacity: 0;
-webkit-transition: all 0.15s ease-out;
-moz-transition: all 0.15s ease-out;
transition: all 0.15s ease-out;
}
.mfp-fade.mfp-bg.mfp-ready {
opacity: 0.8;
}
.mfp-fade.mfp-bg.mfp-removing {
opacity: 0;
}
.mfp-fade.mfp-wrap .mfp-content {
opacity: 0;
-webkit-transition: all 0.15s ease-out;
-moz-transition: all 0.15s ease-out;
transition: all 0.15s ease-out;
}
.mfp-fade.mfp-wrap.mfp-ready .mfp-content {
opacity: 1;
}
.mfp-fade.mfp-wrap.mfp-removing .mfp-content {
opacity: 0;
}
-->
</style>
<?php } ?>
</head>
<body>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '607693242728131',
      xfbml      : true,
      version    : 'v2.5'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

<?php if (!is_logged()) { ?>
<div class="header-unit">
  <div id="video-container">
     <video loop muted autoplay poster="images/vine960c.jpg" class="fade-in-video fillWidth">
       <source src="images/vine960c.mp4" type="video/mp4">
     </video>

    <div class="video-content">
        <div id="logo"><a href="<?php echo $url_relative?>"><img src="images/HapiTjeter-LogoTransOrange.png" alt=""></a></div>
<?php } else { ?>
<div id="all">

        <div id="header">
            <div id="logo"><a href="<?php echo $url_relative?>"><img style="width:220px;" src="images/HapiTjeter-LogoOrangeHeader.png" alt=""></a></div>        
<?php } ?>
       
        <div id="menu">
        <?php
        if(is_logged()) {
            echo '<table cellspacing="2" align="right">';
            if(nivel($_SESSION['log_id']) == 5) {
            }
            else {
                echo'<td><span class="menu"><a href="?act=lectii_user">'.L_LECTII.'</a></span></td>';
            }
                
            if ($userismentor) {
                echo'<td><span class="menu"><a href="?act=rezolvari">'.L_REZOLVARI_UCENICI.' '.rezolvari_noi().'</a></span></td><td><span class="menu"><a href="?act=structura">'.L_STRUCTURA.'</a></span></td>';
            }
                
                echo'<td><span class="menu"><a href="?act=inbox">'.L_MESAGERIE.' '.mesaje_noi().'</a></span></td>
                        <td><span class="menu"><a href="?act=contul_meu">'.L_CONTUL_MEU.'</a></span></td>
                        <td><span class="logout"><a href="?act=deconectare"><img src="images/exit.png" height="34" /></a></span></td></tr></table>';
        }
        else {
        }
        ?>
          <br class="clear"/>
                <div id="ref">
        <?php
          echo '<table cellspacing="2" align="right">';
                          echo '<div
  class="fb-like" style="margin-top:10px; float: left;"
  data-share="true"
  data-width="150"
  data-show-faces="false">
</div>';
        if(is_logged()) {
                echo '<tr><td><div class="nivel" style="margin-right:10px;"><div class="nivel_text">NIVEL<br/><b>'.nivel($_SESSION['log_id']).'</b></div></div></td>';
            if ($userismentor) {
                echo '<td><a style="margin-right:4px; font-size:16px; font-weight:bold;" target="_blank" href="https://dashboard.tawk.to/login">CHAT</a></td>';
                echo '<td>'.L_LINK_INREG.'<span id="ref_link1">SERIA 1</span><span id="ref_linktext1">'.$url_absolut.''.getOneValue("useri", "id", $_SESSION['log_id'], "user").'?seria=1</span><span id="ref_link2">SERIA 2</span><span id="ref_linktext2">'.$url_absolut.''.getOneValue("useri", "id", $_SESSION['log_id'], "user").'?seria=2</span></td>';
            }
            echo '</tr>';
        }
        ?>
                </table>
        </div>
      </div>        
    </div>
    <br class="clear"/>
        
                <div id="static_menu">
                        <ul>
                        <?php
                        $admin = getOneValue("useri", "id", $_SESSION['log_id'], "parent_id");
                        if($admin == 0 AND is_logged()) { //admin 
                        ?>
                                
                                <li><a href="?act=limba"><?php echo L_LIMBA?></a></li>
                                <li><a href="?act=mesaj"><?php echo L_BUN_VENIT?></a></li>
                                <li><a href="?act=statistici"><?php echo L_STATISTICI?></a></li>
                                <li><a href="?act=useri"><?php echo L_USERI?></a></li>
                                <li><a href="?act=teste"><?php echo L_TESTE?></a></li>
                        <?php 
                        }
                        else {
                        ?>
                            <li><a href="<?php echo $url_relative?>">Kryefaqja</a></li>
                            <li><a href="<?php echo $url_relative?>rreth_nesh.html">Rreth nesh</a></li>
                            <li><a href="<?php echo $url_relative?>lente">Lente</a></li>
                            <li><a href="<?php echo $url_relative?>kontakt.html">Kontakt</a></li>
                <?php
                if ($userismentor) {
                ?>
                <li><a href="?act=mesaj"><?php echo L_BUN_VENIT?></a></li>
                <?php
                }
                        } // end if admin
                        ?>
                                
                        </ul>
                </div>
                <br class="clear"/>
<?php if (!is_logged()) { ?>
  </div><!-- end video-container -->
</div><!-- end .header-unit -->
<?php } ?>      
             
                <?php if ((is_logged()) && ($_GET["new_nivel"]) != "") { ?>
<div id="popup1" class="overlay">
    <div class="popup">
        <h2>Urime!</h2>
        <a class="close" href="#popup1">&times;</a>
        <div class="content">
            Urime, ju keni diplomuar n� nivelin e <?php echo $_GET["new_nivel"]; ?>!
        </div>
    </div>
</div>
                <?php } ?>
                
                <?php echo $content; ?>
                                
        <div id="footer">
                Copyright &copy; <?php echo date('Y'); ?> <b>HapiTjeter.net</b>.
        </div>
        <?php
        if(is_logged()) {
                // hide chat widget if user is mentor or if is the main admin
            if (($userismentor == 0) || ($_SESSION['log_id'] == 1)) {
                      // get current username and email
                    $username = getOneValue("useri", "id", $_SESSION['log_id'], "nume");
                    $useremail = getOneValue("useri", "id", $_SESSION['log_id'], "email");
                    $usermentor = getOneValue("useri", "id", getOneValue("useri", "id", $_SESSION["log_id"], "parent_id"), "nume");
                    ?>
                <!--Start of Tawk.to Script-->
                <script type="text/javascript">
                var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
                (function(){
                var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                s1.async=true;
                s1.src='https://embed.tawk.to/5641d3814e95c21711703490/default';
                s1.charset='UTF-8';
                s1.setAttribute('crossorigin','*');
                s0.parentNode.insertBefore(s1,s0);
                })();

                Tawk_API.visitor = {
                        name  : '<?php echo $username . " [M: ".$usermentor."]"; ?>',
                        email : '<?php echo $useremail; ?>',
                        hash  : '<?php echo hash_hmac("sha256", $useremail, "f9f464ac357290bda0937518b180344d3e342776"); ?>'
                };
                </script>
                <!--End of Tawk.to Script-->
                <?php
            }
        }
?>
<?php if (!is_logged()) { 
} else { ?></div><?php 
} ?>
</body>
</html>
<?php ob_end_flush(); ?>
