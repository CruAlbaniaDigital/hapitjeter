var quizArray = [
'49~56~Cine a scris cartea Genezei?~Avraam~Moise~Adam',
'30~54~Primul imparat al Israelului a fost...~David~Moise~Saul',
'49~19~Toate Evangheliile au fost scrise de apostoli~Adevarat~Fals',
'57~61~Cine a fost strabunica lui David?~Sara~Naomi~Rut',
'32~43~La inceputurile crestinismului, invierea lui Iisus Hristos se sarbatorea...~In fiecare sambata~De Paste~In fiecare duminica',
'31~30~Ce apostol s-a lepadat de Hristos?~Toma~Petru~Ioan',
'37~20~Cea mai mare parte a Noului Testament a fost scrisa de:~Pavel~Luca~Ioan',
'49~32~Care din urmatorii apostoli nu a scris nicio epistola a Noului Testament?~Iuda~Toma~Petru',
'24~67~Apostolul iubirii este numit~Ioan~Pavel~Marcu',
'105~50~In Biblie apare sintagma "crede si nu cerceta"~Adevarat~Fals',
'36~55~Biblia ne invata ca dupa moartea fizica urmeaza~Reincarnarea~Judecata~Nimic',
'29~54~Doctrina Trinitatii inseamna ca exista:~Trei Dumnezei egali ca importanta~Un singur Dumnezeu manifestat in trei persoane~Niciuna de mai sus'
];
var qpp = 2; // questions per page to display
var reslink = ""; // file to link to at end of quiz