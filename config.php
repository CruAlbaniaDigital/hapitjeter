<?php
/**
 * Config.php File Doc Comment
 *
 * @category File
 * @package  Hapitjeter
 * @author   Gordon Burgett <gordon.burgett@cru.org>
 * @license  GPL v2
 * @link     https://gitlab.com/cru-albania-ds/hapitjeter
 */

if (file_exists('config.ini')) {
    $site_config = parse_ini_file('config.ini');
}
$retval = true;
if (!$site_config) {
    $site_config=array();
    $retval = false;
}


define('MYSQL_HOST', coalesce(getenv('MYSQL_HOST'), $site_config['mysql.host']));
define('MYSQL_USER', coalesce(getenv('MYSQL_USER'), $site_config['mysql.user']));          
define('MYSQL_PASS', coalesce(getenv('MYSQL_PASSWORD'), $site_config['mysql.pass']));        
define('MYSQL_NAME', coalesce(getenv('MYSQL_DATABASE'), $site_config['mysql.schema'])); 

$url_absolut = coalesce(getenv('URL_CANONICAL'), $site_config['url.canonical']);
$url_relative = coalesce(getenv('URL_RELATIVE_PATH'), $site_config['url.relative_path']);
define('URL_ABSOLUT', $url_absolut);
define('URL_RELATIVE', $url_relative);

$limba = coalesce($site_config['site.lang'], 'en');
define('SITE_LANG', $limba);

return $retval;

/**
 * Performs a null coalesce of the two values
 * 
 * @param object $a the priority value
 * @param object $b the default value
 * 
 * @return [any] $a if not empty, $b if empty
 */
function coalesce($a, $b) 
{
    if (empty($a)) {
        return $b;
    }
    return $a;
}
